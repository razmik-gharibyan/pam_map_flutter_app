// @dart=2.9

import 'package:firebase_auth/firebase_auth.dart';

abstract class UserRepository {

  Future<void> register({final String email, final String password});

  Future<void> login({final String email, final String password});

  Future<void> sendForgotPasswordEmail(final String email);

  Future<void> logout();

  Future<User> getUserInfo();

  Future<String> getRefreshToken();

  Future<void> delete();
}