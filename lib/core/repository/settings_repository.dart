import '../model/model.dart';

abstract class SettingsRepository {
  Future<SettingsResponse> getSettings();

  Future<List<BorderResponse>> getBorders();
}
