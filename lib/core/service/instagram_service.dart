import 'package:pam_map_flutter_app/core/model/model.dart';

abstract class InstagramService {

  Future<InstagramAuthResponse> getAccessInfo(final String code);

  Future<InstagramLongAuthResponse> getLongAccessInfo(final String token);

  Future<String> getUsername(final int userId, final String token);

  Future<InstagramProfileInfo> getProfileInfo(final String username);
}