// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import '../di/di.dart';
import '../model/model.dart';

abstract class NotificationServiceMixin {
  final _notificationService = injector<NotificationService>();

  void showErrorNotification({BuildContext context, String message, bool hideActive = true, Duration duration}) {
    _notificationService.showError(
      context: context,
      message: message,
      hideActive: hideActive,
      duration: duration,
    );
  }

  void showInfoNotification({BuildContext context, String message, hideActive = true, Duration duration}) {
    _notificationService.showInfo(
      context: context,
      message: message,
      hideActive: hideActive,
      duration: duration,
    );
  }

  void showWarningNotification({BuildContext context, String message, hideActive = true, Duration duration}) {
    _notificationService.showWarning(
      context: context,
      message: message,
      hideActive: hideActive,
      duration: duration,
    );
  }

  void showHttpErrorNotification({BuildContext context, ErrorResponse err, bool hideActive = true, Duration duration}) {
    _notificationService.showHttpError(
      context: context,
      err: err,
      hideActive: hideActive,
      duration: duration,
    );
  }

  void hideActiveNotification(BuildContext context) {
    _notificationService.hideActive(context);
  }
}

abstract class NotificationService {
  void showError({
    BuildContext context,
    String message,
    bool hideActive,
    Duration duration,
  });

  void showInfo({
    BuildContext context,
    String message,
    bool hideActive,
    Duration duration,
  });

  void showWarning({
    BuildContext context,
    String message,
    bool hideActive,
    Duration duration,
  });

  void showHttpError({
    BuildContext context,
    ErrorResponse err,
    bool hideActive,
    Duration duration,
  });

  void hideActive(BuildContext context);
}
