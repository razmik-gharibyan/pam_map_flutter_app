import 'package:flutter/material.dart';

abstract class DialogService {
  Future<T> showPlatform<T>({
    @required BuildContext context,
    @required Widget child,
    bool barrierDismissible,
  });

  Future<T> show<T>({
    BuildContext context,
    RoutePageBuilder pageBuilder,
    bool barrierDismissible,
    String barrierLabel,
    Color barrierColor,
    Duration transitionDuration,
    RouteTransitionsBuilder transitionBuilder,
    bool useRootNavigator,
  });

  void hideActiveDialog<T extends Object>([T result]);
}