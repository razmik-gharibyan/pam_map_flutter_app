import 'package:cloud_firestore/cloud_firestore.dart';

import '../model/model.dart';

abstract class FirestoreService {
  Future<void> addUserDocument(final UserProfileInfo user, final String documentId);

  Future<void> updateUserDocument(final UserProfileInfo user, final String documentId);

  Future<UserProfileInfo> getUserDocument(final String documentId);

  Future<List<UserProfileInfo>> findUsersInVisibleRegion(
    final GeoPoint lowGeoPoint,
    final GeoPoint highGeoPoint,
  );

  Future<List<UserProfileInfo>> searchUsers(final String text);

  Future<void> deleteUserDocument(final String documentId);
}
