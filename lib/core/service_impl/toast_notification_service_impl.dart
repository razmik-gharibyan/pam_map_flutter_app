// @dart=2.9

import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:pam_map_flutter_app/common/common.dart';

import '../model/model.dart';
import '../service/service.dart';

// TODO: IOS hanging when swipe back
// https://github.com/AndreHaueisen/flushbar/issues/104
class ToastNotificationService implements NotificationService {
  final Duration defaultDuration;

  final Color errorBackgroundColor;
  final Color errorTextColor;
  final Icon errorIcon;

  final Color infoBackgroundColor;
  final Color infoTextColor;
  final Icon infoIcon;

  final Color warningBackgroundColor;
  final Color warningTextColor;
  final Icon warningIcon;

  Flushbar _flushBar;

  ToastNotificationService({
    @required this.defaultDuration,
    @required this.errorBackgroundColor,
    @required this.errorTextColor,
    this.errorIcon,
    @required this.infoBackgroundColor,
    @required this.infoTextColor,
    this.infoIcon,
    @required this.warningBackgroundColor,
    @required this.warningTextColor,
    this.warningIcon,
  });

  @override
  void showError({
    final BuildContext context,
    final String message,
    final bool hideActive = true,
    final Duration duration,
  }) {

    if (hideActive) {
      this.hideActive(context);
    }

    _flushBar = _buildFlushBar(
      icon: errorIcon,
      content: Text(
        message,
        style: TextStyle(color: errorTextColor),
      ),
      backgroundColor: errorBackgroundColor,
    )..show(context);
  }

  @override
  void showInfo({
    final BuildContext context,
    final String message,
    final hideActive = true,
    final Duration duration,
  }) {

    if (hideActive) {
      this.hideActive(context);
    }

    _flushBar = _buildFlushBar(
      icon: infoIcon,
      content: Text(
        message,
        style: TextStyle(color: infoTextColor),
      ),
      backgroundColor: infoBackgroundColor,
      duration: duration,
    )..show(context);
  }

  @override
  void showWarning({
    final BuildContext context,
    final String message,
    final hideActive = true,
    final Duration duration,
  }) {

    if (hideActive) {
      this.hideActive(context);
    }

    _flushBar = _buildFlushBar(
      icon: warningIcon,
      content: Text(
        message,
        style: TextStyle(color: warningTextColor),
      ),
      backgroundColor: warningBackgroundColor,
      duration: duration,
    )..show(context);
  }

  @override
  void showHttpError({
    final BuildContext context,
    final ErrorResponse err,
    final bool hideActive = true,
    final Duration duration,
  }) {

    if (hideActive) {
      this.hideActive(context);
    }

    final content = buildRichTextFromErrors(context: context, err: err, textColor: errorTextColor);

    _flushBar = _buildFlushBar(
      icon: errorIcon,
      content: content,
      backgroundColor: errorBackgroundColor,
    )..show(context);
  }

  @override
  void hideActive(_) {
    if (_flushBar != null) {
      _flushBar.dismiss();
    }
  }

  Flushbar _buildFlushBar({
    final Icon icon,
    final Widget content,
    final Color backgroundColor,
    final Duration duration,
  }) {
    return Flushbar(
      messageText: content,
      icon: icon,
      backgroundColor: backgroundColor,
      duration: duration ?? defaultDuration,
      shouldIconPulse: false,
      flushbarPosition: FlushbarPosition.TOP,
      flushbarStyle: FlushbarStyle.FLOATING,
      margin: EdgeInsets.all(8),
      borderRadius: BorderRadius.circular(4),
    );
  }
}
