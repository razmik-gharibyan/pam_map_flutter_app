// @dart=2.9

import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';

import '../di/di.dart';
import '../model/model.dart';
import '../service/service.dart';

class FirestoreServiceImpl implements FirestoreService {
  final _firestore = injector<FirebaseFirestore>();

  // TODO: Move to global variable
  final _usersCollection = 'users';
  final _userCollection = 'user';
  final _profile = 'profile';

  Timer _timer;

  List<UserProfileInfo> _matchedList = [];

  @override
  Future<void> addUserDocument(final UserProfileInfo user, final String documentId) async {
    await _firestore
        .collection(_usersCollection)
        .doc(documentId)
        .collection(_userCollection)
        .doc(_profile)
        .set(user.toJson());
  }

  @override
  Future<void> updateUserDocument(final UserProfileInfo user, final String documentId) async {
    await _firestore
        .collection(_usersCollection)
        .doc(documentId)
        .collection(_userCollection)
        .doc(_profile)
        .update(user.toJson());
  }

  @override
  Future<UserProfileInfo> getUserDocument(final String documentId) async {
    final result =
        await _firestore.collection(_usersCollection).doc(documentId).collection(_userCollection).doc(_profile).get();
    return UserProfileInfo.fromJson(result.data());
  }

  @override
  Future<List<UserProfileInfo>> findUsersInVisibleRegion(
    final GeoPoint lowGeoPoint,
    final GeoPoint highGeoPoint,
  ) async {

    // FIXME: Uncomment
    final result = await _firestore
        .collectionGroup(_userCollection)
        .where('location', isGreaterThanOrEqualTo: GeoPoint(lowGeoPoint.latitude, lowGeoPoint.longitude))
        .where('location', isLessThanOrEqualTo: GeoPoint(highGeoPoint.latitude, highGeoPoint.longitude))
        .where('isVisible', isEqualTo: true)
        //.where('isActive', isEqualTo: true)
        .get();

    return result.docs.map((e) => UserProfileInfo.fromJson(e.data())).toList();
  }

  @override
  Future<List<UserProfileInfo>> searchUsers(final String searchText) async {
    if (_matchedList.isNotEmpty) _matchedList.clear();
    if (searchText.isNotEmpty) {
      final resultList = await _firestore
          .collectionGroup(_userCollection)
          .orderBy('username')
          .startAt([searchText])
          .endAt(['$searchText\uf8ff'])
          .get();
      if (resultList.docs != null && resultList.docs.isNotEmpty) {
        _matchedList.clear();
        _matchedList.addAll(resultList.docs.map((e) => UserProfileInfo.fromJson(e.data())));
        return _matchedList;
      }
      return _matchedList;
    } else {
      _matchedList.clear();
      return _matchedList;
    }
  }

  @override
  Future<void> deleteUserDocument(final String documentId) async {
    await _deleteUserDocumentSubCollection(documentId);

    await _firestore.collection(_usersCollection).doc(documentId).delete();
  }

  Future<void> _deleteUserDocumentSubCollection(final String documentId) async {
    await _firestore.collection(_usersCollection).doc(documentId).collection(_userCollection).doc(_profile).delete();
  }

  void stopInBoundsTimer() {
    if (_timer != null) {
      _timer.cancel();
      _timer = null;
    }
  }
}
