// @dart=2.9

import '../service/service.dart';
import '../di/di.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class DialogServiceImpl implements DialogService {
  final _navigationService = injector<NavigationService>();
  bool _isActiveDialog = false;

  @override
  Future<T> showPlatform<T>({
    @required final BuildContext context,
    @required final Widget child,
    final bool barrierDismissible = false,
  }) {
    hideActiveDialog();
    _isActiveDialog = true;

    return showPlatformDialog<T>(
      context: context,
      barrierDismissible: barrierDismissible,
      builder: (_) => WillPopScope(
        onWillPop: () async => barrierDismissible,
        child: child,
      ),
    );
  }
  @override
  Future<T> show<T>({
    @required final BuildContext context,
    @required final RoutePageBuilder pageBuilder,
    final bool barrierDismissible,
    final String barrierLabel,
    final Color barrierColor,
    final Duration transitionDuration,
    final RouteTransitionsBuilder transitionBuilder,
    final bool useRootNavigator = true,
  }) {
    hideActiveDialog();
    _isActiveDialog = true;

    return showGeneralDialog<T>(
      context: context,
      pageBuilder: pageBuilder,
      barrierDismissible: barrierDismissible,
      barrierLabel: barrierLabel,
      barrierColor: barrierColor,
      transitionDuration: transitionDuration,
      transitionBuilder: transitionBuilder,
      useRootNavigator: useRootNavigator,
    );
  }

  @override
  void hideActiveDialog<T extends Object>([T result]) {
    if (_isActiveDialog) {
      _isActiveDialog = false;
      _navigationService.safePop(result);
    }
  }
}