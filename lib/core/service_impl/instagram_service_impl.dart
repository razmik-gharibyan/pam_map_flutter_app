// @dart=2.9

import 'package:dio/dio.dart';

import '../di/di.dart';
import '../model/model.dart';
import '../service/service.dart';

class InstagramServiceImpl implements InstagramService {
  final _httpClient = injector<Dio>();

  @override
  Future<InstagramAuthResponse> getAccessInfo(final String code) async {
    final result = await _httpClient.post(
      'https://api.instagram.com/oauth/access_token',
      options: Options(
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
      ),
      data: InstagramAccessInfoRequest(
        clientId: '460485674626498',
        clientSecret: '0962b86387a8461431728427dfb3a9e6',
        grantType: 'authorization_code',
        redirectUri: 'https://narsad.github.io/Loading-map/',
        code: code,
      ).toJson(),
    );

    return InstagramAuthResponse.fromJson(result.data);
  }

  @override
  Future<InstagramLongAuthResponse> getLongAccessInfo(final String token) async {
    final result = await _httpClient.get(
      'https://graph.instagram.com/access_token',
      queryParameters: InstagramLongAccessInfoRequest(
        grantType: 'ig_exchange_token',
        clientSecret: '0962b86387a8461431728427dfb3a9e6',
        accessToken: token,
      ).toJson(),
    );

    return InstagramLongAuthResponse.fromJson(result.data);
  }

  @override
  Future<String> getUsername(final int userId, final String token) async {
    final result = await _httpClient.get(
      'https://graph.instagram.com/$userId',
      queryParameters: InstagramProfileInfoRequest(
        fields: 'account_type,id,media_count,username',
        accessToken: token,
      ).toJson(),
    );

    // TODO: Move to class variable instead of just String
    return result.data['username'];
  }

  @override
  Future<InstagramProfileInfo> getProfileInfo(final String username) async {
    const _graphql = 'graphql';
    const _user = 'user';
    const _profilePicUrl = 'profile_pic_url_hd';
    const _fbid = 'fbid';
    const _edgeFollowedBy = 'edge_followed_by';
    const _followersCount = 'count';
    const _isPrivate = 'is_private';
    const _isVerified = 'is_verified';

    final result = await _httpClient.get('https://www.instagram.com/$username?__a=1');

    // TODO: Move into class model
    return InstagramProfileInfo(
      username: username,
      instagramId: result.data[_graphql][_user][_fbid],
      profilePictureUrl: result.data[_graphql][_user][_profilePicUrl],
      followers: result.data[_graphql][_user][_edgeFollowedBy][_followersCount],
      isPrivate: result.data[_graphql][_user][_isPrivate],
      isVerified: result.data[_graphql][_user][_isVerified],
    );
  }
}
