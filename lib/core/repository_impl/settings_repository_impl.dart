// @dart=2.9

import 'package:cloud_firestore/cloud_firestore.dart';

import '../di/di.dart';
import '../model/model.dart';
import '../repository/repository.dart';

class SettingsRepositoryImpl implements SettingsRepository {
  final _firestore = injector<FirebaseFirestore>();

  final _themeCollection = 'theme';
  final _defaultTheme = 'default-theme';
  final _borderCollection = 'border';

  @override
  Future<SettingsResponse> getSettings() async {
    final res = await _firestore.collection(_themeCollection).doc(_defaultTheme).get();

    return SettingsResponse.fromJson(res.data());
  }

  @override
  Future<List<BorderResponse>> getBorders() async {
    final res = await _firestore.collection(_borderCollection).get();

    return res.docs.map((e) => BorderResponse.fromJson(e.data())).toList();
  }
}
