// @dart=2.9

import 'package:firebase_auth/firebase_auth.dart';

import '../repository/repository.dart';

class UserRepositoryImpl implements UserRepository {
  final _auth = FirebaseAuth.instance;

  @override
  Future<void> register({String email, String password}) async {
    await _auth.createUserWithEmailAndPassword(email: email, password: password);
  }

  @override
  Future<void> login({String email, String password}) async {
    await _auth.signInWithEmailAndPassword(email: email, password: password);
  }

  @override
  Future<void> sendForgotPasswordEmail(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  @override
  Future<void> logout() async {
    // TODO: update isActive param to false in firestore
    await _auth.signOut();
  }

  @override
  Future<User> getUserInfo() async {
    return _auth.currentUser;
  }

  @override
  Future<String> getRefreshToken() async {
    return _auth.currentUser == null ? null : _auth.currentUser.refreshToken;
  }

  Future<void> sendEmailVerification() async {
    await _auth.currentUser.sendEmailVerification();
  }

  @override
  Future<void> delete() async {
    await _auth.currentUser.delete();
  }
}
