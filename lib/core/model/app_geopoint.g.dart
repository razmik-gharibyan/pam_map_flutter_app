// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_geopoint.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AppGeoPoint _$AppGeoPointFromJson(Map<String, dynamic> json) => AppGeoPoint(
      (json['latitude'] as num).toDouble(),
      (json['longitude'] as num).toDouble(),
    );

Map<String, dynamic> _$AppGeoPointToJson(AppGeoPoint instance) =>
    <String, dynamic>{
      'latitude': instance.latitude,
      'longitude': instance.longitude,
    };
