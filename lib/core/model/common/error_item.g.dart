// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'error_item.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ErrorItem _$ErrorItemFromJson(Map<String, dynamic> json) => ErrorItem(
      errorCode: $enumDecodeNullable(_$AppErrorCodeEnumMap, json['errorCode']),
      description: json['description'] as String?,
    );

Map<String, dynamic> _$ErrorItemToJson(ErrorItem instance) => <String, dynamic>{
      'errorCode': _$AppErrorCodeEnumMap[instance.errorCode],
      'description': instance.description,
    };

const _$AppErrorCodeEnumMap = {
  AppErrorCode.INVALID_EMAIL: 'INVALID_EMAIL',
  AppErrorCode.WRONG_PASSWORD: 'WRONG_PASSWORD',
  AppErrorCode.USER_NOT_FOUND: 'USER_NOT_FOUND',
  AppErrorCode.EMAIL_ALREADY_IN_USE: 'EMAIL_ALREADY_IN_USE',
  AppErrorCode.SERVER_ERROR: 'SERVER_ERROR',
};
