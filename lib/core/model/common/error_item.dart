import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import 'app_error_code.dart';

part 'error_item.g.dart';

@JsonSerializable()
@immutable
class ErrorItem {
  final AppErrorCode? errorCode;
  final String? description;

  const ErrorItem({required this.errorCode, this.description});

  factory ErrorItem.fromJson(Map<String, dynamic> json) => _$ErrorItemFromJson(json);

  Map<String, dynamic> toJson() => _$ErrorItemToJson(this);

  @override
  String toString() {
    return 'ErrorItem{errorCode: $errorCode, description: $description}';
  }
}
