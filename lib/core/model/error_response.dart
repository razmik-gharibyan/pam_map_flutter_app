import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import 'common/common.dart';


part 'error_response.g.dart';

@JsonSerializable()
@immutable
class ErrorResponse {

  @JsonKey(defaultValue: [])
  final List<ErrorItem>? errors;

  const ErrorResponse(this.errors);

  factory ErrorResponse.fromJson(Map<String, dynamic>? json) => _$ErrorResponseFromJson(json ?? {});

  Map<String, dynamic> toJson() => _$ErrorResponseToJson(this);
}