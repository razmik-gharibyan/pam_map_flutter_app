// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_profile_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserProfileInfo _$UserProfileInfoFromJson(Map<String, dynamic> json) =>
    UserProfileInfo(
      token: json['token'] as String?,
      location:
          UserProfileInfo._fromJsonGeoPoint(json['location'] as GeoPoint?),
      isVisible: json['isVisible'] as bool?,
      isActive: json['isActive'] as bool?,
      id: json['id'] as String?,
      cardColor: json['cardColor'] as String?,
      border: json['border'] == null
          ? null
          : BorderResponse.fromJson(json['border'] as Map<String, dynamic>?),
      instagramInfo: json['instagramInfo'] == null
          ? null
          : InstagramProfileInfo.fromJson(
              json['instagramInfo'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$UserProfileInfoToJson(UserProfileInfo instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('token', instance.token);
  writeNotNull('location', UserProfileInfo._toJsonGeoPoint(instance.location));
  writeNotNull('isVisible', instance.isVisible);
  writeNotNull('isActive', instance.isActive);
  writeNotNull('id', instance.id);
  writeNotNull('cardColor', instance.cardColor);
  writeNotNull('border', instance.border?.toJson());
  writeNotNull('instagramInfo', instance.instagramInfo?.toJson());
  return val;
}
