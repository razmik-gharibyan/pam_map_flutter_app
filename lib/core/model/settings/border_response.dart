import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

part 'border_response.g.dart';

@JsonSerializable()
@immutable
class BorderResponse extends Equatable {
  final String? name;
  final String? url;

  bool get haveBorder => name != 'None';

  const BorderResponse(
      this.name,
      this.url,
      );

  factory BorderResponse.fromJson(Map<String, dynamic>? json) => _$BorderResponseFromJson(json ?? {});

  Map<String, dynamic> toJson() => _$BorderResponseToJson(this);

  @override
  List<Object?> get props => [name, url];
}
