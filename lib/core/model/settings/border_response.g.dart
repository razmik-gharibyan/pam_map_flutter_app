// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'border_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

BorderResponse _$BorderResponseFromJson(Map<String, dynamic> json) =>
    BorderResponse(
      json['name'] as String?,
      json['url'] as String?,
    );

Map<String, dynamic> _$BorderResponseToJson(BorderResponse instance) =>
    <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
    };
