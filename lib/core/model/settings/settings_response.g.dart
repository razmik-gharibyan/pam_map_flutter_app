// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'settings_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SettingsResponse _$SettingsResponseFromJson(Map<String, dynamic> json) =>
    SettingsResponse(
      json['border'] == null
          ? null
          : BorderResponse.fromJson(json['border'] as Map<String, dynamic>?),
      json['cardColor'] as String?,
    );

Map<String, dynamic> _$SettingsResponseToJson(SettingsResponse instance) =>
    <String, dynamic>{
      'border': instance.border,
      'cardColor': instance.cardColor,
    };
