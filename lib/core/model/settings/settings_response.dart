import 'package:json_annotation/json_annotation.dart';
import 'package:meta/meta.dart';

import '../model.dart';

part 'settings_response.g.dart';

@JsonSerializable()
@immutable
class SettingsResponse {
  final BorderResponse? border;
  final String? cardColor;

  const SettingsResponse(
    this.border,
    this.cardColor,
  );

  factory SettingsResponse.fromJson(Map<String, dynamic>? json) => _$SettingsResponseFromJson(json ?? {});

  Map<String, dynamic> toJson() => _$SettingsResponseToJson(this);
}
