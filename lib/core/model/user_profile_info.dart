import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

import 'model.dart';

part 'user_profile_info.g.dart';

@JsonSerializable(explicitToJson: true)
@immutable
class UserProfileInfo extends Equatable {
  @JsonKey(includeIfNull: false)
  final String? token;
  @JsonKey(includeIfNull: false, fromJson: _fromJsonGeoPoint, toJson: _toJsonGeoPoint)
  final GeoPoint? location;
  @JsonKey(includeIfNull: false)
  final bool? isVisible;
  @JsonKey(includeIfNull: false)
  final bool? isActive;
  @JsonKey(includeIfNull: false)
  final String? id;
  @JsonKey(includeIfNull: false)
  final String? cardColor;
  @JsonKey(includeIfNull: false)
  final BorderResponse? border;
  @JsonKey(includeIfNull: false)
  final InstagramProfileInfo? instagramInfo;

  const UserProfileInfo({
    this.token,
    this.location,
    this.isVisible,
    this.isActive,
    this.id,
    this.cardColor,
    this.border,
    this.instagramInfo,
  });

  bool get hasInstagram => instagramInfo?.instagramId != null;

  bool get hasLocation => location != null;

  static GeoPoint? _fromJsonGeoPoint(GeoPoint? geoPoint) {
    return geoPoint;
  }

  static GeoPoint? _toJsonGeoPoint(GeoPoint? geoPoint) {
    return geoPoint;
  }

  static UserProfileInfo fromJson(Map<String, dynamic> json) => _$UserProfileInfoFromJson(json);

  Map<String, dynamic> toJson() => _$UserProfileInfoToJson(this);

  UserProfileInfo copyWith(
    String? token,
    GeoPoint? location,
    bool? isVisible,
    bool? isActive,
    String? id,
    String? cardColor,
    BorderResponse? border,
    InstagramProfileInfo? instagramInfo,
  ) {
    return UserProfileInfo(
      token: token ?? this.token,
      location: location ?? this.location,
      isVisible: isVisible ?? this.isVisible,
      isActive: isActive ?? this.isActive,
      id: id ?? this.id,
      cardColor: cardColor ?? this.cardColor,
      border: border ?? this.border,
      instagramInfo: instagramInfo ?? this.instagramInfo,
    );
  }

  @override
  List<Object?> get props => [token, location, isVisible, isActive, id, cardColor, border, instagramInfo];
}
