import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instagram_long_auth_response.g.dart';

@JsonSerializable()
@immutable
class InstagramLongAuthResponse {
  @JsonKey(name: 'access_token')
  final String? accessToken;
  @JsonKey(name: 'token_type')
  final String? tokenType;
  @JsonKey(name: 'expires_in')
  final int? expiresIn;

  InstagramLongAuthResponse({
    this.accessToken,
    this.tokenType,
    this.expiresIn,
  });

  static InstagramLongAuthResponse fromJson(Map<String, dynamic> json) => _$InstagramLongAuthResponseFromJson(json);

  Map<String, dynamic> toJson() => _$InstagramLongAuthResponseToJson(this);

  InstagramLongAuthResponse copyWith(
    String? accessToken,
    String? tokenType,
    int? expiresIn,
  ) {
    return InstagramLongAuthResponse(
      accessToken: accessToken ?? this.accessToken,
      tokenType: tokenType ?? this.tokenType,
      expiresIn: expiresIn ?? this.expiresIn,
    );
  }
}
