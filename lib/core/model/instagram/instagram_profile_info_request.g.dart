// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instagram_profile_info_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstagramProfileInfoRequest _$InstagramProfileInfoRequestFromJson(
        Map<String, dynamic> json) =>
    InstagramProfileInfoRequest(
      fields: json['fields'] as String?,
      accessToken: json['access_token'] as String?,
    );

Map<String, dynamic> _$InstagramProfileInfoRequestToJson(
        InstagramProfileInfoRequest instance) =>
    <String, dynamic>{
      'fields': instance.fields,
      'access_token': instance.accessToken,
    };
