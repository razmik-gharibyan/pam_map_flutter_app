import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instagram_auth_response.g.dart';

@JsonSerializable()
@immutable
class InstagramAuthResponse {
  @JsonKey(name: 'access_token')
  final String? accessToken;
  @JsonKey(name: 'user_id')
  final int? userId;

  InstagramAuthResponse({
    this.accessToken,
    this.userId,
  });

  static InstagramAuthResponse fromJson(Map<String, dynamic> json) => _$InstagramAuthResponseFromJson(json);

  Map<String, dynamic> toJson() => _$InstagramAuthResponseToJson(this);

  InstagramAuthResponse copyWith(
    String? accessToken,
    int? userId,
  ) {
    return InstagramAuthResponse(
      accessToken: accessToken ?? this.accessToken,
      userId: userId ?? this.userId,
    );
  }
}
