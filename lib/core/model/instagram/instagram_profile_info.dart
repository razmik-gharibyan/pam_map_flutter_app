import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instagram_profile_info.g.dart';

@JsonSerializable()
@immutable
class InstagramProfileInfo {
  final String? username;
  final String? profilePictureUrl;
  final int? followers;
  final bool? isPrivate;
  final bool? isVerified;
  final String? instagramId;

  InstagramProfileInfo({
    this.username,
    this.profilePictureUrl,
    this.followers,
    this.isPrivate,
    this.isVerified,
    this.instagramId,
  });

  static InstagramProfileInfo fromJson(Map<String, dynamic> json) => _$InstagramProfileInfoFromJson(json);

  Map<String, dynamic> toJson() => _$InstagramProfileInfoToJson(this);

  InstagramProfileInfo copyWith(
    String? username,
    String? profilePictureUrl,
    int? followers,
    bool? isPrivate,
    bool? isVerified,
    String? instagramId,
  ) {
    return InstagramProfileInfo(
      username: username ?? this.username,
      profilePictureUrl: profilePictureUrl ?? this.profilePictureUrl,
      followers: followers ?? this.followers,
      isPrivate: isPrivate ?? this.isPrivate,
      isVerified: isVerified ?? this.isVerified,
      instagramId: instagramId ?? this.instagramId,
    );
  }
}
