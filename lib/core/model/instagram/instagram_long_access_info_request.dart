import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instagram_long_access_info_request.g.dart';

@JsonSerializable()
@immutable
class InstagramLongAccessInfoRequest {
  @JsonKey(name: 'grant_type')
  final String? grantType;
  @JsonKey(name: 'client_secret')
  final String? clientSecret;
  @JsonKey(name: 'access_token')
  final String? accessToken;

  InstagramLongAccessInfoRequest({
    this.grantType,
    this.clientSecret,
    this.accessToken,
  });

  static InstagramLongAccessInfoRequest fromJson(Map<String, dynamic> json) => _$InstagramLongAccessInfoRequestFromJson(json);

  Map<String, dynamic> toJson() => _$InstagramLongAccessInfoRequestToJson(this);

  InstagramLongAccessInfoRequest copyWith(
      String? grantType,
      String? clientSecret,
      String? accessToken,
      ) {
    return InstagramLongAccessInfoRequest(
      grantType: grantType ?? this.grantType,
      clientSecret: clientSecret ?? this.clientSecret,
      accessToken: accessToken ?? this.accessToken,
    );
  }
}
