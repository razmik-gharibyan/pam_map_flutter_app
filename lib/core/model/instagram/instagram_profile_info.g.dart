// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instagram_profile_info.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstagramProfileInfo _$InstagramProfileInfoFromJson(
        Map<String, dynamic> json) =>
    InstagramProfileInfo(
      username: json['username'] as String?,
      profilePictureUrl: json['profilePictureUrl'] as String?,
      followers: json['followers'] as int?,
      isPrivate: json['isPrivate'] as bool?,
      isVerified: json['isVerified'] as bool?,
      instagramId: json['instagramId'] as String?,
    );

Map<String, dynamic> _$InstagramProfileInfoToJson(
        InstagramProfileInfo instance) =>
    <String, dynamic>{
      'username': instance.username,
      'profilePictureUrl': instance.profilePictureUrl,
      'followers': instance.followers,
      'isPrivate': instance.isPrivate,
      'isVerified': instance.isVerified,
      'instagramId': instance.instagramId,
    };
