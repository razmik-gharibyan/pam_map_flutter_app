// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instagram_long_auth_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstagramLongAuthResponse _$InstagramLongAuthResponseFromJson(
        Map<String, dynamic> json) =>
    InstagramLongAuthResponse(
      accessToken: json['access_token'] as String?,
      tokenType: json['token_type'] as String?,
      expiresIn: json['expires_in'] as int?,
    );

Map<String, dynamic> _$InstagramLongAuthResponseToJson(
        InstagramLongAuthResponse instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'token_type': instance.tokenType,
      'expires_in': instance.expiresIn,
    };
