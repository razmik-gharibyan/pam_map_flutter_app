// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instagram_auth_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstagramAuthResponse _$InstagramAuthResponseFromJson(
        Map<String, dynamic> json) =>
    InstagramAuthResponse(
      accessToken: json['access_token'] as String?,
      userId: json['user_id'] as int?,
    );

Map<String, dynamic> _$InstagramAuthResponseToJson(
        InstagramAuthResponse instance) =>
    <String, dynamic>{
      'access_token': instance.accessToken,
      'user_id': instance.userId,
    };
