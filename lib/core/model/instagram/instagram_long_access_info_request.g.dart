// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instagram_long_access_info_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstagramLongAccessInfoRequest _$InstagramLongAccessInfoRequestFromJson(
        Map<String, dynamic> json) =>
    InstagramLongAccessInfoRequest(
      grantType: json['grant_type'] as String?,
      clientSecret: json['client_secret'] as String?,
      accessToken: json['access_token'] as String?,
    );

Map<String, dynamic> _$InstagramLongAccessInfoRequestToJson(
        InstagramLongAccessInfoRequest instance) =>
    <String, dynamic>{
      'grant_type': instance.grantType,
      'client_secret': instance.clientSecret,
      'access_token': instance.accessToken,
    };
