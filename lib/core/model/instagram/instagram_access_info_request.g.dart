// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'instagram_access_info_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InstagramAccessInfoRequest _$InstagramAccessInfoRequestFromJson(
        Map<String, dynamic> json) =>
    InstagramAccessInfoRequest(
      clientId: json['client_id'] as String?,
      clientSecret: json['client_secret'] as String?,
      grantType: json['grant_type'] as String?,
      redirectUri: json['redirect_uri'] as String?,
      code: json['code'] as String?,
    );

Map<String, dynamic> _$InstagramAccessInfoRequestToJson(
        InstagramAccessInfoRequest instance) =>
    <String, dynamic>{
      'client_id': instance.clientId,
      'client_secret': instance.clientSecret,
      'grant_type': instance.grantType,
      'redirect_uri': instance.redirectUri,
      'code': instance.code,
    };
