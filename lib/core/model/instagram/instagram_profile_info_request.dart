import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instagram_profile_info_request.g.dart';

@JsonSerializable()
@immutable
class InstagramProfileInfoRequest {
  final String? fields;
  @JsonKey(name: 'access_token')
  final String? accessToken;

  InstagramProfileInfoRequest({
    this.fields,
    this.accessToken,
  });

  static InstagramProfileInfoRequest fromJson(Map<String, dynamic> json) => _$InstagramProfileInfoRequestFromJson(json);

  Map<String, dynamic> toJson() => _$InstagramProfileInfoRequestToJson(this);

  InstagramProfileInfoRequest copyWith(
    String? fields,
    String? accessToken,
  ) {
    return InstagramProfileInfoRequest(
      fields: fields ?? this.fields,
      accessToken: accessToken ?? this.accessToken,
    );
  }
}
