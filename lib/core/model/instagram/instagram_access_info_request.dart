import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'instagram_access_info_request.g.dart';

@JsonSerializable()
@immutable
class InstagramAccessInfoRequest {
  @JsonKey(name: 'client_id')
  final String? clientId;
  @JsonKey(name: 'client_secret')
  final String? clientSecret;
  @JsonKey(name: 'grant_type')
  final String? grantType;
  @JsonKey(name: 'redirect_uri')
  final String? redirectUri;
  final String? code;

  InstagramAccessInfoRequest({
    this.clientId,
    this.clientSecret,
    this.grantType,
    this.redirectUri,
    this.code,
  });

  static InstagramAccessInfoRequest fromJson(Map<String, dynamic> json) => _$InstagramAccessInfoRequestFromJson(json);

  Map<String, dynamic> toJson() => _$InstagramAccessInfoRequestToJson(this);

  InstagramAccessInfoRequest copyWith(
    String? clientId,
    String? clientSecret,
    String? grantType,
    String? redirectUri,
    String? code,
  ) {
    return InstagramAccessInfoRequest(
      clientId: clientId ?? this.clientId,
      clientSecret: clientSecret ?? this.clientSecret,
      grantType: grantType ?? this.grantType,
      redirectUri: redirectUri ?? this.redirectUri,
      code: code ?? this.code,
    );
  }
}
