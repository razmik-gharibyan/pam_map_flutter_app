import 'package:flutter/material.dart';
import 'package:json_annotation/json_annotation.dart';

part 'app_geopoint.g.dart';

@JsonSerializable()
@immutable
class AppGeoPoint {
  final double latitude;
  final double longitude;

  const AppGeoPoint(this.latitude, this.longitude)
      : assert(latitude >= -90 && latitude <= 90),
        assert(longitude >= -180 && longitude <= 180);

  static AppGeoPoint fromJson(Map<String, dynamic> json) => _$AppGeoPointFromJson(json);

  Map<String, dynamic> toJson() => _$AppGeoPointToJson(this);

  @override
  bool operator ==(Object other) =>
      other is AppGeoPoint &&
          other.latitude == latitude &&
          other.longitude == longitude;

  @override
  int get hashCode => hashValues(latitude, longitude);
}