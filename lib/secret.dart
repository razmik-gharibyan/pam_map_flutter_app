// @dart=2.9

import 'dart:convert';

import 'package:flutter/services.dart';

class Secret {
  final String placesApiKey;

  Secret({this.placesApiKey = ""});

  factory Secret.fromJson(Map<String, dynamic> jsonMap) {
    return new Secret(placesApiKey: jsonMap["places_api_key"]);
  }
}

class SecretLoader {
  final String secretPath;

  SecretLoader({this.secretPath});

  Future<Secret> load() {
    try {
      return rootBundle.loadStructuredData<Secret>(this.secretPath, (jsonStr) async {
        final secret = Secret.fromJson(json.decode(jsonStr));
        return secret;
      });
    } catch (e) {
      print(e);
    }
  }
}
