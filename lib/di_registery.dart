// @dart=2.9

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import 'core/core.dart';
import 'ui/ui.dart';

void configure() {
  final httpService = Dio();
  injector.registerSingleton<Dio>(httpService);
  injector.registerSingleton<FlutterSecureStorage>(FlutterSecureStorage());

  injector.registerLazySingleton<FirebaseFirestore>(() => FirebaseFirestore.instance);
  injector.registerSingleton<Logger>(Logger(level: Level.warning));
  injector.registerLazySingleton<NavigationService>(() => NavigationServiceImpl());
  injector.registerLazySingleton<FirestoreService>(() => FirestoreServiceImpl());
  injector.registerLazySingleton<InstagramService>(() => InstagramServiceImpl());
  injector.registerFactory<DialogService>(() => DialogServiceImpl());
  injector.registerLazySingleton<NotificationService>(
    () => ToastNotificationService(
      defaultDuration: Duration(seconds: 4),
      errorBackgroundColor: AppColors.error,
      errorTextColor: AppColors.white,
      errorIcon: Icon(Icons.error_outline, color: AppColors.white),
      infoBackgroundColor: AppColors.accent,
      infoTextColor: AppColors.white,
      infoIcon: Icon(Icons.info_outline, color: AppColors.white),
      warningBackgroundColor: AppColors.orange,
      warningTextColor: AppColors.white,
      warningIcon: Icon(Icons.info_outline, color: AppColors.white),
    ),
  );

  injector.registerLazySingleton<UserRepository>(() => UserRepositoryImpl());
  injector.registerLazySingleton<SettingsRepository>(() => SettingsRepositoryImpl());
}
