// @dart=2.9

import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:pam_map_flutter_app/common/common.dart';


Text buildRichTextFromErrors({
  @required final BuildContext context,
  @required final ErrorResponse err,
  @required final Color textColor,
}) {
  final errors = err.errors.isEmpty ? [ErrorItem(errorCode: AppErrorCode.SERVER_ERROR)] : err.errors;

  final messages = errors
      .map((errorItem) => translateError(errorItem.errorCode))
      .toSet()
      .toList();

  return buildRichTextFromStrings(context: context, messages: messages, textColor: textColor);
}

Text buildRichTextFromStrings({
  @required final BuildContext context,
  @required final List<String> messages,
  @required final Color textColor,
}) {

  return Text.rich(
    TextSpan(
      style: Theme.of(context).textTheme.bodyText2,
      children: messages.asMap().entries.map((entity) {
        return TextSpan(
          text: entity.value + (entity.key == messages.length - 1 ? '' : '\n'),
          style: TextStyle(color: textColor),
        );
      }).toList(),
    ),
  );
}
