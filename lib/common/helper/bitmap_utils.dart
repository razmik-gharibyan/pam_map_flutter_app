// @dart=2.9

import 'dart:async';
import 'dart:typed_data';
import 'dart:ui' as ui;

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:image/image.dart' as image;

import 'constraint_utils.dart';
import 'follower_utils.dart';
// FIXME: Maybe markerRatio need to be calculated
final markerRatio = 1;
final int _clusterIconBgWidth = (150 * markerRatio);
final int _clusterIconBgHeight = (150 * markerRatio);
final int _clusterIconMaxFollowersMarkerWidth = (105 * markerRatio);
final int _clusterIconMaxFollowersMarkerHeight = (105 * markerRatio);
final double _clusterOffsetWidth = (23 * markerRatio).toDouble();
final double _clusterOffsetHeight = (12 * markerRatio).toDouble();
final int _avatarImageWidthAndHeight = 500;
final int _avatarProfilePictureWidthAndHeight = 450;

Future<Uint8List> getImageFromUrl(String url) async {
  Uint8List result = (await NetworkAssetBundle(Uri.parse(url)).load(url)).buffer.asUint8List();
  return result;
}

Future<ui.Image> _loadImage(String url, int width, int height) async {
  final img = await getImageFromUrl(url);
  image.Image decodedImage = image.decodeImage(img);
  image.Image resizedImage = image.copyResize(decodedImage, height: height, width: width);
  ui.Codec codec = await ui.instantiateImageCodec(image.encodePng(resizedImage));
  ui.FrameInfo frameInfo = await codec.getNextFrame();
  return frameInfo.image;
}

Future<Uint8List> getBytesFromCanvasWithFollowers(
  String url,
  String borderUrl,
  int followers,
  double constraint,
) async {
  final widthHeightArray = convertMarkerSizeViaFollowers(followers, constraint);
  final width = widthHeightArray[0];
  final height = widthHeightArray[1];
  // This number is calculated from when i've chose 11 and it went perfect for 100 width image,so for example for 200 it will be 11 * 200 / 100 fit
  // Same calculation logic applied for border canvas as i've chose -6 and it went perfect circle for 100 width image
  final borderWidth = width + ((11 * width) / 100);
  final borderHeight = height + ((11 * height) / 100);
  final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  final Canvas canvas = Canvas(pictureRecorder);
  ui.Image image = await _loadImage(url, width.toInt(), height.toInt());
  Path path = Path()..addOval(Rect.fromLTWH(0, 0, width.toDouble(), height.toDouble()));
  canvas.clipPath(path);
  canvas.drawImage(image, Offset.zero, new Paint());
  if (borderUrl != null) {
    if (borderUrl.isNotEmpty) {
      ui.Image borderImage = await _loadImage(borderUrl, borderWidth.toInt(), borderHeight.toInt());
      canvas.drawImage(borderImage, Offset((-6 * width) / 100, (-6 * width) / 100), new Paint());
    }
  }
  final img = await pictureRecorder.endRecording().toImage(width.toInt(), height.toInt());
  final data = await img.toByteData(format: ui.ImageByteFormat.png);
  return data.buffer.asUint8List();
}

Future<ui.Image> getClusterBgImage() async {
  var img = (await rootBundle.load("assets/images/cluster_bg_hd.png")).buffer.asUint8List();
  image.Image decodedImage = image.decodeImage(img);
  image.Image resizedImage = image.copyResize(decodedImage, height: _clusterIconBgHeight, width: _clusterIconBgWidth);
  ui.Codec codec = await ui.instantiateImageCodec(image.encodePng(resizedImage));
  ui.FrameInfo frameInfo = await codec.getNextFrame();
  return frameInfo.image;
}

Future<ui.Image> getClusterFgImage(String url) async {
  final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  final Canvas canvas = Canvas(pictureRecorder);
  ui.Image clusterFgImage =
      await _loadImage(url, _clusterIconMaxFollowersMarkerWidth, _clusterIconMaxFollowersMarkerHeight);
  Path path = Path()
    ..addOval(Rect.fromLTWH(
        0, 0, _clusterIconMaxFollowersMarkerWidth.toDouble(), _clusterIconMaxFollowersMarkerHeight.toDouble()));
  canvas.clipPath(path);
  canvas.drawImage(clusterFgImage, Offset.zero, new Paint());
  return await pictureRecorder
      .endRecording()
      .toImage(_clusterIconMaxFollowersMarkerWidth, _clusterIconMaxFollowersMarkerHeight);
}

Future<Uint8List> getBytesFromClusterBg(String url) async {
  final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  final Canvas canvas = Canvas(pictureRecorder);
  ui.Image clusterBgImage = await getClusterBgImage();
  ui.Image clusterFgImage = await getClusterFgImage(url);
  canvas.drawImage(clusterBgImage, Offset.zero, Paint());
  canvas.drawImage(clusterFgImage, Offset(_clusterOffsetWidth, _clusterOffsetHeight), Paint());
  final img = await pictureRecorder.endRecording().toImage(_clusterIconBgWidth, _clusterIconBgHeight);
  final data = await img.toByteData(format: ui.ImageByteFormat.png);
  return data.buffer.asUint8List();
}

Future<Uint8List> getAppliedBorderBytes(String imageUrl, String borderUrl) async {
  final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  final Canvas canvas = Canvas(pictureRecorder);
  ui.Image profileImage =
      await _loadImage(imageUrl, _avatarProfilePictureWidthAndHeight, _avatarProfilePictureWidthAndHeight);
  Path path = Path()
    ..addOval(Rect.fromLTWH(
        25, 25, _avatarProfilePictureWidthAndHeight.toDouble(), _avatarProfilePictureWidthAndHeight.toDouble()));
  canvas.clipPath(path);
  canvas.drawImage(profileImage, Offset(25, 25), Paint());
  if (borderUrl != null) {
    if (borderUrl.isNotEmpty) {
      ui.Image borderImage = await _loadImage(borderUrl, _avatarImageWidthAndHeight, _avatarImageWidthAndHeight);
      canvas.drawImage(borderImage, Offset.zero, Paint());
    }
  }
  final img = await pictureRecorder.endRecording().toImage(_avatarImageWidthAndHeight, _avatarImageWidthAndHeight);
  final data = await img.toByteData(format: ui.ImageByteFormat.png);
  return data.buffer.asUint8List();
}
