String convertFollowersToInstagramFollowers(int followers) {
  String instagramFollowers;
  final followersStr = followers.toString();
  if (followers >= 10000) {
    instagramFollowers = "${followersStr.substring(0, 2)}.${followersStr.substring(2, 3)} k";
    if (followers >= 100000) {
      instagramFollowers = "${followersStr.substring(0, 3)} k";
      if (followers >= 1000000) {
        instagramFollowers = "${followersStr.substring(0, 1)}.${followersStr.substring(1, 2)} m";
        if (followers >= 10000000) {
          instagramFollowers = "${followersStr.substring(0, 2)}.${followersStr.substring(2, 3)} m";
          if (followers >= 100000000) {
            instagramFollowers = "${followersStr.substring(0, 3)} m";
          }
        }
      }
    }
  } else {
    instagramFollowers = followersStr;
  }
  return instagramFollowers;
}

List<double> convertMarkerSizeViaFollowers(int followers, double constraint) {
  var markerWidthHeight = [80 * constraint, 80 * constraint];
  if (followers >= 1000) {
    markerWidthHeight[0] = 100 * constraint;
    markerWidthHeight[1] = 100 * constraint;
    if (followers >= 10000) {
      markerWidthHeight[0] = 120 * constraint;
      markerWidthHeight[1] = 120 * constraint;
      if (followers >= 50000) {
        markerWidthHeight[0] = 140 * constraint;
        markerWidthHeight[1] = 140 * constraint;
        if (followers >= 100000) {
          markerWidthHeight[0] = 160 * constraint;
          markerWidthHeight[1] = 160 * constraint;
          if (followers >= 500000) {
            markerWidthHeight[0] = 180 * constraint;
            markerWidthHeight[1] = 180 * constraint;
            if (followers >= 1000000) {
              markerWidthHeight[0] = 200 * constraint;
              markerWidthHeight[1] = 200 * constraint;
              if (followers >= 15000000) {
                markerWidthHeight[0] = 220 * constraint;
                markerWidthHeight[1] = 220 * constraint;
                if (followers >= 50000000) {
                  markerWidthHeight[0] = 240 * constraint;
                  markerWidthHeight[1] = 240 * constraint;
                  if (followers >= 100000000) {
                    markerWidthHeight[0] = 300 * constraint;
                    markerWidthHeight[1] = 300 * constraint;
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  return markerWidthHeight;
}
