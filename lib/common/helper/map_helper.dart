// @dart=2.9
/*
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:ui';

import 'package:fluster/fluster.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pam_map_flutter_app/helper/bitmap_utils.dart';
import 'package:pam_map_flutter_app/helper/marker_user.dart';

/// In here we are encapsulating all the logic required to get marker icons from url images
/// and to show clusters using the [Fluster] package.
class MapHelper {
  /// If there is a cached file and it's not old returns the cached marker image file
  /// else it will download the image and save it on the temp dir and return that file.
  ///
  /// This mechanism is possible using the [DefaultCacheManager] package and is useful
  /// to improve load times on the next map loads, the first time will always take more
  /// time to download the file and set the marker image.
  ///
  /// You can resize the marker image by providing a [targetWidth].

  static BitmapEditor _bitmapEditor = BitmapEditor();
  static List<String> documentIdList = List<String>();
  static List<String> documentIdListFeedback = List<String>();
  static List<String> notRemovableIndexList = List<String>();
  static Map<String, BitmapDescriptor> bitmapList = Map<String, BitmapDescriptor>();
  static Map<String, BitmapDescriptor> bitmapListFeedback = Map<String, BitmapDescriptor>();
  static Map<String, BitmapDescriptor> bitmapListFeedbackCopy = Map<String, BitmapDescriptor>();

  static Future<BitmapDescriptor> getMarkerImageFromUrl(
    String url, {
    int targetWidth,
  }) async {
    assert(url != null);

    final File markerImageFile = await DefaultCacheManager().getSingleFile(url);

    Uint8List markerImageBytes = await markerImageFile.readAsBytes();

    if (targetWidth != null) {
      markerImageBytes = await _resizeImageBytes(
        markerImageBytes,
        targetWidth,
      );
    }
    return BitmapDescriptor.fromBytes(markerImageBytes);
  }

  /// Resizes the given [imageBytes] with the [targetWidth].
  ///
  /// We don't want the marker image to be too big so we might need to resize the image.
  static Future<Uint8List> _resizeImageBytes(
    Uint8List imageBytes,
    int targetWidth,
  ) async {
    assert(imageBytes != null);
    assert(targetWidth != null);

    final Codec imageCodec = await instantiateImageCodec(
      imageBytes,
      targetWidth: targetWidth,
    );

    final FrameInfo frameInfo = await imageCodec.getNextFrame();

    final ByteData byteData = await frameInfo.image.toByteData(
      format: ImageByteFormat.png,
    );

    return byteData.buffer.asUint8List();
  }

  /// Inits the cluster manager with all the [MapMarker] to be displayed on the map.
  /// Here we're also setting up the cluster marker itself, also with an [clusterImageUrl].
  ///
  /// For more info about customizing your clustering logic check the [Fluster] constructor.
  static Future<Fluster<MapMarker>> initClusterManager(
    List<MapMarker> markers,
    int minZoom,
    int maxZoom,
  ) async {
    assert(markers != null);
    assert(minZoom != null);
    assert(maxZoom != null);

    return Fluster<MapMarker>(
      minZoom: minZoom,
      maxZoom: maxZoom,
      radius: 150,
      extent: 2048,
      nodeSize: 64,
      points: markers,
      createCluster: (
        BaseCluster cluster,
        double lng,
        double lat,
      ) =>
          MapMarker(
        id: cluster.id.toString(),
        position: LatLng(lat, lng),
        isCluster: cluster.isCluster,
        clusterId: cluster.id,
        pointsSize: cluster.pointsSize,
        childMarkerId: cluster.childMarkerId,
      ),
    );
  }

  /// Gets a list of markers and clusters that reside within the visible bounding box for
  /// the given [currentZoom]. For more info check [Fluster.clusters].
  static Future<List<Marker>> getClusterMarkers(
    Fluster<MapMarker> clusterManager,
    double currentZoom,
    Color clusterColor,
    Color clusterTextColor,
    int clusterWidth,
  ) async {
    assert(currentZoom != null);
    assert(clusterColor != null);
    assert(clusterTextColor != null);
    assert(clusterWidth != null);

    if (clusterManager == null) return Future.value([]);
    var resultList = await getMarkerListFromClusters(
        clusterManager, clusterManager.clusters([-180, -85, 180, 85], currentZoom.toInt()));
    if (bitmapListFeedback.isNotEmpty) {
      for (var bitmapKey in bitmapListFeedback.keys) {
        if (notRemovableIndexList.isNotEmpty) {
          if (!notRemovableIndexList.contains(bitmapKey)) {
            // Remove this bitmap from list because it is no longer a cluster bitmap
            bitmapListFeedbackCopy.remove(bitmapKey);
          }
        } else {
          bitmapListFeedbackCopy.clear();
          break;
        }
      }
    }
    bitmapListFeedback.clear();
    if (bitmapList.isNotEmpty) {
      bitmapListFeedbackCopy.addAll(bitmapList);
    }
    if (bitmapListFeedbackCopy.isNotEmpty) {
      bitmapListFeedback.addAll(bitmapListFeedbackCopy);
    }
    documentIdListFeedback.clear();
    if (documentIdList.isNotEmpty) {
      documentIdListFeedback.addAll(documentIdList);
    }
    documentIdList.clear();
    bitmapList.clear();
    notRemovableIndexList.clear();
    return resultList;
  }

  static Future<List<Marker>> getMarkerListFromClusters(
      Fluster<MapMarker> clusterManager, List<MapMarker> mapMarkerList) async {
    List<Marker> markerList = List<Marker>();
    for (var mapMarker in mapMarkerList) {
      if (mapMarker.isCluster) {
        BitmapDescriptor bitmap;
        int max = 0;
        PammapFirebaseUser maxFollowersUser;
        for (var marker in clusterManager.points(mapMarker.clusterId)) {
          // Find user with max follower number
          if (marker.user.data["followers"] >= max) {
            max = marker.user.data["followers"];
            maxFollowersUser = marker.user;
          }
        }
        documentIdList.add(maxFollowersUser.data["documentId"]);
        bitmap = await checkIfNeedToCreateBitmap(maxFollowersUser);
        if (documentIdListFeedback.contains(maxFollowersUser.data["documentId"])) {
          // Found bitmap with document id, in a list, which means do not remove this bitmap
          bitmap = bitmapListFeedback[maxFollowersUser.data["documentId"]];
          notRemovableIndexList.add(maxFollowersUser.data["documentId"]);
        }
        mapMarker.icon = bitmap;
      }
      markerList.add(mapMarker.toMarker());
    }
    return markerList;
  }

  static Future<BitmapDescriptor> checkIfNeedToCreateBitmap(PammapFirebaseUser maxFollowersUser) async {
    // This function should be separated because of async call
    BitmapDescriptor bitmap;
    if (bitmapListFeedback == null || !documentIdListFeedback.contains((maxFollowersUser.data["documentId"]))) {
      // Enter here if bitmap has to be created from scratch, and bitmapList does not contain it.
      final byteList = await _bitmapEditor.getBytesFromClusterBg(maxFollowersUser.data["profilePicture"]);
      bitmap = BitmapDescriptor.fromBytes(byteList);
      bitmapList.putIfAbsent(maxFollowersUser.data["documentId"], () => bitmap);
    }
    return bitmap;
  }
}

 */
