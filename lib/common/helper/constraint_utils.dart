// @dart=2.9

const double screenHeightCoefficent = 683.42;
const double screenWidthCoefficent = 411.42;

double getConstraintForMarkers(final double width, final double height) {
  double result;
  if (width >= 500 && height <= 600) {
    result = 0.7;
  } else if (width > 600 && height <= 700) {
    result = 1;
  } else if (width > 700 && height <= 800) {
    result = 1.2;
  } else if (width > 800 && height <= 900) {
    result = 1.5;
  } else if (width > 900) {
    result = 1.5;
  } else if (width < 500) {
    result = 0.5;
  }
  return result;
}
