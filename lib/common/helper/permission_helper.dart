// @dart=2.9

/*
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:location_permissions/location_permissions.dart' as permission;
import 'package:pam_map_flutter_app/providers/location_provider.dart';
import 'package:pam_map_flutter_app/providers/visibility_provider.dart';

import '../core/core.dart';

enum PermissionGroup {
  locationAlways,
  locationWhenInUse,
  locationDenied,
}

class PermissionHelper {
  final _firestoreService = injector<FirestoreService>();

  Function _notifyParent;

  PermissionHelper(this._notifyParent);

  final _visibilityProvider = VisibilityProvider();
  final _locationProvider = LocationProvider();

  Future<PermissionGroup> checkOrApplyPermissions(BuildContext context) async {
    if (Platform.isAndroid) {
      final permissionStatus = await _locationProvider.getPermissionForAndroid();
      if (permissionStatus == PermissionGroup.locationDenied) {
        if (_notifyParent != null) _notifyParent(false);
        final result = await askUserLocationDialog(context);
        if (result) {
          userInfo.data["isVisible"] = false;
          await _firestoreService.addUserDocument(userInfo);
          final locationProvider = Provider.of<LocationProvider>(context, listen: false);
          await locationProvider.askForPermission();
          final permissionStatus = await locationProvider.getPermissionForAndroid();
          print(permissionStatus);
          if (permissionStatus == helper.PermissionGroup.locationDenied) {
            // User select to not give permission
            userInfo.data["isVisible"] = false;
            await _firestoreProvider.addUserDocument(userInfo);
            _visibilityProvider.setIsVisible(false);
            Navigator.of(context).pop();
            if (!_visibilityProvider.isMapEntered) {
              Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
            } else {
              _visibilityProvider.canUserBeVisible = false;
            }
          } else if (permissionStatus == helper.PermissionGroup.locationWhenInUse) {
            // User allowed permission
            userInfo.data["isVisible"] = true;
            await _firestoreProvider.addUserDocument(userInfo);
            final serviceStatus = await Location().serviceEnabled();
            if (serviceStatus) {
              // Location service enabled
              locationProvider.startLocationUpdates();
              await _firestoreProvider.addUserDocument(userInfo);
              _visibilityProvider.setIsVisible(true);
              Navigator.of(context).pop();
              if (!_visibilityProvider.isMapEntered) {
                Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
              } else {
                _visibilityProvider.canUserBeVisible = true;
              }
            } else {
              Navigator.of(context).pop();
              showEnableLocationDialog(context);
            }
          } else if (permissionStatus == helper.PermissionGroup.locationWhenInUse) {
            Navigator.of(context).pop();
            openSettingsWhenInUseChosen(context);
          }
        } else {
          userInfo.data["isVisible"] = false;
          await _firestoreProvider.addUserDocument(userInfo);
          _visibilityProvider.setIsVisible(false);
          Navigator.of(context).pop();
          if (!_visibilityProvider.isMapEntered) {
            Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          } else {
            _visibilityProvider.canUserBeVisible = false;
          }
        }
      } else if (permissionStatus == PermissionGroup.locationWhenInUse) {
        // Permission has been granted
        final serviceStatus = await permission.LocationPermissions().checkServiceStatus();
        if (serviceStatus == permission.ServiceStatus.enabled) {
          // Location service enabled
          print("location enabled");
          final locationProvider = LocationProvider();
          locationProvider.startLocationUpdates();
          _currentPammapFirestoreUser.data["isVisible"] = true;
          await FirestoreProvider().addUserDocument(_currentPammapFirestoreUser);
          if (!_visibilityProvider.isMapEntered) {
            Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
          } else {
            _visibilityProvider.canUserBeVisible = true;
          }
        } else {
          // Location service is disabled
          print("location disabled");
          if (_notifyParent != null) _notifyParent(false);
          _showDialogMaker.showEnableLocationDialog(context);
        }
      } else {
        _showDialogMaker.openSettingsWhenInUseChosen(context);
      }
    } else if (Platform.isIOS) {
      // TODO: Uncomment and fix for iOS
      /*
      final serviceStatus = await permission.LocationPermissions().checkServiceStatus();
      if (serviceStatus == permission.ServiceStatus.enabled) {
        // Location service enabled you can ask for permission status now
        print("location enabled");
        final permissionStatus = await permission.LocationPermissions().checkPermissionStatus();
        print("permission is $permissionStatus");
        if(permissionStatus == permission.PermissionStatus.denied) {
          // Permission was denied
          if(_notifyParent != null) _notifyParent(false);
          _showDialogMaker.openSettingsOrContinue(context);
        }else if(permissionStatus == permission.PermissionStatus.unknown) {
          // Permission never been asked yet
          _showDialogMaker.askUserLocationDialog(_currentPammapFirestoreUser, context);
        }else if(permissionStatus == permission.PermissionStatus.granted) {
          // Permission is granted
          final locationProvider = LocationProvider();
          locationProvider.startLocationUpdates();
          _currentPammapFirestoreUser.data["isVisible"] = true;
          await FirestoreProvider().addOrUpdateUserDocument(_currentPammapFirestoreUser);
          if(!_visibilityProvider.isMapEntered) {
            Navigator.of(context).pushReplacementNamed(MainCarcasScreen.routeName);
          }else{
            _visibilityProvider.canUserBeVisible = true;
          }
        }
      }else{
        // Location service is disabled, ask user to enable so you can check for permissions
        if(_notifyParent != null) _notifyParent(false);
        _showDialogMaker.showEnableLocationDialog(context);
      }

       */
    }
  }
}

 */
