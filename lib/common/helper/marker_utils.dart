// @dart=2.9

import 'package:fluster/fluster.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:pam_map_flutter_app/ui/home/home.dart';
import 'package:url_launcher/url_launcher.dart';

import 'bitmap_utils.dart';
import 'follower_utils.dart';

// FIXME: Translate
final _followers = 'followers';
final _account = 'account';

Future<MarkerUser> createRawMapMarker(final UserProfileInfo user) async {
  // FIXME: Maybe markerRatio need to be calculated
  final markerRatio = 1;
  // FIXME: Translate
  final private = user.instagramInfo.isPrivate ? 'Private' : 'Public';
  final instagramFollowers = convertFollowersToInstagramFollowers(user.instagramInfo.followers);
  final infoWindow = InfoWindow(
    title: '${user.instagramInfo.username} : $instagramFollowers $_followers, $private $_account',
    // FIXME: Translate
    snippet: 'Tap to view instagram profile',
    onTap: () => _openInstagramProfileUrl(user.instagramInfo.username),
  );
  final icon = BitmapDescriptor.fromBytes(
    await getBytesFromCanvasWithFollowers(
      user.instagramInfo.profilePictureUrl,
      user.border.url,
      user.instagramInfo.followers,
      markerRatio.toDouble(),
    ),
  );
  return MarkerUser(
    user: user,
    id: user.id,
    icon: icon,
    infoWindow: infoWindow,
    position: LatLng(
      user.location.latitude,
      user.location.longitude,
    ),
  );
}

Fluster<MarkerUser> initFluster(
  final List<MarkerUser> markerUsers,
  final int minZoom,
  final int maxZoom,
) {
  return Fluster<MarkerUser>(
    minZoom: minZoom,
    maxZoom: maxZoom,
    radius: 150,
    extent: 2048,
    nodeSize: 64,
    points: markerUsers,
    createCluster: (BaseCluster cluster, double lng, double lat) => MarkerUser(
      id: cluster.id.toString(),
      position: LatLng(lat, lng),
      isCluster: cluster.isCluster,
      clusterId: cluster.id,
      pointsSize: cluster.pointsSize,
      childMarkerId: cluster.childMarkerId,
    ),
  );
}

List<Marker> getMarkers(final Fluster<MarkerUser> fluster, final double currentZoom) {
  final markerUsers = fluster.clusters([-180, -85, 180, 85], currentZoom.toInt());
  return markerUsers.map((markerUser) {
    Marker marker = markerUser.toMarker;
    if (markerUser.isCluster) {
      final maxFollowerMarkerUser = _getMaxFollowerUser(fluster.points(markerUser.clusterId));
      marker = markerUser
          .copyWith(
            icon: maxFollowerMarkerUser.icon,
            infoWindow: InfoWindow(title: '${maxFollowerMarkerUser.user.instagramInfo.username} and more'),
          )
          .toMarker;
    }
    return marker;
  }).toList();
}

void _openInstagramProfileUrl(final String username) async {
  final url = 'https://instagram.com/$username';
  if (await canLaunch(url)) await launch(url);
}

MarkerUser _getMaxFollowerUser(final List<MarkerUser> markerUsers) {
  markerUsers.sort((a, b) => a.user.instagramInfo.followers);
  return markerUsers.last;
}
