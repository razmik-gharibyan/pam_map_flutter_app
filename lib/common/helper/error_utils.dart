// @dart=2.9

import 'package:pam_map_flutter_app/core/core.dart';

String translateError(AppErrorCode code) {
  final errorCodeMap = {
    AppErrorCode.SERVER_ERROR: 'Something went wrong',
    AppErrorCode.USER_NOT_FOUND: 'Wrong email or password',
    AppErrorCode.EMAIL_ALREADY_IN_USE: 'Email already in use',
    AppErrorCode.INVALID_EMAIL: 'Wrong email or password',
    AppErrorCode.WRONG_PASSWORD: 'Wrong email or password',
  };

  return errorCodeMap[code];
}
