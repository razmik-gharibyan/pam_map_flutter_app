// @dart=2.9

import 'auth/auth.dart';
import 'home/home.dart';
import 'instagram/instagram.dart';
import 'settings/settings.dart';

final routes = {
  ...authRoutes,
  ...homeRoutes,
  ...instagramRoutes,
  ...settingsRoutes,
};
