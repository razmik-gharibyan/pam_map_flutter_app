// @dart=2.9

import 'forgot_password/forgot_password.dart';
import 'legacy/auth_screen.dart';

final authRoutes = {
  'auth': (_) => AuthScreen(),
  'auth/forgot-password': (_) => ForgotPasswordScreen(),
};
