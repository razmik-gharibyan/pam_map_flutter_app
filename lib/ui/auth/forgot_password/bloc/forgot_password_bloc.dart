// @dart=2.9

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import '../../../common/common.dart';

import 'forgot_password_event.dart';
import 'forgot_password_state.dart';

class ForgotPasswordBloc extends Bloc<ForgotPasswordEvent, ForgotPasswordState> with ErrorMixin {
  final _userRepository = injector<UserRepository>();

  ForgotPasswordBloc() : super(ForgotPasswordInitial());

  @override
  Stream<ForgotPasswordState> mapEventToState(final ForgotPasswordEvent event) async* {
    if (event is ForgotPasswordRecoverPressed) {
      yield* _mapRecoverPressedToState(event.email);
    }
  }

  Stream<ForgotPasswordState> _mapRecoverPressedToState(final String email) async* {
    yield ForgotPasswordInProgress();

    try {
      await _userRepository.sendForgotPasswordEmail(email);
      yield ForgotPasswordSendSuccess();
    } on FirebaseAuthException catch (err) {
      final errorCode = mapFirebaseErrorToAppError(err.code);

      yield ForgotPasswordFailure(
        err: ErrorResponse([
          ErrorItem(
            errorCode: errorCode,
            description: err.message,
          )
        ]),
      );
    }
  }
}
