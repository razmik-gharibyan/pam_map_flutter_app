// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

@immutable
abstract class ForgotPasswordState extends Equatable {
  const ForgotPasswordState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class ForgotPasswordInitial extends ForgotPasswordState {}

class ForgotPasswordInProgress extends ForgotPasswordState {}

class ForgotPasswordFailure extends ForgotPasswordState {
  final ErrorResponse err;

  const ForgotPasswordFailure({@required this.err});

  @override
  List<Object> get props => [err];
}

class ForgotPasswordSendSuccess extends ForgotPasswordState {}

