// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ForgotPasswordEvent extends Equatable {
  const ForgotPasswordEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class ForgotPasswordStarted extends ForgotPasswordEvent {}

class ForgotPasswordRecoverPressed extends ForgotPasswordEvent {
  final String email;

  const ForgotPasswordRecoverPressed({@required this.email});

  @override
  List<Object> get props => [email];
}
