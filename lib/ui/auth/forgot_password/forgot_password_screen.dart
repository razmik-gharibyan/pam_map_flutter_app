// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../../common/common.dart';
import 'bloc/bloc.dart';

class ForgotPasswordScreen extends StatefulWidget {
  const ForgotPasswordScreen({Key key}) : super(key: key);

  @override
  State<ForgotPasswordScreen> createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _navigationService = injector<NavigationService>();
  GlobalKey<FormState> _formKey;
  TextEditingController _emailController;

  @override
  void initState() {
    super.initState();

    _formKey = GlobalKey<FormState>();
    _emailController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final mediaQuery = MediaQuery.of(context);

    return BlocProvider<ForgotPasswordBloc>(
      create: (_) => ForgotPasswordBloc(),
      child: BlocCommonListener<ForgotPasswordBloc, ForgotPasswordState>(
        inProgress: (state) => state is ForgotPasswordInProgress,
        error: (state) => state is ForgotPasswordFailure ? state.err : null,
        child: BlocListener<ForgotPasswordBloc, ForgotPasswordState>(
          listener: _blocListener,
          child: BlocBuilder<ForgotPasswordBloc, ForgotPasswordState>(
            builder: (context, state) {
              return Scaffold(
                appBar: AppBar(
                  backgroundColor: AppColors.primary,
                  elevation: 0.5,
                  iconTheme: IconThemeData(
                    color: Colors.white, //change your color here
                  ),
                  title: Text(
                    // FIXME: Translate
                    'Forgot Password',
                    textAlign: TextAlign.center,
                    style: textTheme.bodyText1.copyWith(color: AppColors.white),
                  ),
                ),
                body: LayoutBuilder(
                  builder: (ctx, constraints) =>
                      Column(
                        children: [
                          SizedBox(
                            height: constraints.maxHeight * 0.06,
                          ),
                          FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Container(
                              height: constraints.maxHeight * 0.05,
                              child: Text(
                                // FIXME: Translate
                                'Reset password',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: constraints.maxHeight * 0.04,
                          ),
                          Container(
                            height: constraints.maxHeight * 0.1,
                            padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.07),
                            child: Text(
                              // FIXME: Translate
                              'Please enter your email address in a field below and press the send button , and we will send you a link to reset your password',
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                          SizedBox(height: constraints.maxHeight * 0.02),
                          Form(
                            key: _formKey,
                            child: SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: (mediaQuery.size.width * 27) / screenWidthCoefficent,
                                    ),
                                    child: Container(
                                      height: constraints.maxHeight * 0.1,
                                      child: TextFormField(
                                        controller: _emailController,
                                        style: TextStyle(
                                          fontSize: 14,
                                        ),
                                        decoration: InputDecoration(
                                          // FIXME: Translate
                                          labelText: 'Email',
                                          isDense: true,
                                          errorStyle: TextStyle(
                                            fontSize: 12,
                                          ),
                                        ),
                                        keyboardType: TextInputType.emailAddress,
                                        validator: emailValidator(context),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: constraints.maxHeight * 0.01),
                                  Padding(
                                    padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.1),
                                    child: Container(
                                      height: constraints.maxHeight * 0.13,
                                      alignment: Alignment.center,
                                      child: ButtonTheme(
                                        minWidth: (mediaQuery.size.height * 250) / screenHeightCoefficent,
                                        height: (mediaQuery.size.height * 35) / screenHeightCoefficent,
                                        child: RaisedButton(
                                          child: Text(
                                            // FIXME: Translate
                                            'Send',
                                            style: TextStyle(
                                              fontSize: 13,
                                            ),
                                          ),
                                          shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(30),
                                          ),
                                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                          color: AppColors.primary,
                                          textColor: AppColors.white,
                                          splashColor: AppColors.accent,
                                          onPressed: () => _onSendPressed(context),
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(height: constraints.maxHeight * 0.02),
                                  Container(
                                    height: constraints.maxHeight * 0.1,
                                    padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.07),
                                    child: Text(
                                      // FIXME: Translate
                                      '* Use the same email address that you have registered with',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 9,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black54,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }

  void _blocListener(final BuildContext context, final ForgotPasswordState state) {
    if (state is ForgotPasswordSendSuccess) {
      _navigationService.pushNamedAndRemoveUntil('auth', (route) => route.settings.name == 'auth');
    }
  }

  void _onSendPressed(final BuildContext context) {
    if (_formKey.currentState.validate()) {
      context.read<ForgotPasswordBloc>().add(ForgotPasswordRecoverPressed(email: _emailController.text));
    }
  }
}
