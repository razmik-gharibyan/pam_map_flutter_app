// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

@immutable
abstract class AuthLegacyState extends Equatable {
  const AuthLegacyState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class AuthLegacyLoginInitial extends AuthLegacyState {}

class AuthLegacyInProgress extends AuthLegacyState {}

class AuthLegacyFailure extends AuthLegacyState {
  final ErrorResponse err;

  const AuthLegacyFailure({@required this.err});

  @override
  List<Object> get props => [err];
}

class AuthLegacyLoginEmailFound extends AuthLegacyState {}

class AuthLegacyLoginEmailNotFound extends AuthLegacyState {}

class AuthLegacyLoginEmailResendFailure extends AuthLegacyState {
  final ErrorResponse err;

  AuthLegacyLoginEmailResendFailure({@required this.err});

  @override
  List<Object> get props => [err];
}

class AuthLegacyLoginEmailResendSuccess extends AuthLegacyState {}

class AuthLegacyLoginWrongEmailOrPassword extends AuthLegacyState {}

class AuthLegacyLoginSuccess extends AuthLegacyState {
  final String userId;

  const AuthLegacyLoginSuccess({this.userId});

  @override
  List<Object> get props => [userId];
}

class AuthLegacyLoginWithoutInstagramSuccess extends AuthLegacyState {}

class AuthLegacyRegisterSuccess extends AuthLegacyState {}
