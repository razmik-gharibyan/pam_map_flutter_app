// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthLegacyEvent extends Equatable {
  const AuthLegacyEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class AuthLegacyStarted extends AuthLegacyEvent {}

class AuthLegacyResumed extends AuthLegacyEvent {}

class AuthLegacyLoginBackPressed extends AuthLegacyEvent {}

class AuthLegacyLoginPressed extends AuthLegacyEvent {
  final String email;
  final String password;

  const AuthLegacyLoginPressed({
    @required this.email,
    @required this.password,
  });

  @override
  List<Object> get props => [email, password];
}

class AuthLegacyRegisterPressed extends AuthLegacyEvent {
  final String email;
  final String password;

  const AuthLegacyRegisterPressed({
    @required this.email,
    @required this.password,
  });

  @override
  List<Object> get props => [email, password];
}
