// @dart=2.9

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../../../common/common.dart';
import 'auth_legacy_event.dart';
import 'auth_legacy_state.dart';

class AuthLegacyBloc extends Bloc<AuthLegacyEvent, AuthLegacyState> with ErrorMixin {
  static const _longLiveAccessToken = 'long_live_access_token';
  final AuthBloc _authBloc;
  final _userRepository = injector<UserRepository>();
  final _firestoreService = injector<FirestoreService>();
  final _settingsRepository = injector<SettingsRepository>();
  final _instagramService = injector<InstagramService>();
  final _storage = injector<FlutterSecureStorage>();

  AuthLegacyBloc({@required AuthBloc authBloc})
      : assert(authBloc != null),
        _authBloc = authBloc,
        super(AuthLegacyLoginInitial());

  @override
  Stream<AuthLegacyState> mapEventToState(final AuthLegacyEvent event) async* {
    if (event is AuthLegacyStarted) {
      yield* _mapStartedToState();
    } else if (event is AuthLegacyResumed) {
      yield* _mapResumedToState();
    } else if (event is AuthLegacyLoginPressed) {
      yield* _mapLoginPressedToState(email: event.email, password: event.password);
    } else if (event is AuthLegacyRegisterPressed) {
      yield* _mapRegisterPressedToState(event.email, event.password);
    }
  }

  Stream<AuthLegacyState> _mapStartedToState() async* {}

  Stream<AuthLegacyState> _mapResumedToState() async* {
    yield AuthLegacyLoginInitial();
  }

  Stream<AuthLegacyState> _mapLoginPressedToState({
    @required final String email,
    @required final String password,
    final bool shouldUpdateInstagram = true,
  }) async* {
    yield AuthLegacyInProgress();

    try {
      await _userRepository.login(email: email, password: password);

      _authBloc.add(LoggedIn());

      final user = await _userRepository.getUserInfo();

      final userInfo = await _firestoreService.getUserDocument(user.uid);

      if (userInfo.hasInstagram) {
        final accessToken = await _storage.read(key: _longLiveAccessToken);

        if (accessToken != null && shouldUpdateInstagram) {
          _updateInstagramInfo(accessToken, userInfo.instagramInfo.instagramId, user.uid, email, password);
        }

        yield AuthLegacyLoginSuccess(userId: user.uid);
      } else {
        yield AuthLegacyLoginWithoutInstagramSuccess();
      }
    } on FirebaseAuthException catch (err) {
      final errorCode = mapFirebaseErrorToAppError(err.code);

      yield AuthLegacyFailure(
        err: ErrorResponse([
          ErrorItem(
            errorCode: errorCode,
            description: err.message,
          )
        ]),
      );
    } catch (err) {
      yield AuthLegacyFailure(
        err: ErrorResponse([
          ErrorItem(
            errorCode: AppErrorCode.SERVER_ERROR,
            // TODO: Translate
            description: 'Oops something went wrong...',
          )
        ]),
      );
    }
  }

  Stream<AuthLegacyState> _mapRegisterPressedToState(final String email, final String password) async* {
    yield AuthLegacyInProgress();

    try {
      await _userRepository.register(email: email, password: password);

      final user = await _userRepository.getUserInfo();

      final settings = await _settingsRepository.getSettings();

      await _firestoreService.addUserDocument(
        UserProfileInfo(
          id: user.uid,
          border: settings.border,
          cardColor: settings.cardColor,
          isVisible: false,
        ),
        user.uid,
      );

      yield AuthLegacyRegisterSuccess();
    } on FirebaseAuthException catch (err) {
      yield AuthLegacyFailure(
        err: ErrorResponse([
          ErrorItem(
            errorCode: mapFirebaseErrorToAppError(err.code),
            description: err.message,
          )
        ]),
      );
    }
  }

  void _updateInstagramInfo(
    final String accessToken,
    final String instagramId,
    final String userId,
    final String email,
    final String password,
  ) async {
    try {
      final username = await _instagramService.getUsername(int.parse(instagramId), accessToken);

      final instagramProfileInfo = await _instagramService.getProfileInfo(username);

      await _firestoreService.updateUserDocument(
        UserProfileInfo(
          instagramInfo: instagramProfileInfo,
        ),
        userId,
      );
    } catch (err) {
      _mapLoginPressedToState(email: email, password: password, shouldUpdateInstagram: false);
    }
  }
}
