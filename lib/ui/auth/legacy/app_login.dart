// @dart=2.9

import 'package:flutter/material.dart';

import '../../common/common.dart';
import 'login_form.dart';

class AppLogin extends StatelessWidget {
  final VoidCallback onRegisterPressed;

  AppLogin({this.onRegisterPressed});

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Column(
      children: [
        const SizedBox(height: 26),
        LoginForm(),
        const SizedBox(height: 16),
        Column(
          children: [
            Text(
              // FIXME: Translate
              'Doesn`t have account?',
              textAlign: TextAlign.center,
              style: textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold),
            ),
            const SizedBox(height: 16),
            GestureDetector(
              onTap: onRegisterPressed,
              child: Text(
                // FIXME: Translate
                'Sign up',
                textAlign: TextAlign.center,
                style: textTheme.subtitle1.copyWith(
                  fontWeight: FontWeight.bold,
                  color: AppColors.white,
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }
}
