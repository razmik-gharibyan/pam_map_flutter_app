// @dart=2.9

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../common/common.dart';
import 'bloc/bloc.dart';

class RegisterForm extends StatefulWidget {
  final VoidCallback onBackPressed;

  RegisterForm({this.onBackPressed});

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  GlobalKey<FormState> _formKey;

  TextEditingController _emailController;
  TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();

    _formKey = GlobalKey<FormState>();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 46),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 28),
          child: Column(
            children: [
              const SizedBox(height: 20),
              Text(
                // FIXME: Translate
                'Sign up',
                textAlign: TextAlign.center,
                style: textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextFormField(
                          controller: _emailController,
                          style: textTheme.caption,
                          decoration: InputDecoration(
                            // FIXME: Translate
                            labelText: 'Email',
                            isDense: true,
                            errorStyle: textTheme.subtitle2,
                          ),
                          keyboardType: TextInputType.emailAddress,
                          validator: emailValidator(context),
                        ),
                        const SizedBox(height: 26),
                        TextFormField(
                          controller: _passwordController,
                          obscureText: true,
                          style: textTheme.caption,
                          decoration: InputDecoration(
                            // FIXME: Translate
                            labelText: 'Password',
                            isDense: true,
                            errorStyle: textTheme.subtitle2,
                          ),
                          validator: passwordValidator(context),
                        ),
                        const SizedBox(height: 26),
                        TextFormField(
                          obscureText: true,
                          style: textTheme.caption,
                          decoration: InputDecoration(
                            // FIXME: Translate
                            labelText: 'Confirm password',
                            isDense: true,
                            errorStyle: textTheme.subtitle2,
                          ),
                          validator: (value) =>
                              passwordMatchValidator(context).validateMatch(value, _passwordController.text),
                        ),
                        const SizedBox(height: 20),
                        Center(
                          child: ButtonTheme(
                            minWidth: 200,
                            child: RaisedButton(
                              child: Text(
                                // FIXME: Translate
                                'Sign up',
                                style: textTheme.caption.copyWith(
                                  color: AppColors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                              color: AppColors.primary,
                              splashColor: AppColors.accent,
                              onPressed: () => _onRegisterPressed(context),
                            ),
                          ),
                        ),
                        const SizedBox(height: 26),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.arrow_back_ios,
                              color: AppColors.primary,
                              size: 14,
                            ),
                            GestureDetector(
                              onTap: widget.onBackPressed,
                              child: Text(
                                // FIXME: Translate
                                'back to login',
                                style: textTheme.subtitle2,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onRegisterPressed(final BuildContext context) {
    if (_formKey.currentState.validate()) {
      context.read<AuthLegacyBloc>().add(AuthLegacyRegisterPressed(
            email: _emailController.text,
            password: _passwordController.text,
          ));
    }
  }
}
