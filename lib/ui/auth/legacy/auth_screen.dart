// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../../common/common.dart';
import 'app_login.dart';
import 'bloc/bloc.dart';
import 'register_form.dart';

enum AuthMode {
  login,
  register,
}

class AuthScreen extends StatefulWidget {
  @override
  State<AuthScreen> createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> with TickerProviderStateMixin {
  static const _lastUserId = 'last_user_id';

  AnimationController _loginAnimationController;
  AnimationController _registerAnimationController;
  Animation<Offset> _loginOffsetAnimation;
  Animation<Offset> _registerOffsetAnimation;

  final _navigationService = injector<NavigationService>();
  final _storage = injector<FlutterSecureStorage>();

  AuthMode _authMode = AuthMode.login;
  bool _registerFormVisible = false;

  @override
  void initState() {
    super.initState();
    _loginAnimationController = AnimationController(duration: Duration(milliseconds: 800), vsync: this);
    _registerAnimationController = AnimationController(duration: Duration(milliseconds: 800), vsync: this);
    _loginOffsetAnimation = Tween<Offset>(begin: Offset(0, 0), end: Offset(0, -800))
        .animate(CurvedAnimation(parent: _loginAnimationController, curve: Curves.easeIn));
    _registerOffsetAnimation = Tween<Offset>(
      begin: Offset(0, 0),
      end: Offset(0, (620 * (-520)) / screenHeightCoefficent),
    ).animate(
      CurvedAnimation(
        parent: _registerAnimationController,
        curve: Curves.easeIn,
      ),
    );
    _loginOffsetAnimation.addListener(() => setState(() {}));
    _registerOffsetAnimation.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _registerFormVisible = true;
      } else if (status == AnimationStatus.dismissed) {
        _registerFormVisible = false;
      }
    });
  }

  @override
  void dispose() {
    _loginAnimationController.dispose();
    _registerAnimationController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);
    final textTheme = Theme.of(context).textTheme;

    return BlocProvider<AuthLegacyBloc>(
      create: (_) => AuthLegacyBloc(authBloc: context.read<AuthBloc>())..add(AuthLegacyStarted()),
      child: BlocListener<AuthLegacyBloc, AuthLegacyState>(
        listener: _blocListener,
        child: BlocCommonListener<AuthLegacyBloc, AuthLegacyState>(
          inProgress: (state) => state is AuthLegacyInProgress,
          error: (state) => state is AuthLegacyFailure ? state.err : null,
          child: Scaffold(
            appBar: AppBar(backgroundColor: AppColors.primary),
            body: SingleChildScrollView(
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.only(
                      bottomLeft: Radius.elliptical(100, 50),
                      bottomRight: Radius.elliptical(100, 50),
                    ),
                    child: Container(
                      height: 600,
                      decoration: BoxDecoration(
                        gradient: LinearGradient(
                          colors: [
                            AppColors.primary,
                            AppColors.accent,
                          ],
                          begin: const FractionalOffset(0.0, 0.0),
                          end: const FractionalOffset(0.0, 1.0),
                        ),
                      ),
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        appLocalizations.pam,
                        textAlign: TextAlign.center,
                        style: textTheme.headline3.copyWith(
                          color: AppColors.white,
                          fontWeight: FontWeight.bold,
                          letterSpacing: 12,
                        ),
                      ),
                      const SizedBox(height: 10),
                      Image(
                        height: 60,
                        image: AssetImage('assets/images/pam_map_icon_big_white_circle.png'),
                        fit: BoxFit.cover,
                      ),
                      Transform.translate(
                        offset: _loginOffsetAnimation.value,
                        child: AnimatedSwitcher(
                          duration: Duration(seconds: 1),
                          transitionBuilder: (child, animation) => ScaleTransition(
                            child: child,
                            scale: animation,
                          ),
                          child: AppLogin(
                            onRegisterPressed: _swapLoginRegister,
                          ),
                        ),
                      ),
                      Transform.translate(
                        offset: _registerOffsetAnimation.value,
                        child: AnimatedSwitcher(
                          duration: Duration(seconds: 1),
                          transitionBuilder: (child, animation) => ScaleTransition(
                            child: child,
                            scale: animation,
                          ),
                          child: Visibility(
                            visible: _registerFormVisible,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 66),
                              child: RegisterForm(
                                onBackPressed: _swapLoginRegister,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _blocListener(final BuildContext context, final AuthLegacyState state) async {
    if (state is AuthLegacyLoginWithoutInstagramSuccess || state is AuthLegacyRegisterSuccess) {
      final res = await _navigationService.pushNamed('instagram/connect/inform');
      if (res) {
        context.read<AuthLegacyBloc>().add(AuthLegacyStarted());
      }
    } else if (state is AuthLegacyLoginSuccess) {
      final lastUserId = await _storage.read(key: _lastUserId);
      if (lastUserId != null) {
        if (state.userId == lastUserId) {
          _navigationService.pushReplacementNamed('home');
        } else {
          _navigationService.pushNamed('instagram/connect');
        }
      } else {
        _navigationService.pushReplacementNamed('home');
      }
    }
  }

  // TODO: Move register into another screen
  void _swapLoginRegister() {
    if (_authMode == AuthMode.login) {
      _authMode = AuthMode.register;
      _registerFormVisible = true;

      _loginAnimationController.forward();
      _registerAnimationController.forward();
    } else if (_authMode == AuthMode.register) {
      _authMode = AuthMode.login;

      _loginAnimationController.reverse();
      _registerAnimationController.reverse();
    }
  }
}
