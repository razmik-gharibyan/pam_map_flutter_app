// @dart=2.9

import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:provider/provider.dart';

import '../../common/common.dart';
import 'bloc/bloc.dart';

class LoginForm extends StatefulWidget {
  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> with ConfirmationDialogMixin {
  final _navigationService = injector<NavigationService>();
  GlobalKey<FormState> _formKey;
  TextEditingController _emailController;
  TextEditingController _passwordController;

  @override
  void initState() {
    super.initState();
    _formKey = GlobalKey();
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    final appLocalizations = AppLocalizations.of(context);
    final textTheme = Theme.of(context).textTheme;

    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 46),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 1.0), //(x,y)
              blurRadius: 6.0,
            ),
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 28),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(height: 20),
              Center(
                child: Text(
                  appLocalizations.login,
                  textAlign: TextAlign.center,
                  style: textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold),
                ),
              ),
              const SizedBox(height: 20),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 28),
                child: Form(
                  key: _formKey,
                  child: SingleChildScrollView(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextFormField(
                          controller: _emailController,
                          style: textTheme.caption,
                          decoration: InputDecoration(
                            // FIXME: Translate
                            labelText: 'Email',
                            isDense: true,
                            errorStyle: textTheme.subtitle2,
                          ),
                          keyboardType: TextInputType.emailAddress,
                          validator: emailValidator(context),
                        ),
                        const SizedBox(height: 20),
                        TextFormField(
                          controller: _passwordController,
                          obscureText: true,
                          style: textTheme.caption,
                          decoration: InputDecoration(
                              // FIXME: Translate
                              labelText: 'Password',
                              isDense: true,
                              errorStyle: textTheme.subtitle2),
                          validator: passwordValidator(context),
                        ),
                        const SizedBox(height: 26),
                        GestureDetector(
                          child: Text(
                            // FIXME: Translate
                            'Forgot password?',
                            style: textTheme.caption.copyWith(color: AppColors.accent, fontWeight: FontWeight.w500),
                          ),
                          onTap: () => _openForgotPasswordScreen(),
                        ),
                        const SizedBox(height: 20),
                        Center(
                          child: ButtonTheme(
                            minWidth: 200,
                            child: RaisedButton(
                              child: Text(
                                appLocalizations.login,
                                style: textTheme.caption.copyWith(
                                  color: AppColors.white,
                                  fontWeight: FontWeight.w500,
                                ),
                              ),
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30),
                              ),
                              color: AppColors.primary,
                              splashColor: AppColors.accent,
                              onPressed: () => onLoginPressed(context),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void onLoginPressed(final BuildContext context) {
    if (_formKey.currentState.validate()) {
      context.read<AuthLegacyBloc>().add(AuthLegacyLoginPressed(
            email: _emailController.text,
            password: _passwordController.text,
          ));
    }
  }

  void _openForgotPasswordScreen() {
    _navigationService.pushNamed('auth/forgot-password');
  }
}
