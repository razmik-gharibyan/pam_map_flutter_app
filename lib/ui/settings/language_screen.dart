/*import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/screens/tutorial_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageScreen extends StatefulWidget {

  static const routeName = "/language-screen";

  @override
  _LanguageScreenState createState() => _LanguageScreenState();
}

class _LanguageScreenState extends State<LanguageScreen> {

  final _languageList = [
    {"lang": "Afrikaans", "code": "af"}, // Afrikaans
    {"lang": "中文 & 台灣", "code": "zh"}, // Chinese
    {"lang": "Český", "code": "cs"}, // Czech
    {"lang": "Dansk", "code": "da"}, // Danish
    {"lang": "Nederlands", "code": "nl"}, // Nederlands
    {"lang": "Suomi", "code": "fi"}, // Finnish
    {"lang": "Français", "code": "fr"}, // French
    {"lang": "Deutsch", "code": "de"}, // Deutsch
    {"lang": "English", "code": "en"}, // English
    {"lang": "Ελληνικά", "code": "el"}, // Greek
    {"lang": "Bahasa Indonesia", "code": "id"}, // Indonesian
    {"lang": "Italiano", "code": "it"}, // Italian
    {"lang": "日本語", "code": "ja"}, // Japanese
    {"lang": "한국어", "code": "ko"}, // Korean
    {"lang": "Bahasa Melayu", "code": "ms"}, // Malay
    {"lang": "Norsk", "code": "nb"}, // Norwegian
    {"lang": "Polski", "code": "pl"}, // Polish
    {"lang": "Português", "code": "pt"}, // Portuguese
    {"lang": "Русский", "code": "ru"}, // Russian
    {"lang": "Español", "code": "es"}, // Spanish
    {"lang": "Svenska", "code": "sv"}, // Sweden
    {"lang": "Filipino", "code": "tl"}, // Filipino
    {"lang": "ภาษาไทย", "code": "th"}, // Thai
    {"lang": "Türkçe", "code": "tr"}, // Turkish
    {"lang": "العربية", "code": "ar"}, // Arabic
    {"lang": "עִברִית", "code": "he"}, // Hebrew
    {"lang": "हिन्दी", "code": "hi"}, // India
  ];

  @override
  Widget build(BuildContext context) {

    final _mediaQuery = MediaQuery.of(context);

    return Scaffold(
      body: Container(
        width: _mediaQuery.size.width,
        height: _mediaQuery.size.height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Theme.of(context).accentColor,Theme.of(context).primaryColor]
          ),
        ),
        child: Container(
          width: _mediaQuery.size.width * 0.8,
          height: _mediaQuery.size.height * 0.8,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(_mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                spreadRadius: 2,
                blurRadius: 7,
                offset: Offset(0, 2), // changes position of shadow
              ),
            ]
          ),
          child: Column(
            children: [
              Container(
                alignment: Alignment.center,
                height: _mediaQuery.size.height * 0.1,
                child: Text(
                  "Language",
                  style: TextStyle(
                    fontSize: _mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent
                  ),
                  textAlign: TextAlign.center,
                )
              ),
              Container(
                height: _mediaQuery.size.height * 0.7,
                child: ListView.builder(
                    itemBuilder: (ctx, index) => ListTile(
                      title: Text(
                        _languageList[index]["lang"],
                        style: TextStyle(
                            fontSize: _mediaQuery.size.height * 17 / ConstraintHelper.screenHeightCoefficent
                        ),
                      ),
                      onTap: () async {
                        var sharedPref = await SharedPreferences.getInstance();
                        await sharedPref.setBool("firstCall", true);
                        // Change language
                        await sharedPref.setString("language", _languageList[index]["code"]);
                        EasyLocalization.of(context).locale = Locale(_languageList[index]["code"]);
                        Navigator.of(context).pushReplacementNamed(TutorialScreen.routeName);
                      },
                    ),
                  itemCount: _languageList.length,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

 */
