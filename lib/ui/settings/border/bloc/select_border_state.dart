// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:quiver/core.dart';

@immutable
class SelectBorderState extends Equatable {
  final UserProfileInfo currentUser;
  final List<BorderResponse> borders;
  final bool inProgress;
  final Optional<ErrorResponse> err;

  @override
  List<Object> get props => [
        currentUser,
        borders,
        inProgress,
        err,
      ];

  @override
  bool get stringify => true;

  const SelectBorderState({
    this.currentUser,
    this.borders,
    this.inProgress,
    this.err,
  });

  factory SelectBorderState.initial() {
    return SelectBorderState(
      borders: [],
      inProgress: false,
      err: Optional.absent(),
    );
  }

  SelectBorderState loading() {
    return copyWith(inProgress: true);
  }

  SelectBorderState failure(final ErrorResponse err) {
    return copyWith(err: Optional<ErrorResponse>.of(err));
  }

  SelectBorderState copyWith({
    UserProfileInfo currentUser,
    List<BorderResponse> borders,
    bool inProgress,
    Optional<ErrorResponse> err,
  }) {
    return SelectBorderState(
      borders: borders ?? this.borders,
      currentUser: currentUser ?? this.currentUser,
      inProgress: inProgress ?? false,
      err: err ?? Optional<ErrorResponse>.absent(),
    );
  }
}
