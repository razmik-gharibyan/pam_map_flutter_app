// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

@immutable
abstract class SelectBorderEvent extends Equatable {
  const SelectBorderEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class SelectBorderStarted extends SelectBorderEvent {}

class SelectBorderUpdated extends SelectBorderEvent {
  final BorderResponse border;

  const SelectBorderUpdated({this.border});

  @override
  List<Object> get props => [border];
}
