// @dart=2.9

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import 'select_border_event.dart';
import 'select_border_state.dart';

class SelectBorderBloc extends Bloc<SelectBorderEvent, SelectBorderState> {
  final FirestoreService firestoreService;
  final UserRepository userRepository;
  final SettingsRepository settingsRepository;

  SelectBorderBloc({
    @required this.firestoreService,
    @required this.userRepository,
    @required this.settingsRepository,
  })  : assert(firestoreService != null),
        assert(userRepository != null),
        assert(settingsRepository != null),
        super(SelectBorderState.initial());

  @override
  Stream<SelectBorderState> mapEventToState(final SelectBorderEvent event) async* {
    if (event is SelectBorderStarted) {
      yield* _mapStartedToState();
    } else if (event is SelectBorderUpdated) {
      yield* _mapBorderUpdatedToState(event.border);
    }
  }

  Stream<SelectBorderState> _mapStartedToState() async* {
    yield state.loading();

    try {
      final user = await userRepository.getUserInfo();

      final userProfileInfo = await firestoreService.getUserDocument(user.uid);

      final borders = await settingsRepository.getBorders();

      yield state.copyWith(
        currentUser: userProfileInfo,
        borders: borders,
      );
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }

  Stream<SelectBorderState> _mapBorderUpdatedToState(final BorderResponse border) async* {
    yield state.loading();

    try {
      final user = await userRepository.getUserInfo();

      await firestoreService.updateUserDocument(UserProfileInfo(border: border), user.uid);

      final userProfileInfo = await firestoreService.getUserDocument(user.uid);

      final borders = await settingsRepository.getBorders();

      yield state.copyWith(
        currentUser: userProfileInfo,
        borders: borders,
      );
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }
}
