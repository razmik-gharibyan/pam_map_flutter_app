// @dart=2.9

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../../common/common.dart';
import 'bloc/bloc.dart';

class SelectBorderScreen extends StatelessWidget {
  final _firestoreService = injector<FirestoreService>();
  final _userRepository = injector<UserRepository>();
  final _settingsRepository = injector<SettingsRepository>();

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return BlocProvider<SelectBorderBloc>(
      create: (_) => SelectBorderBloc(
        firestoreService: _firestoreService,
        userRepository: _userRepository,
        settingsRepository: _settingsRepository,
      )..add(SelectBorderStarted()),
      child: BlocCommonListener<SelectBorderBloc, SelectBorderState>(
        inProgress: (state) => state.inProgress,
        error: (state) => state.err.isPresent ? state.err.value : null,
        child: BlocBuilder<SelectBorderBloc, SelectBorderState>(
          builder: (context, state) {
            bool canBuildSelectBorder() => state.currentUser != null;

            return Scaffold(
              backgroundColor: AppColors.lightestGray2,
              appBar: AppBar(
                backgroundColor: AppColors.white,
                elevation: 0.5,
                title: Center(
                  child: Text(
                    // FIXME: Translate
                    'People Around Me',
                    style: textTheme.bodyText1.copyWith(color: AppColors.accent),
                  ),
                ),
              ),
              body: canBuildSelectBorder()
                  ? SingleChildScrollView(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const SizedBox(height: 10),
                          FittedBox(
                            fit: BoxFit.fill,
                            child: state.currentUser.border.haveBorder
                                ? CircleAvatar(
                                    radius: 60,
                                    backgroundColor: Colors.transparent,
                                    backgroundImage: CachedNetworkImageProvider(state.currentUser.border.url),
                                    child: CircleAvatar(
                                      backgroundImage:
                                          CachedNetworkImageProvider(state.currentUser.instagramInfo.profilePictureUrl),
                                      radius: 50,
                                    ),
                                  )
                                : CircleAvatar(
                                    backgroundImage:
                                        CachedNetworkImageProvider(state.currentUser.instagramInfo.profilePictureUrl),
                                    radius: 50,
                                  ),
                          ),
                          const SizedBox(height: 10),
                          Divider(),
                          const SizedBox(height: 15),
                          GridView.builder(
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                            itemCount: state.borders.length,
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    state.borders[index].haveBorder
                                        ? FittedBox(
                                            fit: BoxFit.fill,
                                            child: CircleAvatar(
                                              backgroundColor: Colors.white,
                                              backgroundImage: CachedNetworkImageProvider(state.borders[index].url),
                                              radius: 20,
                                            ),
                                          )
                                        : CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            radius: 20,
                                          ),
                                    const SizedBox(height: 8),
                                    Container(
                                      alignment: Alignment.center,
                                      child: state.currentUser.border.name == state.borders[index].name
                                          ? Icon(
                                              Icons.check_circle,
                                              color: AppColors.primary,
                                              size: 16,
                                            )
                                          : FittedBox(
                                              fit: BoxFit.fill,
                                              child: Text(
                                                state.borders[index].name,
                                                style: textTheme.bodyText2.copyWith(color: AppColors.text),
                                                textAlign: TextAlign.center,
                                              ),
                                            ),
                                    ),
                                  ],
                                ),
                                onTap: () => _updateBorder(context, state.borders[index]),
                              );
                            },
                          ),
                        ],
                      ),
                    )
                  : Container(),
            );
          },
        ),
      ),
    );
  }

  void _updateBorder(final BuildContext context, final BorderResponse border) {
    context.read<SelectBorderBloc>().add(SelectBorderUpdated(border: border));
  }
}
