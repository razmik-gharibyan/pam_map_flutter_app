// @dart=2.9

import 'border/border.dart';

final settingsRoutes = {
  'settings/border': (_) => SelectBorderScreen(),
};