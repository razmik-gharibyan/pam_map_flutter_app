// @dart=2.9
/*
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/model/slide.dart';
import 'package:pam_map_flutter_app/screens/auth_screen.dart';
import 'package:pam_map_flutter_app/widgets/tutorial/slide_dot.dart';
import 'package:pam_map_flutter_app/widgets/tutorial/slider_item.dart';
import 'package:shared_preferences/shared_preferences.dart';

class TutorialScreen extends StatefulWidget {

  static const routeName = "/tutorial-screen";

  @override
  _TutorialScreenState createState() => _TutorialScreenState();
}

class _TutorialScreenState extends State<TutorialScreen> {

  // Tools
  var _pageController = PageController(initialPage: 0);
  // Vars
  int _currentIndex = 0;

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final _mediaQuery = MediaQuery.of(context);

    return Scaffold(
      body: Container(
        width: _mediaQuery.size.width,
        height: _mediaQuery.size.height,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Theme.of(context).accentColor,Theme.of(context).primaryColor]
          ),
        ),
        child: Container(
          width: _mediaQuery.size.width * 0.85,
          alignment: Alignment.center,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: double.infinity,
                height: _mediaQuery.size.height * 0.07,
                margin: EdgeInsets.only(top: _mediaQuery.size.height * 0.02),
                alignment: Alignment.bottomRight,
                child: InkWell(
                  child: Text(
                    tr("tutorial.buttons.skip_btn"),
                    textAlign: TextAlign.center,
                    softWrap: true,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: _mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  onTap: () async {
                    var sharedPref = await SharedPreferences.getInstance();
                    await sharedPref.setBool("firstCall", false);
                    Navigator.of(context).pushReplacementNamed(AuthScreen.routeName);
                  },
                ),
              ),
              Container(
                width: _mediaQuery.size.width * 0.85,
                height: _mediaQuery.size.height * 0.81,
                padding: EdgeInsets.symmetric(vertical: _mediaQuery.size.height * 0.05),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(_mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent)
                ),
                child: PageView.builder(
                  controller: _pageController,
                  scrollDirection: Axis.horizontal,
                  onPageChanged: _onPageChanged,
                  itemBuilder: (ctx, index) => SliderItem(index),
                  itemCount: slideList.length,
                ),
              ),
              Container(
                width: _mediaQuery.size.width * 0.85,
                height: _mediaQuery.size.height * 0.1,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      alignment: Alignment.center,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          for(int i = 0; i < slideList.length; i++)
                            if(i == _currentIndex)
                              SlideDot(true)
                            else
                              SlideDot(false)
                        ],
                      ),
                    ),
                    InkWell(
                      child: Container(
                        alignment: Alignment.centerRight,
                        child: _currentIndex == 5 ? Text(
                          tr("tutorial.buttons.get_started_btn"),
                          softWrap: true,
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: _mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
                              fontWeight: FontWeight.bold
                          ),
                        ) : null
                      ),
                      onTap: () async {
                        var sharedPref = await SharedPreferences.getInstance();
                        await sharedPref.setBool("firstCall", false);
                        Navigator.of(context).pushReplacementNamed(AuthScreen.routeName);
                      },
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _onPageChanged(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

}
*/