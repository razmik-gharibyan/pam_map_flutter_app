// @dart=2.9

import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/common/common.dart';

class SlideDot extends StatelessWidget {
  final bool _isDotActive;

  SlideDot(this._isDotActive);

  @override
  Widget build(BuildContext context) {
    final _mediaQuery = MediaQuery.of(context);

    return AnimatedContainer(
      duration: Duration(milliseconds: 150),
      margin: EdgeInsets.symmetric(horizontal: _mediaQuery.size.height * 10 / screenHeightCoefficent),
      width: _isDotActive
          ? _mediaQuery.size.height * 12 / screenHeightCoefficent
          : _mediaQuery.size.height * 8 / screenHeightCoefficent,
      height: _isDotActive
          ? _mediaQuery.size.height * 12 / screenHeightCoefficent
          : _mediaQuery.size.height * 8 / screenHeightCoefficent,
      decoration: BoxDecoration(
          color: _isDotActive ? Colors.white : Colors.grey,
          borderRadius: BorderRadius.all(Radius.circular(_mediaQuery.size.height * 12 / screenHeightCoefficent))),
    );
  }
}
