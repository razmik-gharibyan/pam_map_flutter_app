// @dart=2.9

import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/ui/tutorial/slide.dart';

class SliderItem extends StatelessWidget {

  final int _index;

  SliderItem(this._index);

  @override
  Widget build(BuildContext context) {

    final _mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(
      builder: (ctx, constraints) => Container(
        width: constraints.maxWidth,
        height: constraints.maxHeight,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: constraints.maxHeight * 0.75,
              alignment: Alignment.center,
              child: Image.asset(
                  slideList[_index].imagePath,
              ),
            ),
            Container(
              height: constraints.maxHeight * 0.25,
              child: Scrollbar(
                child: SingleChildScrollView(
                  child: Text(
                    slideList[_index].title,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: _mediaQuery.size.height * 15 / screenHeightCoefficent,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
