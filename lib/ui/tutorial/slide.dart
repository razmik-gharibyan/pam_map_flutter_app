// @dart=2.9

import 'package:flutter/cupertino.dart';

class Slide {

  final String imagePath;
  final String title;

  Slide({
    @required this.imagePath,
    @required this.title
  });

}
// FIXME: Translate
final slideList = [
  Slide(
      imagePath: "assets/images/tutorial/map_tutorial.png",
      title: 'PAM Map allows you to see Instagram users around the world in real time and interact with them'
  ),
  Slide(
      imagePath: "assets/images/tutorial/instagram_tutorial.png",
      title: 'To be able to fully use application, you need to create PAM Map account and connect it with Instagram account'
  ),
  Slide(
      imagePath: "assets/images/tutorial/location_tutorial.png",
      title: 'Enable location option, if you want other users to see you on map, or use map in invisible mode, where you can see all users ,while remaining invisible on map'
  ),
  Slide(
      imagePath: "assets/images/tutorial/userlist_tutorial.png",
      title: 'You can also see all nearby users as a list of users, where you can see information about them in detail, and open their Instagram profile'
  ),
  Slide(
      imagePath: "assets/images/tutorial/customize_tutorial.png",
      title: 'To be unique on map, you can customize your Instagram profile picture appearance by adding decorations'
  ),
  Slide(
      imagePath: "assets/images/tutorial/last_tutorial.png",
      title: 'Tap on Get Started button to start using PAM Map application'
  ),
];