// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pam_map_flutter_app/ui/common/custom_icons.dart';

import '../common/common.dart';
import 'app_tab.dart';
import 'bloc/bloc.dart';
import 'map/map.dart';
import 'settings/settings.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final topText = "People Around Me";
  final _tabs = [
    MapTab(),
    SettingsTab(),
  ];

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => HomeBloc()..add(HomeStarted()),
        ),
      ],
      child: BlocListener<LifecycleBloc, LifecycleState>(
        listener: _lifecycleListener,
        child: BlocBuilder<HomeBloc, HomeState>(
          builder: (context, state) {
            return SafeArea(
              child: Scaffold(
                appBar: AppBar(
                  backgroundColor: AppColors.white,
                  elevation: 0.5,
                  title: Center(
                    child: Text(
                      topText,
                      style: textTheme.bodyText1.copyWith(color: AppColors.accent),
                    ),
                  ),
                ),
                body: _buildTab(context, state),
                bottomNavigationBar: _buildBottomNavigationBar(context, state),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _buildTab(final BuildContext context, final HomeState state) {
    return _tabs.elementAt(AppTab.values.indexOf(state.activeTab));
  }

  Widget _buildBottomNavigationBar(final BuildContext context, final HomeState state) {
    final textTheme = Theme.of(context).textTheme;
    final iconContainerHeight = 25.0;

    return BottomNavigationBar(
      type: BottomNavigationBarType.shifting,
      currentIndex: AppTab.values.indexOf(state.activeTab),
      backgroundColor: AppColors.lightestGray3,
      selectedItemColor: AppColors.accent,
      selectedFontSize: textTheme.bodyText2.fontSize,
      unselectedFontSize: textTheme.bodyText2.fontSize,
      unselectedItemColor: AppColors.gray1,
      onTap: (index) => _onNavigationItemTapped(context, AppTab.values[index]),
      showSelectedLabels: false,
      items: [
        BottomNavigationBarItem(
          // FIXME: Translate
          label: 'Map',
          icon: SizedBox(
            height: iconContainerHeight,
            child: Icon(CustomIcon.globe_americas),
          ),
        ),
        BottomNavigationBarItem(
          // FIXME: Translate
          label: 'Settings',
          icon: SizedBox(
            height: iconContainerHeight,
            child: Icon(Icons.account_circle),
          ),
        ),
      ],
    );
  }

  void _onNavigationItemTapped(final BuildContext context, final AppTab activeTab) {
    context.read<HomeBloc>().add(HomeStarted(activeTab: activeTab));
  }

  void _lifecycleListener(final BuildContext context, final LifecycleState state) {
    if (state is LifecycleInactive || state is LifecycleDetached) {
      context.read<HomeBloc>().add(HomeLifecycleStopped());
    } else if (state is LifecycleResumed) {
      // FIXME: Give isVisible a proper value from apps state of current visible switch position
      context.read<HomeBloc>().add(HomeLifecycleResumed(isVisible: false));
    }
  }
}
