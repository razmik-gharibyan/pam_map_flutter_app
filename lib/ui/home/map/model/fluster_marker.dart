// @dart=2.9

import 'package:fluster/fluster.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';

class FlusterMarker extends Clusterable {
  final String id;
  final LatLng position;
  final InfoWindow infoWindow;
  final BitmapDescriptor icon;

  FlusterMarker({
    @required this.id,
    @required this.position,
    this.icon,
    this.infoWindow,
    isCluster = false,
    clusterId,
    pointsSize,
    childMarkerId,
  }) : super(
          markerId: id,
          latitude: position.latitude,
          longitude: position.longitude,
          isCluster: isCluster,
          clusterId: clusterId,
          pointsSize: pointsSize,
          childMarkerId: childMarkerId,
        );

  Marker get toMarker => Marker(
        markerId: MarkerId(isCluster ? 'cl_$id' : id),
        position: position,
        icon: icon,
        infoWindow: infoWindow,
      );
}
