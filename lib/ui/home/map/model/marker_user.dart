// @dart=2.9

import 'package:fluster/fluster.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

class MarkerUser extends Clusterable {
  final String id;
  final LatLng position;
  final UserProfileInfo user;
  final InfoWindow infoWindow;
  final BitmapDescriptor icon;

  MarkerUser({
    @required this.id,
    @required this.position,
    this.user,
    this.icon,
    this.infoWindow,
    isCluster = false,
    clusterId,
    pointsSize,
    childMarkerId,
  }) : super(
    markerId: id,
    latitude: position.latitude,
    longitude: position.longitude,
    isCluster: isCluster,
    clusterId: clusterId,
    pointsSize: pointsSize,
    childMarkerId: childMarkerId,
  );

  MarkerUser copyWith({
    String id,
    LatLng position,
    UserProfileInfo user,
    InfoWindow infoWindow,
    BitmapDescriptor icon,
  }) {
    return MarkerUser(
      id: id ?? this.id,
      position: position ?? this.position,
      user: user ?? this.user,
      icon: icon ?? this.icon,
      infoWindow: infoWindow ?? this.infoWindow,
    );
  }

  Marker get toMarker => Marker(
    markerId: MarkerId(isCluster ? 'cl_$id' : id),
    position: position,
    icon: icon,
    infoWindow: infoWindow,
  );
}
