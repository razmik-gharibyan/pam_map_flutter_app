// @dart=2.9

import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pam_map_flutter_app/common/common.dart';

import '../../common/common.dart';
import 'bloc/bloc.dart';

class MapTab extends StatefulWidget {
  @override
  _MapTabState createState() => _MapTabState();
}

class _MapTabState extends State<MapTab> with WidgetsBindingObserver, ConfirmationDialogMixin, LocationPermissionMixin {
  final initLatitude = 40.790124;
  final initLongitude = -73.9537724;

  GoogleMapController _controller;
  Timer _timer;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _timer?.cancel();
    _controller?.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final mediaQuery = MediaQuery.of(context);

    return BlocProvider<MapBloc>(
      create: (_) => MapBloc()..add(MapStarted()),
      child: BlocListener<MapBloc, MapState>(
        listener: _blocListener,
        child: BlocBuilder<MapBloc, MapState>(
          builder: (context, state) {
            bool canBuildMap() => state.currentUser != null;

            if (canBuildMap()) {
              return Container(
                child: Stack(
                  children: [
                    GoogleMap(
                      markers: Set.of(state.markers),
                      mapType: MapType.normal,
                      initialCameraPosition: CameraPosition(
                        target: state.currentUser.hasLocation
                            ? LatLng(state.currentUser.location.latitude, state.currentUser.location.longitude)
                            : LatLng(initLatitude, initLongitude),
                        zoom: 16,
                      ),
                      zoomGesturesEnabled: true,
                      zoomControlsEnabled: false,
                      gestureRecognizers: <Factory<OneSequenceGestureRecognizer>>[
                        Factory<OneSequenceGestureRecognizer>(() => EagerGestureRecognizer()),
                      ].toSet(),
                      onMapCreated: (controller) async {
                        _controller = controller;

                        _startVisibleRegionUserSearch(context, _controller);
                      },
                    ),
                    Container(
                      child: IconButton(
                        icon: state.visibility ? Icon(Icons.visibility) : Icon(Icons.visibility_off),
                        color: Theme.of(context).accentColor,
                        onPressed: () => onVisibilityPressed(context, state),
                        iconSize: mediaQuery.size.height * 30 / screenHeightCoefficent,
                      ),
                      alignment: Alignment.topRight,
                    )
                  ],
                ),
              );
            }

            return Container();
          },
        ),
      ),
    );
  }

  void _blocListener(final BuildContext context, final MapState state) {
    if (state.shouldUpdateCameraPosition && state.currentUser.location != null) {
      _updateCameraPosition(LatLng(
        state.currentUser.location.latitude,
        state.currentUser.location.longitude,
      ));
    }
  }

  void _startVisibleRegionUserSearch(final BuildContext context, final GoogleMapController controller) async {
    _timer = Timer.periodic(Duration(milliseconds: 1500), (timer) async {
      final visibleRegion = await controller.getVisibleRegion();

      context.read<MapBloc>().add(MapVisibleRegionChanged(
            visibleRegion: visibleRegion,
            currentZoom: await controller.getZoomLevel(),
          ));
    });
  }

  void onVisibilityPressed(final BuildContext context, final MapState state) async {
    if (state.visibility) {
      context.read<MapBloc>().add(MapVisibilityChanged(
            visibility: !state.visibility,
            shouldUpdateCameraPosition: true,
          ));
    } else {
      _requestLocationUpdates(context, state);
    }
  }

  void _requestLocationUpdates(final BuildContext context, final MapState state) async {
    await promptLocation(
      context: context,
      onLocationServiceEnabled: () {
        _startLocationUpdates(context);
        context.read<MapBloc>().add(MapVisibilityChanged(
              visibility: !state.visibility,
              shouldUpdateCameraPosition: true,
            ));
      },
      onLocationServiceDisabled: () {
        _requestLocationUpdates(context, state);
      },
      onLocationServiceCanceled: () {},
      onPermissionNotGranted: () {},
    );
  }

  void _startLocationUpdates(final BuildContext context) {
    BlocProvider.of<LocationBloc>(context).add(LocationStarted());
  }

  void _updateCameraPosition(final LatLng latLng) {
    _controller.animateCamera(CameraUpdate.newLatLng(latLng));
  }
}
