// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

@immutable
abstract class MapEvent extends Equatable {
  const MapEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class MapStarted extends MapEvent {}

class MapVisibleRegionChanged extends MapEvent {
  final LatLngBounds visibleRegion;
  final double currentZoom;

  const MapVisibleRegionChanged({
    this.visibleRegion,
    this.currentZoom,
  });

  @override
  List<Object> get props => [visibleRegion, currentZoom];
}

class MapUsersUpdated extends MapEvent {
  final List<UserProfileInfo> users;

  const MapUsersUpdated({this.users});

  @override
  List<Object> get props => [users];
}

class MapVisibilityChanged extends MapEvent {
  final bool visibility;
  final bool shouldUpdateCameraPosition;

  const MapVisibilityChanged({
    this.visibility,
    this.shouldUpdateCameraPosition,
  });

  @override
  List<Object> get props => [visibility, shouldUpdateCameraPosition];
}
