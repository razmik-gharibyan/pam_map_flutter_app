// @dart=2.9

import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluster/fluster.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../model/model.dart';
import 'map_event.dart';
import 'map_state.dart';

class MapBloc extends Bloc<MapEvent, MapState> {
  final _firestoreService = injector<FirestoreService>();
  final _userRepository = injector<UserRepository>();

  MapBloc() : super(MapState.initial());

  @override
  Stream<MapState> mapEventToState(final MapEvent event) async* {
    if (event is MapStarted) {
      yield* _mapStartedToState();
    } else if (event is MapVisibleRegionChanged) {
      yield* _mapVisibleRegionChangedToState(event.visibleRegion, event.currentZoom);
    } else if (event is MapVisibilityChanged) {
      yield* _mapVisibilityChangedToState(event.visibility, event.shouldUpdateCameraPosition);
    }
  }

  Stream<MapState> _mapStartedToState() async* {
    try {
      final user = await _userRepository.getUserInfo();

      final userProfileInfo = await _firestoreService.getUserDocument(user.uid);

      yield state.copyWith(
        currentUser: userProfileInfo,
        visibility: userProfileInfo.isVisible,
      );
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }

  Stream<MapState> _mapVisibleRegionChangedToState(final LatLngBounds visibleRegion, final double currentZoom) async* {
    try {
      final neLongitude = visibleRegion.northeast.longitude;
      final neLatitude = visibleRegion.northeast.latitude;
      final swLongitude = visibleRegion.southwest.longitude;
      final swLatitude = visibleRegion.southwest.latitude;
      final lowGeoPoint = GeoPoint(swLatitude, swLongitude);
      final highGeoPoint = GeoPoint(neLatitude, neLongitude);

      final users = await _firestoreService.findUsersInVisibleRegion(lowGeoPoint, highGeoPoint);

      yield* _mapUsersUpdatedToState(users, currentZoom);
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }

  Stream<MapState> _mapVisibilityChangedToState(final bool visibility, final bool shouldUpdateCameraPosition) async* {
    try {
      final user = await _userRepository.getUserInfo();

      await _firestoreService.updateUserDocument(
        UserProfileInfo(isVisible: visibility),
        user.uid,
      );

      final userProfileInfo = await _firestoreService.getUserDocument(user.uid);

      yield state.copyWith(
        visibility: visibility,
        currentUser: userProfileInfo,
        shouldUpdateCameraPosition: shouldUpdateCameraPosition,
      );
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }

  Stream<MapState> _mapUsersUpdatedToState(final List<UserProfileInfo> users, final double currentZoom) async* {
    List<MarkerUser> markerUsers;
    if (users.isEmpty) {
      markerUsers = [];
    } else {
      // TODO: Might be too heavy operation
      markerUsers = await Future.wait(users.map((user) async => await createRawMapMarker(user)).toList());
    }
    Fluster<MarkerUser> fluster = initFluster(markerUsers, 0, 19);

    yield state.copyWith(markers: getMarkers(fluster, currentZoom));
  }
}
