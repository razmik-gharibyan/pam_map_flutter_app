// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:quiver/core.dart';

import '../model/model.dart';

@immutable
class MapState extends Equatable {
  final UserProfileInfo currentUser;
  final List<MarkerUser> markerUsers;
  final List<Marker> markers;
  final bool visibility;
  final bool shouldUpdateCameraPosition;
  final bool inProgress;
  final Optional<ErrorResponse> err;

  MarkerUser getMinimumFollowersMarkerUser() {
    markerUsers.sort((a, b) => a.user.instagramInfo.followers);
    return markerUsers.first;
  }

  @override
  List<Object> get props => [
        currentUser,
        markerUsers,
        markers,
        visibility,
        shouldUpdateCameraPosition,
        inProgress,
        err,
      ];

  @override
  bool get stringify => true;

  const MapState({
    this.currentUser,
    this.markerUsers,
    this.markers,
    this.visibility,
    this.shouldUpdateCameraPosition,
    this.inProgress,
    this.err,
  });

  factory MapState.initial() {
    return MapState(
      markerUsers: [],
      markers: [],
      visibility: false,
      shouldUpdateCameraPosition: false,
      inProgress: false,
      err: Optional.absent(),
    );
  }

  MapState loading() {
    return copyWith(inProgress: true);
  }

  MapState failure(final ErrorResponse err) {
    return copyWith(err: Optional<ErrorResponse>.of(err));
  }

  MapState copyWith({
    UserProfileInfo currentUser,
    List<MarkerUser> markerUser,
    List<Marker> markers,
    bool visibility,
    bool shouldUpdateCameraPosition,
    bool inProgress,
    Optional<ErrorResponse> err,
  }) {
    return MapState(
      currentUser: currentUser ?? this.currentUser,
      markerUsers: markerUser ?? this.markerUsers,
      markers: markers ?? this.markers,
      visibility: visibility ?? this.visibility,
      shouldUpdateCameraPosition: shouldUpdateCameraPosition ?? false,
      inProgress: inProgress ?? false,
      err: err ?? Optional<ErrorResponse>.absent(),
    );
  }
}
