/*
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/core/model/user_profile_info.dart';
import 'package:pam_map_flutter_app/providers/auth_provider.dart';
import 'package:pam_map_flutter_app/providers/back_provider.dart';
import 'package:pam_map_flutter_app/providers/firestore_provider.dart';
import 'package:pam_map_flutter_app/providers/location_provider.dart';
import 'package:pam_map_flutter_app/providers/main_carcas_provider.dart';
import 'package:pam_map_flutter_app/providers/map_provider.dart';
import 'package:pam_map_flutter_app/providers/visibility_provider.dart';
import 'package:pam_map_flutter_app/screens/auth_screen.dart';
import 'package:pam_map_flutter_app/widgets/main_carcas/account_widgets/change_border_widget.dart';
import 'package:pam_map_flutter_app/widgets/main_carcas/account_widgets/language_widget.dart';
import 'package:provider/provider.dart';

class AccountListView extends StatelessWidget {

  final border = 'Border';
  final findMe = 'Find me';
  final language = 'language';
  final delete = 'delete';
  final logout = 'logout';
  List<Map<String,dynamic>> _itemList = [
    {"title": tr("account.item_list.change_border"), "icon": Icons.person_pin},
    {"title": tr("account.item_list.find_me"), "icon": Icons.my_location},
    {"title": tr("account.item_list.language"), "icon": Icons.language},
    {"title": tr("account.item_list.delete_account"), "icon": Icons.delete},
    {"title": tr("account.item_list.log_out"), "icon": Logout.logout},
  ];

  final List<Map<String,dynamic>> _itemList;
  Function notifyParent;
  Function refreshMainCarcas;

  AccountListView(this._itemList,this.notifyParent,this.refreshMainCarcas);

  // Singletons
  var _currentPamMapFirestoreUser = UserProfileInfo();
  // Tools
  final _mapProvider = MapProvider();
  final _mainCarcasProvider = MainCarcasProvider();
  final _authProvider = AuthProvider();
  final _locationProvider = LocationProvider();
  final _visibilityProvider = VisibilityProvider();

  @override
  Widget build(BuildContext context) {

    final mediaQuery = MediaQuery.of(context);
    var _currentItem;

    return LayoutBuilder(
      builder: (c,constraint) => ListView.builder(
       itemBuilder: (ctx, index) => InkWell(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: EdgeInsets.only(left: constraint.maxWidth * 0.05),
                  child: Text(
                    _itemList[index]["title"],
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(right: constraint.maxWidth * 0.05),
                  child: Icon(
                    _itemList[index]["icon"],
                    color: Colors.white,
                    size: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                  ),
                )
              ],
              ),
              Divider()
            ],
          ),
          onTap: () {
            switch (index) {
              case 0:
                // Open Change border widget
                _currentItem = ChangeBorderWidget();
                notifyParent(_currentItem);
                break;
              case 1:
                // Move user to it's last known location
                Map<String,dynamic> position = _currentPamMapFirestoreUser.data["location"];
                _mapProvider.currentLocation = LatLng(position["latitude"], position["longitude"]);
                _mapProvider.initCameraPosition = CameraPosition(target: _mapProvider.currentLocation, zoom: 19);
                _mainCarcasProvider.pageIndex = 0; // Navigate to map p
                refreshMainCarcas();
                break;
              case 2:
                // Open Language menu
                _currentItem = LanguageWidget();
                notifyParent(_currentItem);
                break;
              case 3:
                // Delete user account and navigate to authScreen
                _showDeleteDialogBox(context);
                break;
              case 4:
                // Logout user and navigate to authScreen
                _locationProvider.stopLocationUpdates();
                _authProvider.logOutUser();
                _visibilityProvider.isMapEntered = false;
                Navigator.of(context).pushReplacementNamed(AuthScreen.routeName);
                Provider.of<BackProvider>(context,listen: false).setRegisterConnectAccountPressed(false);
                Provider.of<BackProvider>(context,listen: false).setBackPressed(true);
                break;
            }
          },
        ),
        itemCount: _itemList.length,
      ),
    );
  }

  void _showDeleteDialogBox(BuildContext context) {
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) =>
        Platform.isAndroid ? AlertDialog(
          title: Text(tr("account.delete.title")),
          content: Text(tr("account.delete.content")),
          actions: [
            FlatButton(
                child: Text(tr("account.delete.cancel")),
                onPressed: () {
                  Navigator.of(context).pop();
                }
            ),
            FlatButton(
                child: Text(
                  tr("account.delete.delete"),
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () async{
                  _locationProvider.stopLocationUpdates();
                  _visibilityProvider.isMapEntered = false;
                  //await _firestoreProvider.deleteInstagramAccount();
                  await _authProvider.logOutUser();
                  await _authProvider.deleteUser();
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacementNamed(AuthScreen.routeName);
                  Provider.of<BackProvider>(context,listen: false).setRegisterConnectAccountPressed(false);
                  Provider.of<BackProvider>(context,listen: false).setBackPressed(true);
            })
          ],
        )
        : CupertinoAlertDialog(
          title: Text(tr("account.delete.title")),
          content: Text(tr("account.delete.content")),
          actions: [
            FlatButton(
                child: Text(tr("account.delete.cancel")),
                onPressed: () {
                  Navigator.of(context).pop();
                }
            ),
            FlatButton(
                child: Text(
                  tr("account.delete.delete"),
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () async{
                  _locationProvider.stopLocationUpdates();
                  _authProvider.logOutUser();
                  _visibilityProvider.isMapEntered = false;
                  //await _firestoreProvider.deleteInstagramAccount();
                  await _authProvider.logOutUser();
                  await _authProvider.deleteUser();
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacementNamed(AuthScreen.routeName);
                  Provider.of<BackProvider>(context,listen: false).setRegisterConnectAccountPressed(false);
                  Provider.of<BackProvider>(context,listen: false).setBackPressed(true);
            })
          ],
        )
    );
  }

}

 */
