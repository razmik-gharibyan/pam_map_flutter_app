// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../../common/common.dart';
import 'bloc/bloc.dart';

class SettingsTab extends StatelessWidget {
  final _navigationService = injector<NavigationService>();
  final _dialogService = injector<DialogService>();

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final mediaQuery = MediaQuery.of(context);

    return BlocProvider<SettingsBloc>(
      create: (context) => SettingsBloc()..add(SettingsStarted()),
      child: BlocCommonListener<SettingsBloc, SettingsState>(
        inProgress: (state) => state.inProgress,
        error: (state) => state.err.isPresent ? state.err.value : null,
        child: BlocBuilder<SettingsBloc, SettingsState>(
          builder: (context, state) {
            bool canBuildSettings() => state.currentUser != null;

            if (canBuildSettings()) {
              return LayoutBuilder(
                builder: (ctx, constraints) {
                  return Container(
                    color: AppColors.lightestGray9,
                    child: Column(
                      children: [
                        Container(
                          height: constraints.maxHeight * 0.48,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(mediaQuery.size.height * 40 / screenHeightCoefficent),
                                bottomRight: Radius.circular(mediaQuery.size.height * 40 / screenHeightCoefficent),
                              ),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(0, 2), // changes position of shadow
                                ),
                              ]
                              //border:
                              ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                height: constraints.maxHeight * 0.08,
                                padding: EdgeInsets.only(left: mediaQuery.size.height * 20 / screenHeightCoefficent),
                                alignment: Alignment.centerLeft,
                                child: Text(
                                  // FIXME: Translate
                                  'Settings',
                                  style: TextStyle(fontSize: mediaQuery.size.height * 20 / screenHeightCoefficent),
                                  textAlign: TextAlign.left,
                                ),
                              ),
                              Container(
                                width: mediaQuery.size.width * 0.85,
                                height: constraints.maxHeight * 0.31,
                                alignment: Alignment.center,
                                padding:
                                    EdgeInsets.symmetric(vertical: mediaQuery.size.height * 5 / screenHeightCoefficent),
                                child: FittedBox(
                                  fit: BoxFit.fill,
                                  child: CircleAvatar(
                                    radius: mediaQuery.size.height * 85 / screenHeightCoefficent,
                                    backgroundColor: Colors.transparent,
                                    backgroundImage: state.currentUser.border.haveBorder
                                        ? NetworkImage(state.currentUser.border.url)
                                        : null,
                                    child: CircleAvatar(
                                      backgroundImage: NetworkImage(state.currentUser.instagramInfo.profilePictureUrl),
                                      radius: mediaQuery.size.height * 70 / screenHeightCoefficent,
                                    ),
                                  ),
                                ),
                              ),
                              Text(
                                state.currentUser.instagramInfo.username,
                                style: TextStyle(
                                  fontSize: mediaQuery.size.height * 19 / screenHeightCoefficent,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          width: mediaQuery.size.width * 0.95,
                          height: constraints.maxHeight * 0.44,
                          margin: EdgeInsets.only(top: constraints.maxHeight * 0.04),
                          color: AppColors.lightestGray9,
                          child: Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 30),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ListTile(
                                  leading: Icon(
                                    Icons.person_pin,
                                    color: AppColors.lightGray,
                                  ),
                                  title: Text(
                                    // FIXME: Translate
                                    'Border',
                                    style: textTheme.bodyText1.copyWith(color: AppColors.black),
                                  ),
                                  onTap: () => _openSelectBorderScreen(context),
                                ),
                                Divider(height: 1),
                                ListTile(
                                  leading: Icon(
                                    Icons.logout,
                                    color: AppColors.lightGray,
                                  ),
                                  title: Text(
                                    // FIXME: Translate
                                    'Logout',
                                    style: textTheme.bodyText1.copyWith(color: AppColors.black),
                                  ),
                                  onTap: () => _logout(context),
                                ),
                                Divider(height: 1),
                                ListTile(
                                  leading: Icon(
                                    Icons.delete,
                                    color: AppColors.red1,
                                  ),
                                  title: Text(
                                    // FIXME: Translate
                                    'Delete account',
                                    style: textTheme.bodyText1.copyWith(color: AppColors.black),
                                  ),
                                  onTap: () => _deleteAccount(context),
                                ),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              );
            }
            return Container();
          },
        ),
      ),
    );
  }

  void _openSelectBorderScreen(final BuildContext context) async {
    await _navigationService.pushNamed('settings/border');
    context.read<SettingsBloc>().add(SettingsStarted());
  }

  void _deleteAccount(final BuildContext context) async {
    final result = await _openDeleteAccountDialog(context);
    if (result) {
      context.read<SettingsBloc>().add(SettingsAccountDeleted());

      _navigationService.pushReplacementNamed('auth');
    }
  }

  void _logout(final BuildContext context) {
    context.read<SettingsBloc>().add(SettingsLoggedOut());

    _navigationService.pushReplacementNamed('auth');
  }

  Future<bool> _openDeleteAccountDialog(final BuildContext context) {
    return _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        // FIXME: Translate
        content: Text('Are you sure you want to delete your account?' ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text('Cancel'),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text('Delete'),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );
  }
}
