// @dart=2.9

import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import 'settings_event.dart';
import 'settings_state.dart';

class SettingsBloc extends Bloc<SettingsEvent, SettingsState> {
  static const _userInstagramAuthStateStorageKey = 'user_instagram_auth_state';
  static const _lastUserId = 'last_user_id';
  final _firestoreService = injector<FirestoreService>();
  final _userRepository = injector<UserRepository>();
  final _storage = injector<FlutterSecureStorage>();

  SettingsBloc() : super(SettingsState.initial());

  @override
  Stream<SettingsState> mapEventToState(final SettingsEvent event) async* {
    if (event is SettingsStarted) {
      yield* _mapStartedToState();
    } else if (event is SettingsLoggedOut) {
      yield* _mapLogoutToState();
    } else if (event is SettingsAccountDeleted) {
      yield* _mapAccountDeletedToState();
    }
  }

  Stream<SettingsState> _mapStartedToState() async* {
    yield state.loading();

    try {
      final user = await _userRepository.getUserInfo();

      final userProfileInfo = await _firestoreService.getUserDocument(user.uid);

      yield state.copyWith(currentUser: userProfileInfo);
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }

  Stream<SettingsState> _mapLogoutToState() async* {
    yield state.loading();

    try {
      final user = await _userRepository.getUserInfo();

      await _userRepository.logout();

      await _storage.write(key: _userInstagramAuthStateStorageKey, value: 'logout');

      await _storage.write(key: _lastUserId, value: user.uid);

      yield state.copyWith();
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }

  Stream<SettingsState> _mapAccountDeletedToState() async* {
    yield state.loading();

    try {
      final user = await _userRepository.getUserInfo();

      await _firestoreService.deleteUserDocument(user.uid);

      await _userRepository.delete();

      yield state.copyWith();
    } catch (err) {
      yield state.failure(ErrorResponse.fromJson(err?.response?.data));
    }
  }
}
