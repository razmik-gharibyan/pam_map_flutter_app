// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

@immutable
abstract class SettingsEvent extends Equatable {
  const SettingsEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class SettingsStarted extends SettingsEvent {}

class SettingsLoggedOut extends SettingsEvent {}

class SettingsAccountDeleted extends SettingsEvent {}

