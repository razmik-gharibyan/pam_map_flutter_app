// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:quiver/core.dart';

@immutable
class SettingsState extends Equatable {
  final UserProfileInfo currentUser;
  final bool inProgress;
  final Optional<ErrorResponse> err;

  @override
  List<Object> get props => [
    currentUser,
    inProgress,
    err,
  ];

  @override
  bool get stringify => true;

  const SettingsState({
    this.currentUser,
    this.inProgress,
    this.err,
  });

  factory SettingsState.initial() {
    return SettingsState(
      inProgress: false,
      err: Optional.absent(),
    );
  }

  SettingsState loading() {
    return copyWith(inProgress: true);
  }

  SettingsState failure(final ErrorResponse err) {
    return copyWith(err: Optional<ErrorResponse>.of(err));
  }

  SettingsState copyWith({
    UserProfileInfo currentUser,
    bool inProgress,
    Optional<ErrorResponse> err,
  }) {
    return SettingsState(
      currentUser: currentUser ?? this.currentUser,
      inProgress: inProgress ?? false,
      err: err ?? Optional<ErrorResponse>.absent(),
    );
  }
}
