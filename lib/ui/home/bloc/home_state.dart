// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:quiver/core.dart';

import '../app_tab.dart';

@immutable
class HomeState extends Equatable {
  final AppTab activeTab;
  final bool inProgress;
  final Optional<ErrorResponse> err;

  @override
  List<Object> get props => [
        activeTab,
        inProgress,
        err,
      ];

  @override
  bool get stringify => true;

  const HomeState({
    this.activeTab,
    this.inProgress,
    this.err,
  });

  factory HomeState.initial() {
    return HomeState(
      activeTab: AppTab.map,
    );
  }

  HomeState loading() {
    return copyWith(inProgress: true);
  }

  HomeState failure(final ErrorResponse err) {
    return copyWith(err: Optional<ErrorResponse>.of(err));
  }

  HomeState copyWith({
    AppTab activeTab,
    bool inProgress,
    Optional<ErrorResponse> err,
  }) {
    return HomeState(
      activeTab: activeTab ?? this.activeTab,
      inProgress: inProgress ?? this.inProgress,
      err: err ?? Optional<ErrorResponse>.absent(),
    );
  }
}
