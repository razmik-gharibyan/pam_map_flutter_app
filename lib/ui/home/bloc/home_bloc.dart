// @dart=2.9

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../app_tab.dart';
import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final _userRepository = injector<UserRepository>();
  final _firestoreService = injector<FirestoreService>();

  HomeBloc() : super(HomeState.initial());

  @override
  Stream<HomeState> mapEventToState(final HomeEvent event) async* {
    if (event is HomeStarted) {
      yield* _mapStartedToState(event);
    } else if (event is HomeLifecycleStopped) {
      yield* _mapLifecycleChangedToState();
    } else if (event is HomeLifecycleResumed) {
      yield* _mapLifecycleChangedToState(isVisible: event.isVisible);
    }
  }

  Stream<HomeState> _mapStartedToState(final HomeStarted event) async* {
    yield state.copyWith(
      activeTab: event.activeTab ?? AppTab.map,
    );
  }

  Stream<HomeState> _mapLifecycleChangedToState({final bool isVisible = false}) async* {
    final user = await _userRepository.getUserInfo();

    await _firestoreService.updateUserDocument(
      UserProfileInfo(isVisible: isVisible),
      user.uid,
    );

    yield state.copyWith();
  }
}
