// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

import '../app_tab.dart';

@immutable
abstract class HomeEvent extends Equatable {
  const HomeEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class HomeStarted extends HomeEvent {
  final AppTab activeTab;

  @override
  List<Object> get props => [activeTab];

  const HomeStarted({this.activeTab = AppTab.map});
}

class HomeLifecycleStopped extends HomeEvent {}

class HomeLifecycleResumed extends HomeEvent {
  final bool isVisible;

  @override
  List<Object> get props => [isVisible];

  const HomeLifecycleResumed({this.isVisible});
}
