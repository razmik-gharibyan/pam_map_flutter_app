// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'auth/auth.dart';
import 'common/common.dart';
import 'home/home_screen.dart';
import 'instagram/instagram.dart';

class InitialScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const _topText = "People Around Me";

    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) {
        if (state is AuthUninitialized || state is AuthInProgress) return CenteredLoadingIndicator();
        if (state is AuthAuthenticated) {
          return state.hasInstagram ? HomeScreen() : InstagramConnectScreen();
        }
        if (state is AuthUnauthenticated) return AuthScreen();

        return Scaffold(
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                SizedBox(
                  height: 50,
                  child: Container(
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                SizedBox(
                  height: 80,
                  child: Container(
                    color: Theme.of(context).primaryColor,
                    child: Stack(
                      children: [
                        Container(
                          alignment: Alignment.center,
                          child: Text(
                            _topText,
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 20,
                              color: Colors.white,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
