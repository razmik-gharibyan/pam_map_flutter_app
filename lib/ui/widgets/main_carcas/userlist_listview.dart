/*
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pam_map_flutter_app/extension/hex_to_rgb.dart';
import 'package:pam_map_flutter_app/helper/follower_utils.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/model/pammap_firebase_user.dart';
import 'package:pam_map_flutter_app/providers/main_carcas_provider.dart';
import 'package:pam_map_flutter_app/providers/map_provider.dart';
import 'package:url_launcher/url_launcher.dart';

class UserListListView extends StatefulWidget {

  final List<PammapFirebaseUser> _userList;
  Function notifyParent;

  UserListListView(this._userList,this.notifyParent);

  @override
  _UserListListViewState createState() => _UserListListViewState(notifyParent);
}

class _UserListListViewState extends State<UserListListView> {

  Function notifyParent;

  _UserListListViewState(this.notifyParent);

  // Constants
  final _followers = tr("userlist.followers");
  final _account = tr("userlist.account");
  final _private = tr("marker.private");
  final _public = tr("marker.public");
  final _official = tr("userlist.official");
  final _verified = tr("userlist.verified");
  final _notVerified = tr("userlist.not_verified");
  final _openBtnText = tr("userlist.open_btn");
  // Tools
  final _followersEditor = FollowersEditor();
  final _mapProvider = MapProvider();
  final _mainCarcasProvider = MainCarcasProvider();

  @override
  Widget build(BuildContext context) {

    final mediaQuery = MediaQuery.of(context);

    return Container(
      child: ListView.builder(
        itemBuilder: (ctx, index) => Container(
          height: mediaQuery.size.height * 0.14,
          margin: EdgeInsets.only(
            bottom: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
            left: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
            right: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
          ),
          decoration: BoxDecoration(
              color: HexColor.fromHex("fafafa"),
              borderRadius: BorderRadius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent,),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.4),
                  spreadRadius: 2,
                  blurRadius: 7,
                  offset: Offset(0, 2), // changes position of shadow
                ),
              ]
          ),
          child: InkWell(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  width: mediaQuery.size.width * 0.2,
                  height: mediaQuery.size.height * 0.14,
                    child: Padding(
                      padding: EdgeInsets.all(mediaQuery.size.height * 1 / ConstraintHelper.screenHeightCoefficent),
                      child: Center(
                        child: FittedBox(
                          fit: BoxFit.fill,
                          child: CircleAvatar(
                            backgroundImage: NetworkImage(widget._userList[index].data["profilePicture"]),
                            radius: mediaQuery.size.height * 24 / ConstraintHelper.screenHeightCoefficent,
                          ),
                        ),
                      ),
                    ),
                ),
                Container(
                  width: mediaQuery.size.width * 0.5,
                  height: mediaQuery.size.height * 0.14,
                  child: Column(
                    children: [
                      Container(
                        height: mediaQuery.size.height * 0.07,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Flexible(
                              child: Text(
                                widget._userList[index].data["username"],
                                style: TextStyle(
                                  fontSize: mediaQuery.size.height * 12 / ConstraintHelper.screenHeightCoefficent,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        width: mediaQuery.size.width * 0.5,
                        height: mediaQuery.size.height * 0.07,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              width: mediaQuery.size.width * 0.1,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Flexible(
                                    child: Text(
                                      _followersEditor.convertFollowersToInstagramFollowers(widget._userList[index].data["followers"]),
                                      style: TextStyle(
                                          fontSize: mediaQuery.size.height * 8 / ConstraintHelper.screenHeightCoefficent
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Text(
                                      _followers,
                                      style: TextStyle(
                                          fontSize: mediaQuery.size.height * 7 / ConstraintHelper.screenHeightCoefficent
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: mediaQuery.size.width * 0.2,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Flexible(
                                    child: Text(
                                      widget._userList[index].data["isPrivate"] ? _private : _public,
                                      style: TextStyle(
                                          fontSize: mediaQuery.size.height * 8 / ConstraintHelper.screenHeightCoefficent
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Text(
                                      _account,
                                      style: TextStyle(
                                          fontSize: mediaQuery.size.height * 7 / ConstraintHelper.screenHeightCoefficent
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              width: mediaQuery.size.width * 0.2,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  Flexible(
                                    child: Text(
                                      widget._userList[index].data["isVerified"] ? _verified : _notVerified,
                                      style: TextStyle(
                                        color: widget._userList[index].data["isVerified"] ? Colors.blue : Colors.black87,
                                        fontSize: mediaQuery.size.height * 8 / ConstraintHelper.screenHeightCoefficent,
                                      ),
                                    ),
                                  ),
                                  Flexible(
                                    child: Text(
                                      _official,
                                      style: TextStyle(
                                          fontSize: mediaQuery.size.height * 7 / ConstraintHelper.screenHeightCoefficent
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                InkWell(
                  child: Container(
                    width: mediaQuery.size.width * 0.15,
                    height: double.infinity,
                    decoration: BoxDecoration(
                      gradient: LinearGradient(
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft,
                          colors: [Theme.of(context).accentColor,Theme.of(context).primaryColorLight]
                      ),
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                        bottomRight: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                      ),
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Icon(
                          Icons.open_in_new,
                          color: Colors.white,
                          size: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                        ),
                        Text(
                          _openBtnText,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: mediaQuery.size.height * 8 / ConstraintHelper.screenHeightCoefficent
                          ),
                        )
                      ],
                    ),
                  ),
                  onTap: () {
                    _openProfileOnInstagram(widget._userList[index], context);
                  },
                )
              ],
            ),
            onTap: () {
              if (widget._userList[index].data["isVisible"] && widget._userList[index].data["isActive"]) {
                Map<String,dynamic> position = widget._userList[index].data["location"];
                _mapProvider.currentLocation = LatLng(position["latitude"], position["longitude"]);
                _mapProvider.initCameraPosition = CameraPosition(target: _mapProvider.currentLocation, zoom: 19);
                _mainCarcasProvider.pageIndex = 0; // Navigate to map page
                notifyParent();
              }else{
                setState(() {
                  Scaffold.of(context).showSnackBar(
                      SnackBar(
                        content: Text(tr("marker.error_text")),
                        duration: Duration(milliseconds: 2000),
                      )
                  );
                });
              }
            },
          )
        ),
        itemCount: widget._userList.length,
      ),
    );
  }

  void _openProfileOnInstagram(PammapFirebaseUser user,BuildContext context) async {
    final url = "http://instagram.com/_u/${user.data['username']}";
    try {
      if(await canLaunch(url)) {
        await launch(url,universalLinksOnly: true);
      }
    }catch(error) {
      Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text(tr("marker.error_text")),
            duration: Duration(milliseconds: 2000),
          )
      );
    }
  }
}

 */
