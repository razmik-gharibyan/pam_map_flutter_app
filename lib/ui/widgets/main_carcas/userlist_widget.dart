/*
import 'dart:async';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/model/pammap_firebase_user.dart';
import 'package:pam_map_flutter_app/providers/firestore_provider.dart';
import 'package:pam_map_flutter_app/widgets/main_carcas/userlist_listview.dart';

class UserListWidget extends StatefulWidget {

  Function notifyParent;

  UserListWidget(this.notifyParent);

  // Singletons
  @override
  _UserListWidgetState createState() => _UserListWidgetState(notifyParent);
}

class _UserListWidgetState extends State<UserListWidget> {

  Function notifyParent;

  _UserListWidgetState(this.notifyParent);

  final _firestoreProvider = FirestoreProvider();
  final _streamController = StreamController<List<PammapFirebaseUser>>();
  // Vars
  var _userList = List<PammapFirebaseUser>();

  @override
  void initState() {
    _listenForUserListStream();
    _streamController.add(_firestoreProvider.getUsersInBoundsList);
  }


  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(
      builder: (ctx, constraints) => Container(
        height: constraints.maxHeight,
        child: Column(
          children: [
            Container(
              width: double.infinity,
              height: constraints.maxHeight * 0.05,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(0.0, 1.0),
                  colors: [Theme.of(context).primaryColor,Theme.of(context).accentColor]
                ),
                borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent),
                  bottomRight: Radius.circular(mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.4),
                    spreadRadius: 2,
                    blurRadius: 7,
                    offset: Offset(0, 2), // changes position of shadow
                  ),
                ]
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: mediaQuery.size.height * 5 / ConstraintHelper.screenHeightCoefficent),
                child: Text(
                  tr("userlist.top_text"),
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: mediaQuery.size.height * 13 / ConstraintHelper.screenHeightCoefficent
                  ),
                  textAlign: TextAlign.center,
                ),
              ),
            ),
            Container(
              height: constraints.maxHeight * 0.95,
              child: _userList.isNotEmpty
                ? UserListListView(_userList,notifyParent)
                : Center(
                    child: Text(
                      tr("userlist.noone_text"),
                      style: TextStyle(
                        fontSize: mediaQuery.size.height * 13 / ConstraintHelper.screenHeightCoefficent
                      ),
                    ),
                  ),
            )
          ],
        )
      ),
    );
  }

  void _listenForUserListStream() {
    _streamController.stream.listen((userList) {
      _userList.clear();
      _userList.addAll(userList);
      setState(() {});
    });
  }
}

 */
