/*
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:pam_map_flutter_app/extension/hex_to_rgb.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/model/place_model.dart';
import 'package:pam_map_flutter_app/providers/main_carcas_provider.dart';
import 'package:pam_map_flutter_app/providers/map_provider.dart';

class PlaceListListView extends StatefulWidget {

  final List<PlaceModel> _placeList;
  Function notifyParent;

  PlaceListListView(this._placeList,this.notifyParent);

  @override
  _PlaceListListViewState createState() => _PlaceListListViewState(notifyParent);
}

class _PlaceListListViewState extends State<PlaceListListView> {

  Function notifyParent;

  _PlaceListListViewState(this.notifyParent);

  // Tools
  final _mapProvider = MapProvider();
  final _mainCarcasProvider = MainCarcasProvider();

  @override
  Widget build(BuildContext context) {

    final mediaQuery = MediaQuery.of(context);

    return Container(
      child: ListView.builder(
        itemBuilder: (ctx, index) => Container(
          margin: EdgeInsets.only(
            bottom: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
            left: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
            right: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
          ),
          decoration: BoxDecoration(
            color: HexColor.fromHex("fafafa"),
            borderRadius: BorderRadius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent,),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                spreadRadius: 2,
                blurRadius: 7,
                offset: Offset(0, 2), // changes position of shadow
              ),
            ]
          ),
          child: ListTile(
            leading: FittedBox(
              fit: BoxFit.fill,
              child: CircleAvatar(
                backgroundColor: Theme.of(context).accentColor,
                radius: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
                child: Icon(
                  Icons.place,
                  color: Colors.white,
                  size: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent,
                ),
              ),
            ),
            title: Text(
              widget._placeList[index].title,
              style: TextStyle(
                  fontSize: mediaQuery.size.height * 12 / ConstraintHelper.screenHeightCoefficent
              ),
            ),
            onTap: () {
              _mapProvider.currentLocation = widget._placeList[index].location;
              _mapProvider.initCameraPosition = CameraPosition(target: _mapProvider.currentLocation, zoom: 15);
              _mainCarcasProvider.pageIndex = 0; // Navigate to map widget
              notifyParent();
            },
          ),
        ),
        itemCount: widget._placeList.length,
      ),
    );
  }

}

 */
