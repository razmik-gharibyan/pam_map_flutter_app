/*
import 'dart:async';
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_keyboard_visibility/flutter_keyboard_visibility.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_place/google_place.dart';
import 'package:pam_map_flutter_app/extension/hex_to_rgb.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/model/pammap_firebase_user.dart';
import 'package:pam_map_flutter_app/model/place_model.dart';
import 'package:pam_map_flutter_app/providers/firestore_provider.dart';
import 'package:pam_map_flutter_app/widgets/main_carcas/placelist_listview.dart';
import 'package:pam_map_flutter_app/widgets/main_carcas/userlist_listview.dart';
import 'package:pam_map_flutter_app/secret.dart';
import 'package:stream_transform/stream_transform.dart';

class SearchWidget extends StatefulWidget {

  Function notifyParent;

  SearchWidget(this.notifyParent);

  @override
  _SearchWidgetState createState() => _SearchWidgetState(notifyParent);
}

class _SearchWidgetState extends State<SearchWidget> {

  Function notifyParent;

  _SearchWidgetState(this.notifyParent);
  // Constants
  final _searchPeopleTitle = tr("search.search_people");
  final _searchPlacesTitle = tr("search.search_places");
  final _cancelBtnTitle = tr("search.cancel_btn");
  final _notFoundPlaceTitle = tr("search.not_found_place");
  final _notFoundUserTitle = tr("search.not_found_user");
  // Tools
  final _firestoreProvider = FirestoreProvider();
  var _streamController = StreamController<String>();
  var _controller = TextEditingController();
  var _googlePlaces;
  // Vars
  bool _isLoading = false;
  bool _isPlaceSearched = false;
  bool _isFocused = false;
  List<PammapFirebaseUser> _userList = List<PammapFirebaseUser>();
  List<PlaceModel> _placeList = List<PlaceModel>();
  String _userSearchText = "";
  String _placeSearchText = "";
  
  @override
  void initState() {
    super.initState();
    _searchUserOrPlace();
    _listenKeyboardChanges();
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    var secret = await SecretLoader(secretPath: "assets/secrets.json").load();
    _googlePlaces = GooglePlace(secret.placesApiKey);
  }

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(
      builder: (ctx, constraints) => Container(
        color: Colors.white,//HexColor.fromHex("f5f5f5"),
        child: Column(
          children: [
            Container(
              height: constraints.maxHeight * 0.195,
              color: Theme.of(context).primaryColor,
              child: Column(
                children: [
                  Column(
                    children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              InkWell(
                                child: Container(
                                  width: mediaQuery.size.width * 0.45,
                                  height: constraints.maxHeight * 0.08,
                                  decoration: BoxDecoration(
                                    color: _isPlaceSearched ? HexColor.fromHex("fafafa") : Colors.white,
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent)
                                    ),
                                  ),
                                  child: Row(
                                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                     crossAxisAlignment: CrossAxisAlignment.center,
                                     children: [
                                       Icon(
                                         Icons.person,
                                         size: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                                         color: _isPlaceSearched ? Colors.black38 : Theme.of(context).primaryColorLight
                                       ),
                                       Text(
                                         _searchPeopleTitle,
                                         style: TextStyle(
                                           color: _isPlaceSearched ? Colors.black38 : Theme.of(context).primaryColorLight,
                                           fontSize: mediaQuery.size.height * 12 / ConstraintHelper.screenHeightCoefficent,
                                         ),
                                       )
                                      ],
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    _isPlaceSearched = false;
                                    _controller.text = _userSearchText;
                                  });
                                },
                              ),
                              Container(
                                color: HexColor.fromHex("f5f5f5"),
                                width: mediaQuery.size.width * 0.005,
                                height: constraints.maxHeight * 0.08,
                              ),
                              InkWell(
                                child: Container(
                                  width: mediaQuery.size.width * 0.45,
                                  height: constraints.maxHeight * 0.08,
                                  decoration: BoxDecoration(
                                      color: _isPlaceSearched ? Colors.white : HexColor.fromHex("fafafa") ,
                                      borderRadius: BorderRadius.only(
                                          topRight: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent)
                                      ),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: [
                                      Text(
                                        _searchPlacesTitle,
                                        style: TextStyle(
                                          color: _isPlaceSearched ? Theme.of(context).primaryColorLight : Colors.black38,
                                          fontSize: mediaQuery.size.height * 12 / ConstraintHelper.screenHeightCoefficent,
                                        ),
                                      ),
                                      Icon(
                                          Icons.place,
                                          size: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                                          color: _isPlaceSearched ? Theme.of(context).primaryColorLight : Colors.black38,
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    _isPlaceSearched = true;
                                    _controller.text = _placeSearchText;
                                  });
                                },
                              )
                            ],
                          ),
                        ],
                      ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        width: mediaQuery.size.width * 0.45,
                        height: constraints.maxHeight * 0.003,
                        color: _isPlaceSearched ? Colors.white : Theme.of(context).primaryColorLight,
                      ),
                      Container(
                        width: mediaQuery.size.width * 0.45,
                        height: constraints.maxHeight * 0.003,
                        color: _isPlaceSearched ? Theme.of(context).primaryColorLight : Colors.white,
                      )
                    ],
                  ),
                  Container(
                    width: mediaQuery.size.width * 0.905,
                    height: constraints.maxHeight * 0.1,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                        bottomRight: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                      )
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(
                          width: mediaQuery.size.width * 0.85,
                          height: constraints.maxHeight * 0.06,
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                width: (Platform.isIOS && _isFocused) ? mediaQuery.size.width * 0.7 : mediaQuery.size.width * 0.85,
                                height: constraints.maxHeight * 0.06,
                                child: TextFormField(
                                  controller: _controller,
                                  style: TextStyle(
                                    fontSize: mediaQuery.size.height * 16 / ConstraintHelper.screenHeightCoefficent
                                  ),
                                  decoration: InputDecoration(
                                    suffixIcon: (Platform.isIOS && _isFocused)
                                      ? IconButton(
                                          icon: CircleAvatar(
                                            backgroundColor: Colors.grey,
                                            radius: mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent,
                                            child: Icon(
                                              Icons.clear,
                                              color: Colors.white,
                                              size: mediaQuery.size.height * 14 / ConstraintHelper.screenHeightCoefficent,
                                            ),
                                          ),
                                          onPressed: () {
                                            _controller.text = "";
                                            _streamController.add(_controller.text);
                                          },
                                        )
                                      : Icon(
                                          Icons.search,
                                          size: mediaQuery.size.height * 17 / ConstraintHelper.screenHeightCoefficent,
                                        ),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent),
                                      borderSide: BorderSide(
                                          width: mediaQuery.size.height * 0.1 / ConstraintHelper.screenHeightCoefficent),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent),
                                      borderSide: BorderSide(
                                          color: Theme.of(context).primaryColorLight,
                                          width: mediaQuery.size.height * 0.5 / ConstraintHelper.screenHeightCoefficent),
                                    ),
                                    contentPadding: EdgeInsets.symmetric(horizontal: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent)
                                  ),
                                  onChanged: (text) {
                                    setState(() {
                                      _isLoading = true;
                                      _streamController.add(text);
                                    });
                                  },
                                  onTap: () {
                                    setState(() {
                                      _isFocused = true;
                                    });
                                  },
                                ),
                              ),
                              (Platform.isIOS && _isFocused) ? InkWell(
                                child: Container(
                                  width: mediaQuery.size.width * 0.15,
                                  height: constraints.maxHeight * 0.06,
                                  padding: EdgeInsets.symmetric(vertical: mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                                  child: Text(
                                    _cancelBtnTitle,
                                    style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontSize: mediaQuery.size.height * 13 / ConstraintHelper.screenHeightCoefficent,
                                      fontWeight: FontWeight.bold
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ),
                                onTap: () {
                                  FocusScope.of(context).unfocus();
                                },
                              ) : SizedBox()
                            ],
                          )
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              height: constraints.maxHeight * 0.805,
              child: _isLoading ? Center(child: Platform.isAndroid ? CircularProgressIndicator() : CupertinoActivityIndicator()) : _isPlaceSearched
                  ? _placeList.isEmpty
                    ? Center(
                        child: Text(_controller.text.isEmpty ? "" : _notFoundPlaceTitle),
                      )
                    : PlaceListListView(_placeList,notifyParent)
                  : _userList.isEmpty
                    ? Center(
                        child: Text(_controller.text.isEmpty ? "" : _notFoundUserTitle),
                      )
                    : UserListListView(_userList,notifyParent)
            )
           ],
        )
      ),
    );
  }

  void _searchUserOrPlace() {
    _streamController.stream.debounce(Duration(milliseconds: 400)).listen((text) async {
      if(!_isPlaceSearched) {
        print("entered with text $text");
        _userSearchText = _controller.text;
        _userList.clear();
        _userList.addAll(await _firestoreProvider.findAllUsersMatchingSearch(text));
        setState(() {
          _isLoading = false;
        });
      }else{
        _placeSearchText = _controller.text;
        _placeList.clear();
        if(text.isNotEmpty) {
          final response = await _googlePlaces.autocomplete.get(text);
          if(response != null) {
            if(response.status != "REQUEST_DENIED") {
              for(var prediction in response.predictions) {
                var detail = await _googlePlaces.details.get(prediction.placeId);
                LatLng placeLocation = LatLng(detail.result.geometry.location.lat,detail.result.geometry.location.lng);
                _placeList.add(PlaceModel(prediction.description,placeLocation));
                if(_placeSearchText.isEmpty) {
                  _placeList.clear();
                }
              }
            }
            setState(() {
              _isLoading = false;
            });
          }
        }else{
          setState(() {
            _isLoading = false;
          });
        }
      }
    });
  }

  void _listenKeyboardChanges() {
    KeyboardVisibility.onChange.listen((value) {
      if(!value) {
        // Keyboard closed
        setState(() {
          _isFocused = false;
        });
      }
    });
  }

}

 */
