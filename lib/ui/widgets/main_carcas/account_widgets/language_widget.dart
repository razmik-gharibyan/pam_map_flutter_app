/*
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/providers/account_back_provider.dart';
import 'package:pam_map_flutter_app/providers/map_provider.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LanguageWidget extends StatelessWidget {

  // Constants
  final _languageList = [
    {"lang": "Afrikaans", "code": "af"}, // Afrikaans
    {"lang": "中文 & 台灣", "code": "zh"}, // Chinese
    {"lang": "Český", "code": "cs"}, // Czech
    {"lang": "Dansk", "code": "da"}, // Danish
    {"lang": "Nederlands", "code": "nl"}, // Nederlands
    {"lang": "Suomi", "code": "fi"}, // Finnish
    {"lang": "Français", "code": "fr"}, // French
    {"lang": "Deutsch", "code": "de"}, // Deutsch
    {"lang": "English", "code": "en"}, // English
    {"lang": "Ελληνικά", "code": "el"}, // Greek
    {"lang": "Bahasa Indonesia", "code": "id"}, // Indonesian
    {"lang": "Italiano", "code": "it"}, // Italian
    {"lang": "日本語", "code": "ja"}, // Japanese
    {"lang": "한국어", "code": "ko"}, // Korean
    {"lang": "Bahasa Melayu", "code": "ms"}, // Malay
    {"lang": "Norsk", "code": "nb"}, // Norwegian
    {"lang": "Polski", "code": "pl"}, // Polish
    {"lang": "Português", "code": "pt"}, // Portuguese
    {"lang": "Русский", "code": "ru"}, // Russian
    {"lang": "Español", "code": "es"}, // Spanish
    {"lang": "Svenska", "code": "sv"}, // Sweden
    {"lang": "Filipino", "code": "tl"}, // Filipino
    {"lang": "ภาษาไทย", "code": "th"}, // Thai
    {"lang": "Türkçe", "code": "tr"}, // Turkish
    {"lang": "العربية", "code": "ar"}, // Arabic
    {"lang": "עִברִית", "code": "he"}, // Hebrew
    {"lang": "हिन्दी", "code": "hi"}, // India
  ];
  // Tools
  final _mapProvider = MapProvider();

  @override
  Widget build(BuildContext context) {

    final _mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(
      builder: (ctx, constraints) => Container(
        child: Column(
          children: [
            Container(
                alignment: Alignment.center,
                height: constraints.maxHeight * 0.1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    IconButton(
                      icon: Icon(
                        Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios,
                        size: _mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                      ),
                      onPressed: () {
                        Provider.of<AccountBackProvider>(context,listen: false).setBackPressed(true);
                      },
                    ),
                    Text(
                      tr("account.language.top_text"),
                      style: TextStyle(
                          fontSize: _mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ],
                )
            ),
            SizedBox(
              height: constraints.maxHeight * 0.05,
            ),
            FutureBuilder(
              future: _getCurrentLanguage(),
              builder: (ctx, snapshot) => Container(
                height: constraints.maxHeight * 0.85,
                child: snapshot.connectionState == ConnectionState.done ? ListView.builder(
                  padding: EdgeInsets.zero,
                  itemBuilder: (ctx, index) => ListTile(
                    title: Text(
                      _languageList[index]["lang"],
                      style: TextStyle(
                          color: snapshot.data == _languageList[index]["code"] ? Theme.of(context).accentColor : Colors.black,
                          fontSize: _mediaQuery.size.height * 17 / ConstraintHelper.screenHeightCoefficent
                      ),
                    ),
                    onTap: () async {
                      var sharedPref = await SharedPreferences.getInstance();
                      // Change language
                      await sharedPref.setString("language", _languageList[index]["code"]);
                      EasyLocalization.of(context).locale = Locale(_languageList[index]["code"]);
                      // Reload map with empty markers , to update marker titles translation too
                      _mapProvider.usersInBoundsList.clear();
                      _mapProvider.allUsersList.clear();
                      _mapProvider.markerList.clear();
                      _mapProvider.markerListCopy.clear();
                      _mapProvider.markers.clear();
                      Provider.of<AccountBackProvider>(context,listen: false).setBackPressed(true);
                    },
                  ),
                  itemCount: _languageList.length,
                ) : Center(child: Platform.isAndroid ? CircularProgressIndicator() : CupertinoActivityIndicator())
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<String> _getCurrentLanguage() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    return sharedPrefs.getString("language");
  }

}

 */
