/*
import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/extension/hex_to_rgb.dart';
import 'package:pam_map_flutter_app/helper/bitmap_utils.dart';
import 'package:pam_map_flutter_app/helper/follower_utils.dart';
import 'package:pam_map_flutter_app/helper/constraint_utils.dart';
import 'package:pam_map_flutter_app/core/model/user_profile_info.dart';
import 'package:pam_map_flutter_app/providers/account_back_provider.dart';
import 'package:pam_map_flutter_app/providers/firestore_provider.dart';
import 'package:provider/provider.dart';

class ChangeCardColorWidget extends StatefulWidget {
  @override
  _ChangeCardColorState createState() => _ChangeCardColorState();
}

class _ChangeCardColorState extends State<ChangeCardColorWidget> {

  // Constants
  final _changeCardColorText = tr("account.change_card.top_text");
  final _followers = tr("userlist.followers");
  final _account = tr("userlist.account");
  final _private = tr("marker.private");
  final _public = tr("marker.public");
  final _official = tr("userlist.official");
  final _verified = tr("userlist.verified");
  final _notVerified = tr("userlist.not_verified");
  final _openBtnText = tr("userlist.open_btn");
  // Singletons
  var _currentPamMapFirebaseUser = UserProfileInfo();
  // Tools
  final _firestoreProvider = FirestoreProvider();
  final _bitmapEditor = BitmapEditor();
  final _followersEditor = FollowersEditor();

  // Vars
  var _cardList = [
    {"title": tr("account.change_card.card_list.basic"), "color": "fafafa"},
    {"title": tr("account.change_card.card_list.blue"), "color":  "0574f2"},
    {"title": tr("account.change_card.card_list.orange"), "color": "f28505"},
    {"title": tr("account.change_card.card_list.yellow"), "color": "eaf205"},
    {"title": tr("account.change_card.card_list.aqua"), "color": "05bdf2"},
    {"title": tr("account.change_card.card_list.dark_blue"), "color": "3705f2"},
    {"title": tr("account.change_card.card_list.candy"), "color": "eb1077"},
    {"title": tr("account.change_card.card_list.green"), "color": "24eb10"},
    {"title": tr("account.change_card.card_list.lawn_green"), "color": "aaeb10"},
    {"title": tr("account.change_card.card_list.pink"), "color": "eb10c4"},
    {"title": tr("account.change_card.card_list.purple"), "color": "a510eb"},
    {"title": tr("account.change_card.card_list.red"), "color": "eb1010"},
  ];
  var _avatarImage;
  bool _isLoading = true;
  bool _isDisposed = false;
  int _colorIndex = 0;


  @override
  void didChangeDependencies() async {
    int index = 0;
    for(var colorMap in _cardList) {
      if(_currentPamMapFirebaseUser.data["cardColor"] == colorMap["color"]) {
        _colorIndex = index;
        break;
      }
      index++;
    }
    if(_avatarImage == null) {
      if(!_isDisposed) {
        _avatarImage = await _bitmapEditor.getAppliedBorderBytes(_currentPamMapFirebaseUser.data["profilePicture"],_currentPamMapFirebaseUser.data["border"]);
        if(!_isDisposed) setState(() {_isLoading = false;});
      }
    }
  }

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    final mediaQuery = MediaQuery.of(context);

    return LayoutBuilder(
      builder: (ctx, constraints) => Container(
        color: HexColor.fromHex("fafafa"),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              height: constraints.maxHeight * 0.4,
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(mediaQuery.size.height * 40 / ConstraintHelper.screenHeightCoefficent),
                    bottomRight: Radius.circular(mediaQuery.size.height * 40 / ConstraintHelper.screenHeightCoefficent),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ]
                //border:
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: constraints.maxHeight * 0.08,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        IconButton(
                          icon: Icon(
                            Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios,
                            size: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                          ),
                          onPressed: () {
                            Provider.of<AccountBackProvider>(context,listen: false).setBackPressed(true);
                          },
                        ),
                        Container(
                          height: constraints.maxHeight * 0.08,
                          padding: EdgeInsets.only(left: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent),
                          alignment: Alignment.centerLeft,
                          child: Text(
                            _changeCardColorText,
                            style: TextStyle(
                                fontSize: mediaQuery.size.height * 15 / ConstraintHelper.screenHeightCoefficent
                            ),
                            textAlign: TextAlign.left,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: mediaQuery.size.width * 0.85,
                    height: constraints.maxHeight * 0.3,
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(vertical: mediaQuery.size.height * 5 / ConstraintHelper.screenHeightCoefficent),
                    child: FittedBox(
                      fit: BoxFit.fill,
                      child: _isLoading ? (Platform.isAndroid ? CircularProgressIndicator() : CupertinoActivityIndicator()) :  Container(
                          height: constraints.maxHeight * 0.17,
                          decoration: BoxDecoration(
                              color: HexColor.fromHex(_cardList[_colorIndex]["color"]),
                              borderRadius: BorderRadius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent,),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.4),
                                  spreadRadius: 2,
                                  blurRadius: 7,
                                  offset: Offset(0, 2), // changes position of shadow
                                ),
                              ]
                          ),
                          child: InkWell(
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  width: mediaQuery.size.width * 0.7,
                                  height: constraints.maxHeight * 0.17,
                                  child: Column(
                                    children: [
                                      Row(
                                        mainAxisAlignment: MainAxisAlignment.start,
                                        children: [
                                          Padding(
                                            padding: EdgeInsets.all(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                                            child: FittedBox(
                                              fit: BoxFit.fill,
                                              child: CircleAvatar(
                                                backgroundImage: MemoryImage(_avatarImage),
                                                radius: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                                              ),
                                            ),
                                          ),
                                          Flexible(
                                            child: Text(
                                              _currentPamMapFirebaseUser.data["username"],
                                              style: TextStyle(
                                                fontSize: mediaQuery.size.height * 12 / ConstraintHelper.screenHeightCoefficent,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      Container(
                                        width: mediaQuery.size.width * 0.55,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          children: [
                                            Column(
                                              children: [
                                                Text(
                                                  _followersEditor.convertFollowersToInstagramFollowers(_currentPamMapFirebaseUser.data["followers"]),
                                                  style: TextStyle(
                                                      fontSize: mediaQuery.size.height * 9 / ConstraintHelper.screenHeightCoefficent
                                                  ),
                                                ),
                                                Text(
                                                  _followers,
                                                  style: TextStyle(
                                                      fontSize: mediaQuery.size.height * 7 / ConstraintHelper.screenHeightCoefficent
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                Text(
                                                  _currentPamMapFirebaseUser.data["isPrivate"] ? _private : _public,
                                                  style: TextStyle(
                                                      fontSize: mediaQuery.size.height * 9 / ConstraintHelper.screenHeightCoefficent
                                                  ),
                                                ),
                                                Text(
                                                  _account,
                                                  style: TextStyle(
                                                      fontSize: mediaQuery.size.height * 7 / ConstraintHelper.screenHeightCoefficent
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                Text(
                                                  _currentPamMapFirebaseUser.data["isVerified"] ? _verified : _notVerified,
                                                  style: TextStyle(
                                                    color: _currentPamMapFirebaseUser.data["isVerified"] ? Colors.blue : Colors.black87,
                                                    fontSize: mediaQuery.size.height * 9 / ConstraintHelper.screenHeightCoefficent,
                                                  ),
                                                ),
                                                Text(
                                                  _official,
                                                  style: TextStyle(
                                                      fontSize: mediaQuery.size.height * 7 / ConstraintHelper.screenHeightCoefficent
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                InkWell(
                                  child: Container(
                                    width: mediaQuery.size.width * 0.15,
                                    height: double.infinity,
                                    decoration: BoxDecoration(
                                      gradient: LinearGradient(
                                          begin: Alignment.topRight,
                                          end: Alignment.bottomLeft,
                                          colors: [Theme.of(context).accentColor,Theme.of(context).primaryColorLight]
                                      ),
                                      borderRadius: BorderRadius.only(
                                        topRight: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                                        bottomRight: Radius.circular(mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent),
                                      ),
                                    ),
                                    child: Column(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Icon(
                                          Icons.open_in_new,
                                          color: Colors.white,
                                          size: mediaQuery.size.height * 20 / ConstraintHelper.screenHeightCoefficent,
                                        ),
                                        Text(
                                          _openBtnText,
                                          style: TextStyle(
                                              color: Colors.white,
                                              fontSize: mediaQuery.size.height * 8 / ConstraintHelper.screenHeightCoefficent
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                )
                              ],
                            ),
                          )
                      ),
                      ),
                    ),
                ],
              ),
            ),
            Container(
              width: mediaQuery.size.width,
              height: constraints.maxHeight * 0.57,
              margin: EdgeInsets.only(top: constraints.maxHeight * 0.03),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topRight,
                      end: Alignment.bottomLeft,
                      colors: [Theme.of(context).primaryColorLight,Theme.of(context).accentColor]
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular( mediaQuery.size.height * 30 / ConstraintHelper.screenHeightCoefficent),
                    topRight: Radius.circular( mediaQuery.size.height * 30 / ConstraintHelper.screenHeightCoefficent),
                  ),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.4),
                      spreadRadius: 2,
                      blurRadius: 7,
                      offset: Offset(0, 2), // changes position of shadow
                    ),
                  ]
                //border:
              ),
              child: GridView.builder(
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 4
                ),
                itemBuilder: (ctx, index) {
                  return InkWell(
                    child: Container(
                      width: constraints.maxWidth * 0.2,
                      height: constraints.maxHeight * 0.2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            width: constraints.maxWidth * 0.2,
                            height: constraints.maxHeight * 0.14,
                            child: FittedBox(
                              fit: BoxFit.fill,
                              child: CircleAvatar(
                                backgroundColor: HexColor.fromHex(_cardList[index]["color"]),
                                radius: mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent,
                              ),
                            ),
                          ),
                          Container(
                              width: constraints.maxWidth * 0.2,
                              height: constraints.maxHeight * 0.04,
                              alignment: Alignment.center,
                              child: _colorIndex == index ? Icon(
                                Icons.check_circle,
                                color: Colors.white,
                                size: mediaQuery.size.height * 14 / ConstraintHelper.screenHeightCoefficent,
                              ) : Text(
                                _cardList[index]["title"],
                                style: TextStyle(
                                    color: Colors.white,
                                    fontSize: mediaQuery.size.height * 10 / ConstraintHelper.screenHeightCoefficent
                                ),
                                textAlign: TextAlign.center,
                              )
                          )
                        ],
                      ),
                    ),
                    onTap: () async {
                      setState(() {
                        _isLoading = true;
                      });
                      if(!_isDisposed) {
                        _colorIndex = index;
                        _currentPamMapFirebaseUser.data["cardColor"] = _cardList[index]["color"];
                        _firestoreProvider.addUserDocument(_currentPamMapFirebaseUser);
                        if(!_isDisposed) setState(() {_isLoading = false;});
                      }
                    },
                  );
                },
                itemCount: _cardList.length,
              ),
            )
          ],
        ),
      ),
    );
  }
}

 */
