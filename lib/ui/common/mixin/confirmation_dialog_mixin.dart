// @dart=2.9

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:pam_map_flutter_app/core/core.dart';

mixin ConfirmationDialogMixin {
  // FIXME: Translate
  final _firstPart =
      'Location service is disabled press Open Settings to enable location and be visible on map or Continue to use without sharing your location';
  final _openSettings = 'Open Settings';
  final _continue = 'Continue';

  final _dialogService = injector<DialogService>();

  Timer _timer;
  Timer _timerIOS;
  Timer _timerForPermission;
  Timer _timerForWhenInUse;

  Future<bool> askUserLocationDialog(BuildContext context) async {
    // FIXME: Translate
    final _contentFirstPart =
        'In order to be visible on map, PAM Map application require your device location permission. Press Yes to enable location permission, or if you just want to see other users on map without showing yourself press No';
    final _no = 'No';
    final _yes = 'Yes';

    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: Text(_contentFirstPart ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(_no),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(_yes),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;

    /*
    // IOS Implementation
    CupertinoAlertDialog(
      title: Text(_locationDialogTitle),
      content: Text("$_contentFirstPart $_yes $_contentSecondPart $_no"),
      actions: [
        FlatButton(
          child: Text(_no),
          onPressed: () async {
            userInfo.data["isVisible"] = false;
            await _firestoreProvider.addUserDocument(userInfo);
            _visibilityProvider.setIsVisible(false);
            Navigator.of(context).pop();
            if (!_visibilityProvider.isMapEntered) {
              Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
            } else {
              _visibilityProvider.canUserBeVisible = false;
            }
          },
        ),
        FlatButton(
          child: Text(_yes),
          onPressed: () async {
            userInfo.data["isVisible"] = false;
            await _firestoreProvider.addUserDocument(userInfo);
            final locationProvider = Provider.of<LocationProvider>(context, listen: false);
            final permissionStatus = await locationProvider.askForPermission();
            print(permissionStatus);
            if (permissionStatus == permission.PermissionStatus.denied) {
              // User select to not give permission
              userInfo.data["isVisible"] = false;
              await _firestoreProvider.addUserDocument(userInfo);
              _visibilityProvider.setIsVisible(false);
              Navigator.of(context).pop();
              if (!_visibilityProvider.isMapEntered) {
                Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
              } else {
                _visibilityProvider.canUserBeVisible = false;
              }
            } else if (permissionStatus == permission.PermissionStatus.granted) {
              // User allowed permission
              userInfo.data["isVisible"] = true;
              final serviceStatus = await Location().serviceEnabled();
              if (serviceStatus) {
                // Location service enabled
                await locationProvider.getCurrentLocation();
                locationProvider.startLocationUpdates();
                await _firestoreProvider.addUserDocument(userInfo);
                _visibilityProvider.setIsVisible(true);
                Navigator.of(context).pop();
                if (!_visibilityProvider.isMapEntered) {
                  Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                } else {
                  _visibilityProvider.canUserBeVisible = true;
                }
              } else {
                Navigator.of(context).pop();
                showEnableLocationDialog(context);
              }
            } else if (permissionStatus == permission.PermissionStatus.unknown) {
              Navigator.of(context).pop();
              openSettingsOrContinue(context);
            }
          },
        )
      ],
    );

     */
  }

  Future<bool> showEnableLocationServiceDialog(BuildContext context) async {
    // FIXME: Translate
    final content = 'To be visible on map, please enable location service on device and press OK';
    final no = 'No thank you';
    final yes = 'OK';

    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: Text(content ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(no),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(yes),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;

    /*
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => Platform.isAndroid
            ? AlertDialog(
                title: Text(tr("location.enable_dialog.title")),
                content: Text("$_firstPart 'YES' $_secondPart 'NO' $_thirdPart"),
                actions: [
                  FlatButton(
                    child: Text(tr("instagram.dialog.no")),
                    onPressed: () async {
                      userInfo.data["isVisible"] = false;
                      await _firestoreProvider.addUserDocument(userInfo);
                      _visibilityProvider.setIsVisible(false);
                      Navigator.of(context).pop();
                      if (!_visibilityProvider.isMapEntered) {
                        Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                      } else {
                        _visibilityProvider.canUserBeVisible = false;
                      }
                    },
                  ),
                  FlatButton(
                      child: Text(tr("instagram.dialog.yes")),
                      onPressed: () async {
                        await _runTimer(context);
                        OpenSettings.openLocationSetting();
                        Navigator.of(context).pop();
                        _continueAfterSettings(context);
                      })
                ],
              )
            : CupertinoAlertDialog(
                title: Text(tr("location.enable_dialog.title")),
                content: Text("$_firstPart 'YES' $_secondPart 'NO' $_thirdPart"),
                actions: [
                  FlatButton(
                    child: Text(tr("instagram.dialog.no")),
                    onPressed: () async {
                      userInfo.data["isVisible"] = false;
                      await _firestoreProvider.addUserDocument(userInfo);
                      _visibilityProvider.setIsVisible(false);
                      Navigator.of(context).pop();
                      if (!_visibilityProvider.isMapEntered) {
                        Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                      } else {
                        _visibilityProvider.canUserBeVisible = false;
                      }
                    },
                  ),
                  FlatButton(
                      child: Text(tr("instagram.dialog.yes")),
                      onPressed: () async {
                        await _runTimerIOS(context);
                        OpenSettings.openLocationSetting();
                        Navigator.of(context).pop();
                        continueAfterSettingsIOSLocationServiceDisabled(context);
                      })
                ],
              ));

     */
  }

  Future<bool> _continueAfterSettings(BuildContext context) async {
    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: Text(_firstPart ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(_openSettings),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(_continue),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;

    /*
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => Platform.isAndroid
            ? AlertDialog(
                title: Text(tr("location.continue_after_settings_dialog.title")),
                content: Text("$_firstPart $_openSettings $_secondPart $_continue $_thirdPart"),
                actions: [
                  FlatButton(
                      child: Text(_openSettings),
                      onPressed: () async {
                        OpenSettings.openLocationSetting();
                      }),
                  FlatButton(
                      child: Text(_continue),
                      onPressed: () async {
                        if (_timer != null) _timer.cancel();
                        userInfo.data["isVisible"] = false;
                        await _firestoreProvider.addUserDocument(userInfo);
                        _visibilityProvider.setIsVisible(false);
                        Navigator.of(context).pop();
                        if (!_visibilityProvider.isMapEntered) {
                          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                        } else {
                          _visibilityProvider.canUserBeVisible = false;
                        }
                      })
                ],
              )
            : CupertinoAlertDialog(
                title: Text(tr("location.continue_after_settings_dialog.title")),
                content: Text("$_firstPart $_openSettings $_secondPart $_continue $_thirdPart"),
                actions: [
                  FlatButton(
                      child: Text(_openSettings),
                      onPressed: () async {
                        OpenSettings.openLocationSetting();
                      }),
                  FlatButton(
                      child: Text(_continue),
                      onPressed: () async {
                        if (_timer != null) _timer.cancel();
                        userInfo.data["isVisible"] = false;
                        await _firestoreProvider.addUserDocument(userInfo);
                        _visibilityProvider.setIsVisible(false);
                        Navigator.of(context).pop();
                        if (!_visibilityProvider.isMapEntered) {
                          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                        } else {
                          _visibilityProvider.canUserBeVisible = false;
                        }
                      })
                ],
              ));

     */
  }

  Future<bool> continueAfterSettingsIOSLocationServiceDisabled(BuildContext context) async {
    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: Text(_firstPart ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(_openSettings),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(_continue),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;

    /*
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => CupertinoAlertDialog(
              title: Text(tr("location.continue_after_settings_dialog.title")),
              content: Text("$_firstPart $_openSettings $_secondPart $_continue $_thirdPart"),
              actions: [
                FlatButton(
                    child: Text(_openSettings),
                    onPressed: () async {
                      OpenSettings.openLocationSetting();
                    }),
                FlatButton(
                    child: Text(_continue),
                    onPressed: () async {
                      if (_timerIOS != null) _timerIOS.cancel();
                      userInfo.data["isVisible"] = false;
                      await _firestoreProvider.addUserDocument(userInfo);
                      _visibilityProvider.setIsVisible(false);
                      Navigator.of(context).pop();
                      if (!_visibilityProvider.isMapEntered) {
                        Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                      } else {
                        _visibilityProvider.canUserBeVisible = false;
                      }
                    })
              ],
            ));

     */
  }

  Future<bool> openSettingsOrContinue(BuildContext context) async {
    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: Text(_firstPart ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(_openSettings),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(_continue),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;

    /*
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => Platform.isAndroid
            ? AlertDialog(
                title: Text(tr("location.continue_after_settings_dialog.title")),
                content: Text("$_firstPart $_openSettings $_secondPart $_continue $_thirdPart"),
                actions: [
                  FlatButton(
                      child: Text(_openSettings),
                      onPressed: () async {
                        if (_timerForPermission == null) {
                          await _runTimerForPermission(context);
                        }
                        OpenSettings.openLocationSetting();
                      }),
                  FlatButton(
                      child: Text(_continue),
                      onPressed: () async {
                        // Close timer to not wait
                        if (_timerForPermission != null) {
                          _timerForPermission.cancel();
                          _timerForPermission = null;
                        }
                        userInfo.data["isVisible"] = false;
                        await _firestoreProvider.addUserDocument(userInfo);
                        _visibilityProvider.setIsVisible(false);
                        Navigator.of(context).pop();
                        if (!_visibilityProvider.isMapEntered) {
                          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                        } else {
                          _visibilityProvider.canUserBeVisible = false;
                        }
                      })
                ],
              )
            : CupertinoAlertDialog(
                title: Text(tr("location.continue_after_settings_dialog.title")),
                content: Text("$_firstPart $_openSettings $_secondPart $_continue $_thirdPart"),
                actions: [
                  FlatButton(
                      child: Text(_openSettings),
                      onPressed: () async {
                        if (_timerForPermission == null) {
                          await _runTimerForPermission(context);
                        }
                        OpenSettings.openLocationSetting();
                      }),
                  FlatButton(
                      child: Text(_continue),
                      onPressed: () async {
                        // Close timer to not wait
                        if (_timerForPermission != null) {
                          _timerForPermission.cancel();
                          _timerForPermission = null;
                        }
                        userInfo.data["isVisible"] = false;
                        await _firestoreProvider.addUserDocument(userInfo);
                        _visibilityProvider.setIsVisible(false);
                        Navigator.of(context).pop();
                        if (!_visibilityProvider.isMapEntered) {
                          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                        } else {
                          _visibilityProvider.canUserBeVisible = false;
                        }
                      })
                ],
              ));

     */
  }

  Future<bool> openSettingsWhenInUseChosen(BuildContext context) async {
    // FIXME: Translate
    final firstPart =
        'Application require location permission of type When In Use. Press Open Settings to navigate to system settings menu and choose Allow When In Use, or Press Continue to continue using application without sharing your location';

    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: Text(
          firstPart ?? '',
          textAlign: TextAlign.center,
        ),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(_openSettings),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(_continue),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;

    /*
    showDialog(
        context: context,
        barrierDismissible: false,
        builder: (ctx) => AlertDialog(
              title: Text(tr("location.continue_after_when_in_use.title")),
              content: Text(
                  "$firstPart $alwaysText $secondPart $_openSettings $thirdPart $alwaysText $fourthPart $_continue $continueText"),
              actions: [
                FlatButton(
                    child: Text(_openSettings),
                    onPressed: () async {
                      if (_timerForWhenInUse == null) {
                        await _runTimerForWhenInUsePermission(ctx);
                      }
                      OpenSettings.openLocationSetting();
                    }),
                FlatButton(
                    child: Text(_continue),
                    onPressed: () async {
                      // Close timer to not wait
                      if (_timerForWhenInUse != null) {
                        _timerForWhenInUse.cancel();
                        _timerForWhenInUse = null;
                      }
                      userInfo.data["isVisible"] = false;
                      await _firestoreProvider.addUserDocument(userInfo);
                      _visibilityProvider.setIsVisible(false);
                      Navigator.of(ctx).pop();
                      if (!_visibilityProvider.isMapEntered) {
                        Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
                      } else {
                        _visibilityProvider.canUserBeVisible = false;
                      }
                    })
              ],
            ));

     */
  }

  Future<bool> showAllowAlwaysDialog(BuildContext context) async {
    // FIXME: Translate
    final contentFirstPart =
        'To disable flashing blue bar on top of your phone from appearing constantly, press Open Settings and and from location permissions choose Always';
    final dontAskAgain = 'Dont ask again';

    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: RichText(
          text: TextSpan(
            style: TextStyle(color: Colors.black54),
            children: [
              TextSpan(text: contentFirstPart),
            ],
          ),
        ),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(dontAskAgain),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(_openSettings),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;
    /*
    // For iOS only
    showDialog(
        context: context,
        builder: (ctx) => CupertinoAlertDialog(
              title: Text(title),
              content: RichText(
                text: TextSpan(style: TextStyle(color: Colors.black54), children: [
                  TextSpan(text: contentFirstPart),
                  TextSpan(text: " $_openSettings", style: TextStyle(color: Theme.of(context).accentColor)),
                  TextSpan(text: " $contentSecondPart"),
                  TextSpan(text: " $allowAwaysText", style: TextStyle(color: Theme.of(context).accentColor)),
                ]),
              ),
              actions: [
                FlatButton(
                    child: Text(dontAskAgainText),
                    onPressed: () async {
                      _locationProvider.isAllowAlwaysPermissionDialogClosedByUser = true;
                      var prefs = await SharedPreferences.getInstance();
                      await prefs.setBool("allow_always_skip", true);
                      Navigator.of(context).pop();
                    }),
                FlatButton(
                    child: Text(
                      _openSettings,
                      style: TextStyle(color: Theme.of(context).accentColor),
                    ),
                    onPressed: () async {
                      OpenSettings.openLocationSetting();
                      Navigator.of(context).pop();
                    }),
              ],
            ));

     */
  }

/*
  Future<void> _runTimer(BuildContext context) async {
    _timer = Timer.periodic(new Duration(milliseconds: 500), (timer) async {
      final serviceStatus = await Location().serviceEnabled();
      print("Android timer entered");
      if (serviceStatus) {
        timer.cancel();
        _locationProvider.startLocationUpdates();
        userInfo.data["isVisible"] = true;
        await _firestoreProvider.addUserDocument(userInfo);
        _visibilityProvider.setIsVisible(true);
        if (!_visibilityProvider.isMapEntered) {
          Navigator.of(context).pushReplacementNamed(HomeScreen.routeName);
        } else {
          _visibilityProvider.canUserBeVisible = true;
          Navigator.of(context).pop();
        }
      }
    });
  }

   */

// TODO: Uncomment and fix for iOS
/*
  Future<void> _runTimerIOS(BuildContext context) async {
    _timerIOS = Timer.periodic(new Duration(milliseconds: 500), (timer) async {
      final serviceStatus = await Location().serviceEnabled();
      print("IOS timer entered");
      if (serviceStatus) {
        timer.cancel();
        final permissionStatus = await permission.LocationPermissions().checkPermissionStatus();
        if (permissionStatus == permission.PermissionStatus.granted) {
          // User have location permission
          _locationProvider.startLocationUpdates();
          currentPammapFirestoreUser.data["isVisible"] = true;
          await _firestoreProvider.addUserDocument(currentPammapFirestoreUser);
          _visibilityProvider.setIsVisible(true);
          if (!_visibilityProvider.isMapEntered) {
            Navigator.of(context).pushReplacementNamed(MainCarcasScreen.routeName);
          } else {
            _visibilityProvider.canUserBeVisible = true;
            Navigator.of(context).pop();
          }
        } else if (permissionStatus == permission.PermissionStatus.denied) {
          // User have denied permission when last time was asked, just stay in current dialogBox which is continueAfterSettingsIOS
          _visibilityProvider.setIsVisible(true);
          Navigator.of(context).pop();
          openSettingsOrContinue(context);
        } else if (permissionStatus == permission.PermissionStatus.unknown) {
          _visibilityProvider.setIsVisible(false);
          // User never been asked about permissions
          Navigator.of(context).pop();
          askUserLocationDialog(currentPammapFirestoreUser, context);
        }
      }
    });
  }
  */

// FIXME: Maybe remove since we only use whenInUse permission
/*
  Future<void> _runTimerForPermission(BuildContext context) async {
    _timerForPermission = Timer.periodic(Duration(milliseconds: 500), (timer) async {
      print("permission timer entered");
      final permissionStatus = await permission.LocationPermissions().checkPermissionStatus();
      if (permissionStatus == permission.PermissionStatus.granted) {
        _timerForPermission.cancel();
        _locationProvider.startLocationUpdates();
        currentPammapFirestoreUser.data["isVisible"] = true;
        await _firestoreProvider.addUserDocument(currentPammapFirestoreUser);
        _visibilityProvider.setIsVisible(true);
        Navigator.of(context).pop();
        if (!_visibilityProvider.isMapEntered) {
          Navigator.of(context).pushReplacementNamed(MainCarcasScreen.routeName);
        } else {
          _visibilityProvider.canUserBeVisible = true;
        }
      }
    });
  }
   */

/*
  Future<void> _runTimerForWhenInUsePermission(BuildContext context) async {
    _timerForWhenInUse = Timer.periodic(new Duration(milliseconds: 500), (timer) async {
      print("whenInUsePermission timer entered");
      final permissionStatus = await _locationProvider.getPermissionForAndroid();
      if (permissionStatus == helper.PermissionGroup.locationAlways) {
        // User chosed Allow Always location permission
        _timerForWhenInUse.cancel();
        currentPammapFirestoreUser.data["isVisible"] = true;
        await _firestoreProvider.addUserDocument(currentPammapFirestoreUser);
        _visibilityProvider.setIsVisible(true);
        final isServiceEnabled = await _locationProvider.isServiceEnabled();
        Navigator.of(context).pop();
        if (isServiceEnabled) {
          // Location service is enabled
          if (!_visibilityProvider.isMapEntered) {
            Navigator.of(context).pushReplacementNamed(MainCarcasScreen.routeName);
          } else {
            _visibilityProvider.canUserBeVisible = true;
          }
        } else {
          // Location service is disabled
          showEnableLocationDialog(context);
        }
      }
    });
  }

   */

  Future<bool> showLocationDisclosureDialog(BuildContext context) async {
    // FIXME: Translate
    final content = 'This app collects location data to enable real time users position updates on map';
    final ok = 'Ok I\'m in';
    final no = 'No thank you';

    final result = await _dialogService.showPlatform<bool>(
      context: context,
      child: PlatformAlertDialog(
        content: Text(content ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            child: Text(no),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            child: Text(ok),
            onPressed: () => _dialogService.hideActiveDialog(true),
          ),
        ],
      ),
    );

    return result ?? false;
  }
}
