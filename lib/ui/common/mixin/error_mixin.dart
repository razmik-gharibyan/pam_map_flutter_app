// @dart=2.9

import 'package:pam_map_flutter_app/core/model/common/common.dart';

class ErrorMixin {
  AppErrorCode mapFirebaseErrorToAppError(String errorCode) {
    final firebaseErrorCodeMap = {
      'auth/invalid-email': AppErrorCode.INVALID_EMAIL,
      'auth/wrong-password': AppErrorCode.WRONG_PASSWORD,
      'user-not-found': AppErrorCode.USER_NOT_FOUND,
      'email-already-in-use': AppErrorCode.EMAIL_ALREADY_IN_USE,
    };

    return firebaseErrorCodeMap[errorCode];
  }
}
