// @dart=2.9

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';

import 'confirmation_dialog_mixin.dart';

mixin LocationPermissionMixin on ConfirmationDialogMixin {
  Future<LocationPermission> requestPermission() {
    return Geolocator.requestPermission();
  }

  Future<LocationPermission> checkPermission() {
    return Geolocator.checkPermission();
  }

  Future<bool> isServiceEnabled() {
    return Geolocator.isLocationServiceEnabled();
  }

  Future<void> promptLocation({
    final BuildContext context,
    final VoidCallback onLocationServiceEnabled,
    final VoidCallback onLocationServiceDisabled,
    final VoidCallback onLocationServiceCanceled,
    final VoidCallback onPermissionNotGranted,
  }) async {
    final disclosureResult = await showLocationDisclosureDialog(context);
    if (disclosureResult) {
      LocationPermission res = await checkPermission();
      if (res == LocationPermission.denied) {
        res = await requestPermission();
      }

      if (res == LocationPermission.whileInUse) {
        bool locationServiceResult = await isServiceEnabled();

        if (locationServiceResult) {
          onLocationServiceEnabled?.call();
        } else {
          locationServiceResult = await showEnableLocationServiceDialog(context);

          if (locationServiceResult) {
            if (await isServiceEnabled()) {
              onLocationServiceEnabled?.call();
            } else {
              onLocationServiceDisabled?.call();
            }
          } else {
            onLocationServiceCanceled?.call();
          }
        }
      } else {
        onPermissionNotGranted?.call();
      }
    } else {
      onPermissionNotGranted?.call();
    }
  }
}
