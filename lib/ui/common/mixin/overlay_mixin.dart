// @dart=2.9

import 'package:flutter/material.dart';

class OverlayMixin {
  OverlayEntry _overlayEntry;

  void removeOverlay() {
    if (_overlayEntry != null) {
      _overlayEntry.remove();
      _overlayEntry = null;
    }
  }

  void insertOverlay(final BuildContext context, [final Widget child]) {
    _overlayEntry = _createOverlayEntry(child);
    Overlay.of(context).insert(_overlayEntry);
  }

  OverlayEntry _createOverlayEntry([final Widget child]) {
    return OverlayEntry(
      builder: (context) {
        return Material(
          color: Colors.black.withOpacity(.1),
          child: child,
        );
      },
    );
  }
}
