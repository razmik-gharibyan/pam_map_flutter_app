// @dart=2.9

import 'dart:io';

import 'package:flutter/material.dart';

class AppColors {
  AppColors._();

  static const primary = Color(0xFF006778);
  static const accent = Color(0xFF0093ab);
  static const error = Color(0xFFe91229);
  static const text = Color(0xFF354052);
  static const gray = Color(0xFFa8aab7);
  static const gray1 = Color(0xFF6d6e70);
  static const gray3 = Color(0xFF505050);
  static const lightGray = Color(0xFFbebebe);
  static const regentGray = Color(0xFF7f8fa4);
  static const lightestGray = Color(0xFFe1e6ed);
  static const lightestGray1 = Color(0xFFf1f4f8);
  static const lightestGray2 = Color(0xFFfafafa);
  static const lightestGray3 = Color(0xFFF7F6F3);
  static const lightestGray4 = Color(0xFFF8F8F8);
  static const lightestGray5 = Color(0xFFe0e3e8);
  static const lightestGray6 = Color(0xFFb0b0b1);
  static const lightestGray7 = Color(0xFFe6e7e8);
  static const lightestGray8 = Color(0xFFd8d8d8);
  static const lightestGray9 = Color(0xFFF6F6F6);
  static const bluishGray = Color(0xFF93b0bf);
  static const white = Color(0xFFFFFFFF);
  static const link = Color(0xFF5acce8);
  static const activeIcon = Color(0xFF00afc1);
  static const green = Color(0xFFb5d200);
  static const green1 = Color(0xFF6dd400);
  static const orange = Color(0xFFf99d00);
  static const paperYellow = Color(0xFFfdf4cb);
  static const keppel = Color(0xFF39ADA9);
  static const keppel1 = Color(0xFF0d939f);
  static const pampas = Color(0xFFf7f6f3);
  static const red = Color(0xFFf85359);
  static const red1 = Color(0xFFe02020);
  static const orange1 = Color(0xFFFADA5E);
  static const orange2 = Color(0xFFFEC701);
  static const lightPurple = Color(0xFFe7eaff);
  static const golden = Color(0xFFe3b80d);
  static const ink = Color(0xFF516173);
  static const blue = Color(0xFF2761ab);

  static const black = Colors.black;
  static const transparent = Colors.transparent;
}

class AppGradients {
  AppGradients._();

  static const turquoise = LinearGradient(
    colors: [Color(0xff1fdce8), Color(0xff09f1d1)],
    begin: Alignment.centerLeft,
    end: Alignment.centerRight,
  );

  static const lightestGray = LinearGradient(
    colors: [Color(0xfff2f4f7), AppColors.white],
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter,
  );

  static const orange = LinearGradient(
    colors: [AppColors.orange2, AppColors.orange1],
    begin: Alignment.bottomCenter,
    end: Alignment.topCenter,
  );
}

final theme = ThemeData(
  fontFamily: 'KumbhSans',
  primaryColor: AppColors.primary,
  accentColor: AppColors.accent,
  highlightColor: Platform.isIOS ? AppColors.transparent : null,
  splashColor: Platform.isIOS ? AppColors.transparent : null,
  disabledColor: AppColors.lightestGray,
  appBarTheme: AppBarTheme(
    iconTheme: IconThemeData(color: AppColors.black),
    elevation: 0,
    brightness: Brightness.light,
  ),
  textTheme: TextTheme(
    headline3: TextStyle(color: AppColors.text, fontSize: 32),
    headline4: TextStyle(color: AppColors.text, fontSize: 26),
    headline5: TextStyle(color: AppColors.text, fontSize: 24),
    headline6: TextStyle(color: AppColors.text, fontSize: 22),
    subtitle1: TextStyle(color: AppColors.text, fontSize: 18),
    bodyText1: TextStyle(color: AppColors.text, fontSize: 16),
    bodyText2: TextStyle(color: AppColors.text, fontSize: 15),
    caption: TextStyle(color: AppColors.text, fontSize: 14),
    subtitle2: TextStyle(color: AppColors.text, fontSize: 12),
  ),
  tooltipTheme: TooltipThemeData(
    padding: EdgeInsets.all(10),
    margin: EdgeInsets.all(20),
  ),
);
