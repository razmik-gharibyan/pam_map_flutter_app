// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:form_field_validator/form_field_validator.dart';
// FIXME: Translate
final requiredValidator = (final BuildContext context) => RequiredValidator(errorText: 'required');

String Function(String value) emailValidator(final BuildContext context) {
  return (String value) =>
      MultiValidator([requiredValidator(context), EmailValidator(errorText: 'Invalid email')])(
          (value ?? '').toLowerCase());
}

final passwordValidator = (final BuildContext context) => MultiValidator(
    [requiredValidator(context), MinLengthValidator(6, errorText: 'Password is too short')]);

final passwordMatchValidator =
    (final BuildContext context) => MatchValidator(errorText: 'Passwords do not match');
