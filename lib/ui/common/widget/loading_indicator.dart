// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

class LoadingIndicator extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return SizedBox(
      height: 20,
      width: 20,
      child: PlatformCircularProgressIndicator(
        material: (_, __) => MaterialProgressIndicatorData(strokeWidth: 2),
      ),
    );
  }
}
