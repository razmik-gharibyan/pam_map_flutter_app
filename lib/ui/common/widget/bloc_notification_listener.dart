// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pam_map_flutter_app/core/core.dart';

class BlocNotificationListener<B extends Bloc<dynamic, S>, S> extends StatefulWidget {
  final Widget child;
  final Duration infoDuration;
  final Duration errorDuration;
  final bool enabled;
  final ErrorResponse Function(S state) error;
  final String Function(S state) info;

  const BlocNotificationListener({
    Key key,
    @required this.child,
    this.infoDuration,
    this.errorDuration,
    this.enabled = true,
    this.error,
    this.info,
  })  : assert(enabled != null),
        assert(child != null),
        super(key: key);

  @override
  _BlocNotificationListenerState createState() => _BlocNotificationListenerState<B, S>();
}

class _BlocNotificationListenerState<B extends Bloc<dynamic, S>, S> extends State<BlocNotificationListener<B, S>>
    with NotificationServiceMixin {
  @override
  Widget build(final BuildContext context) {
    if (!widget.enabled) {
      return widget.child;
    }

    return BlocListener<B, S>(
      listener: _errorListener,
      child: widget.child,
    );
  }

  void _errorListener(final BuildContext context, S state) {
    final error = widget.error != null ? widget.error(state) : null;
    final info = widget.info != null ? widget.info(state) : null;

    if (error == null && info == null) {
      hideActiveNotification(context);
    }

    if (error != null) showHttpErrorNotification(context: context, err: error, duration: widget.errorDuration);

    if (info != null) showInfoNotification(context: context, message: info, duration: widget.infoDuration);
  }
}
