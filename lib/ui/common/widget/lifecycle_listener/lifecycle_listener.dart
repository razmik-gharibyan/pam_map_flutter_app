// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../common.dart';

class LifecycleListener extends StatefulWidget {
  final Widget child;

  LifecycleListener({@required this.child});

  @override
  _LifecycleListenerState createState() => _LifecycleListenerState();
}

class _LifecycleListenerState extends State<LifecycleListener> with WidgetsBindingObserver {

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed: {
        context.read<LifecycleBloc>().add(LifecycleChanged(event: AppLifecycleEvent.Resumed));
      }
      break;
      case AppLifecycleState.inactive: {
        context.read<LifecycleBloc>().add(LifecycleChanged(event: AppLifecycleEvent.Inactive));
      }
      break;
      case AppLifecycleState.paused: {
        context.read<LifecycleBloc>().add(LifecycleChanged(event: AppLifecycleEvent.Paused));
      }
      break;
      case AppLifecycleState.detached: {
        context.read<LifecycleBloc>().add(LifecycleChanged(event: AppLifecycleEvent.Detached));
      }
      break;
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
