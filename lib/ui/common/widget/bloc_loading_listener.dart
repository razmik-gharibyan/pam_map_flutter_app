// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../mixin/mixin.dart';
import 'centered_loading_indicator.dart';

class BlocLoadingListener<B extends Bloc<dynamic, S>, S> extends StatefulWidget {
  final Widget child;
  final bool Function(S state) inProgress;
  final bool enabled;

  const BlocLoadingListener({
    Key key,
    @required this.child,
    @required this.inProgress,
    this.enabled = true,
  })  : assert(enabled != null),
        assert(child != null),
        super(key: key);

  @override
  _BlocLoadingListenerState createState() => _BlocLoadingListenerState<B, S>();
}

class _BlocLoadingListenerState<B extends Bloc<dynamic, S>, S> extends State<BlocLoadingListener<B, S>>
    with OverlayMixin {
  bool _lastInProgressState = false;

  @override
  Widget build(final BuildContext context) {
    if (!widget.enabled) {
      return widget.child;
    }

    return BlocListener<B, S>(
      listener: _loadingListener,
      child: widget.child,
    );
  }

  @override
  void dispose() {
    super.dispose();
    removeOverlay();
  }

  void _loadingListener(final BuildContext context, S state) {
    final inProgress = widget.inProgress != null ? widget.inProgress(state) : false;

    if (inProgress != _lastInProgressState) {
      _lastInProgressState = inProgress;

      if (_lastInProgressState) {
        insertOverlay(context, CenteredLoadingIndicator());
      } else {
        removeOverlay();
      }
    }
  }
}
