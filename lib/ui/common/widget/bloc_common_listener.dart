// @dart=2.9

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import 'bloc_loading_listener.dart';
import 'bloc_notification_listener.dart';

class BlocCommonListener<B extends Bloc<dynamic, S>, S> extends StatefulWidget {
  final Widget child;
  final Duration infoDuration;
  final Duration errorDuration;
  final bool enabled;
  final String Function(S state) info;
  final ErrorResponse Function(S state) error;
  final bool Function(S state) inProgress;

  const BlocCommonListener({
    Key key,
    @required this.child,
    this.inProgress,
    this.infoDuration,
    this.errorDuration,
    this.enabled = true,
    this.error,
    this.info,
  })  : assert(enabled != null),
        assert(child != null),
        super(key: key);

  @override
  _BlocCommonListenerState createState() => _BlocCommonListenerState<B, S>();
}

class _BlocCommonListenerState<B extends Bloc<dynamic, S>, S> extends State<BlocCommonListener<B, S>> {
  @override
  Widget build(final BuildContext context) {
    return BlocNotificationListener<B, S>(
      infoDuration: widget.infoDuration,
      errorDuration: widget.errorDuration,
      enabled: widget.enabled,
      error: widget.error,
      info: widget.info,
      child: BlocLoadingListener<B, S>(
        enabled: widget.enabled,
        inProgress: widget.inProgress,
        child: widget.child,
      ),
    );
  }
}
