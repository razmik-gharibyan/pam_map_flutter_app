// @dart=2.9

import 'package:flutter/material.dart';

import 'loading_indicator.dart';

class CenteredLoadingIndicator extends StatelessWidget {
  @override
  Widget build(final BuildContext context) {
    return Center(
      child: LoadingIndicator(),
    );
  }
}
