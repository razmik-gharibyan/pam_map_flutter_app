// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LifecycleState extends Equatable {
  const LifecycleState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class LifecycleInitial extends LifecycleState {}

class LifecycleResumed extends LifecycleState {}

class LifecycleInactive extends LifecycleState {}

class LifecyclePaused extends LifecycleState {}

class LifecycleDetached extends LifecycleState {}
