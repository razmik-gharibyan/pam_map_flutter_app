// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

enum AppLifecycleEvent {
  Resumed, Inactive, Paused, Detached
}

@immutable
abstract class LifecycleEvent extends Equatable {

  const LifecycleEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;

}

class LifecycleStartedEvent extends LifecycleEvent {}

class LifecycleChanged extends LifecycleEvent {
  final AppLifecycleEvent event;

  const LifecycleChanged({@required this.event});

  @override
  List<Object> get props => [event];
}