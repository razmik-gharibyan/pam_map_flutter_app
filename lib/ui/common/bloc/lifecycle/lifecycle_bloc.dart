// @dart=2.9

import 'package:bloc/bloc.dart';

import 'lifecycle_event.dart';
import 'lifecycle_state.dart';

class LifecycleBloc extends Bloc<LifecycleEvent, LifecycleState> {
  LifecycleBloc() : super(LifecycleInitial());

  @override
  Stream<LifecycleState> mapEventToState(LifecycleEvent event) async* {
    if (event is LifecycleChanged) {
      yield* _mapAppLifecycleChangedToState(event);
    }
  }

  Stream<LifecycleState> _mapAppLifecycleChangedToState(final LifecycleChanged event) async* {
    switch (event.event) {
      case AppLifecycleEvent.Resumed:
        yield LifecycleResumed();

        break;
      case AppLifecycleEvent.Inactive:
        yield LifecycleInactive();

        break;
      case AppLifecycleEvent.Paused:
        yield LifecyclePaused();

        break;
      case AppLifecycleEvent.Detached:
        yield LifecycleDetached();

        break;
    }
  }
}
