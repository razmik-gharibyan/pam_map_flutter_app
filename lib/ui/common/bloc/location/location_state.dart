// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LocationState extends Equatable {
  const LocationState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class LocationInitial extends LocationState {}

class LocationCanceled extends LocationState {}
