// @dart=2.9

import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import 'location_event.dart';
import 'location_state.dart';

class LocationBloc extends Bloc<LocationEvent, LocationState> {
  final _firestoreService = injector<FirestoreService>();
  final _userRepository = injector<UserRepository>();
  StreamSubscription<Position> _positionStream;

  LocationBloc() : super(LocationInitial());

  @override
  Stream<LocationState> mapEventToState(LocationEvent event) async* {
    if (event is LocationStarted) {
      yield* _mapAppStartedToState();
    } else if (event is LocationCancelPressed) {
      yield* _mapLocationCancelPressedToState();
    }
  }

  Stream<LocationState> _mapAppStartedToState() async* {
    if (_positionStream == null) {
      final locationSettings = LocationSettings(
        accuracy: LocationAccuracy.high,
        distanceFilter: 30,
      );

      final user = await _userRepository.getUserInfo();

      await _firestoreService.updateUserDocument(
        UserProfileInfo(isVisible: true),
        user.uid,
      );

      _positionStream =
          Geolocator.getPositionStream(locationSettings: locationSettings).listen((Position position) async {
        if (position != null) {
          await _firestoreService.updateUserDocument(
            UserProfileInfo(location: GeoPoint(position.latitude, position.longitude)),
            user.uid,
          );
        } else {
          add(LocationCancelPressed());
        }
      });
    }
  }

  Stream<LocationState> _mapLocationCancelPressedToState() async* {
    final user = await _userRepository.getUserInfo();

    await _firestoreService.updateUserDocument(
      UserProfileInfo(isVisible: false),
      user.uid,
    );

    _positionStream.cancel();

    yield LocationCanceled();
  }
}
