// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class LocationEvent extends Equatable {
  const LocationEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class LocationStarted extends LocationEvent {}

class LocationCancelPressed extends LocationEvent {}
