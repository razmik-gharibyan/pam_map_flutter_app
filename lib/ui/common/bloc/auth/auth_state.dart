// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:meta/meta.dart';

@immutable
abstract class AuthState extends Equatable {
  const AuthState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class AuthUninitialized extends AuthState {}

class AuthAuthenticated extends AuthState {
  final User user;
  final bool hasInstagram;

  AuthAuthenticated({
    @required this.user,
    this.hasInstagram = false,
  });

  @override
  List<Object> get props => [
        user,
        hasInstagram,
      ];
}

class AuthUnauthenticated extends AuthState {}

class AuthInProgress extends AuthState {}
