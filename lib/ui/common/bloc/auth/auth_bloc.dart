// @dart=2.9

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import 'auth_event.dart';
import 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final _userRepository = injector<UserRepository>();
  final _firestoreService = injector<FirestoreService>();

  User get user => state is AuthAuthenticated ? (state as AuthAuthenticated).user : null;

  AuthBloc() : super(AuthUninitialized());

  @override
  Stream<AuthState> mapEventToState(AuthEvent event) async* {
    if (event is AppStarted) {
      yield* _mapAppStartedToState(event);
    } else if (event is LoggedIn) {
      yield* _mapLoginToState(event);
    } else if (event is LoggedOut) {
      yield* _mapLogoutToState(event);
    }
  }

  Stream<AuthState> _mapAppStartedToState(final AppStarted event) async* {
    final refreshToken = await _userRepository.getRefreshToken();
    if (refreshToken != null) {
      try {
        final user = await _userRepository.getUserInfo();

        final userProfileInfo = await _firestoreService.getUserDocument(user.uid);

        yield AuthAuthenticated(user: user, hasInstagram: userProfileInfo.hasInstagram);
      } catch (err) {
        yield AuthUnauthenticated();
      }
    } else {
      await _userRepository.logout();
      yield AuthUnauthenticated();
    }
  }

  Stream<AuthState> _mapLoginToState(final LoggedIn event) async* {
    yield AuthInProgress();

    try {
      final user = await _userRepository.getUserInfo();

      final userProfileInfo = await _firestoreService.getUserDocument(user.uid);

      yield AuthAuthenticated(
        user: user,
        hasInstagram: userProfileInfo.hasInstagram,
      );
    } catch (err) {
      yield AuthUnauthenticated();
    }
  }

  Stream<AuthState> _mapLogoutToState(final LoggedOut event) async* {
    yield AuthInProgress();

    await _userRepository.logout();

    yield AuthUnauthenticated();
  }
}
