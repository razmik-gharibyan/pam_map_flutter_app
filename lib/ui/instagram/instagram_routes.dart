// @dart=2.9

import 'connect/connect.dart';

final instagramRoutes = {
  'instagram/connect': (_) => InstagramConnectScreen(),
  'instagram/connect/inform': (_) => InformInstagramConnectScreen(),
};