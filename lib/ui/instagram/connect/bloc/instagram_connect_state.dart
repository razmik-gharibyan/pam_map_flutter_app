// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:pam_map_flutter_app/core/core.dart';

@immutable
abstract class InstagramConnectState extends Equatable {
  const InstagramConnectState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InstagramConnectInitial extends InstagramConnectState {}

class InstagramConnectStarted extends InstagramConnectState {
  final String instagramState;

  const InstagramConnectStarted({this.instagramState});

  @override
  List<Object> get props => [];
}

class InstagramConnectInProgress extends InstagramConnectState {}

class InstagramConnectCanceled extends InstagramConnectState {}

class InstagramConnectSucceed extends InstagramConnectState {}

class InstagramConnectFailure extends InstagramConnectState {
  final ErrorResponse err;

  const InstagramConnectFailure({@required this.err});

  @override
  List<Object> get props => [err];
}
