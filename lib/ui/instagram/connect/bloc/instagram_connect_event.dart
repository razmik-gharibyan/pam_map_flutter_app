// @dart=2.9

import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class InstagramConnectEvent extends Equatable {
  const InstagramConnectEvent();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => true;
}

class InstagramConnectEventStarted extends InstagramConnectEvent {}

class InstagramConnectCancelPressed extends InstagramConnectEvent {}

class InstagramConnected extends InstagramConnectEvent {
  final String code;

  const InstagramConnected({this.code});

  @override
  List<Object> get props => [code];
}
