// @dart=2.9

import 'package:bloc/bloc.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../../../common/common.dart';
import 'instagram_connect_event.dart';
import 'instagram_connect_state.dart';

class InstagramConnectBloc extends Bloc<InstagramConnectEvent, InstagramConnectState> with ErrorMixin {
  static const _userInstagramAuthStateStorageKey = 'user_instagram_auth_state';
  static const _lastUserId = 'last_user_id';
  static const _longLiveAccessToken = 'long_live_access_token';
  final _userRepository = injector<UserRepository>();
  final _instagramService = injector<InstagramService>();
  final _firestoreService = injector<FirestoreService>();
  final _storage = injector<FlutterSecureStorage>();

  InstagramConnectBloc() : super(InstagramConnectInitial());

  @override
  Stream<InstagramConnectState> mapEventToState(final InstagramConnectEvent event) async* {
    if (event is InstagramConnectEventStarted) {
      yield* _mapStartedToState();
    } else if (event is InstagramConnectCancelPressed) {
      yield* _mapCancelPressedToState();
    } else if (event is InstagramConnected) {
      yield* _mapConnectedToState(event.code);
    }
  }

  Stream<InstagramConnectState> _mapStartedToState() async* {
    yield InstagramConnectInProgress();

    try {
      final result = await _storage.read(key: _userInstagramAuthStateStorageKey);

      yield InstagramConnectStarted(instagramState: result);
    } on FirebaseAuthException catch (err) {
      yield InstagramConnectFailure(
        err: ErrorResponse([
          ErrorItem(
            errorCode: mapFirebaseErrorToAppError(err.code),
            description: err.message,
          )
        ]),
      );
    }
  }

  Stream<InstagramConnectState> _mapCancelPressedToState() async* {
    yield InstagramConnectInProgress();

    try {
      await _userRepository.logout();

      yield InstagramConnectCanceled();
    } on FirebaseAuthException catch (err) {
      yield InstagramConnectFailure(
        err: ErrorResponse([
          ErrorItem(
            errorCode: mapFirebaseErrorToAppError(err.code),
            description: err.message,
          )
        ]),
      );
    }
  }

  Stream<InstagramConnectState> _mapConnectedToState(final String code) async* {
    yield InstagramConnectInProgress();

    try {
      final authResponse = await _instagramService.getAccessInfo(code);

      final longAuthResponse = await _instagramService.getLongAccessInfo(authResponse.accessToken);

      final username = await _instagramService.getUsername(authResponse.userId, longAuthResponse.accessToken);

      final instagramProfileInfo = await _instagramService.getProfileInfo(username);

      final user = await _userRepository.getUserInfo();

      await _firestoreService.updateUserDocument(
        UserProfileInfo(
          instagramInfo: instagramProfileInfo,
        ),
        user.uid,
      );

      await _storage.write(key: _userInstagramAuthStateStorageKey, value: 'login');

      await _storage.write(key: _lastUserId, value: user.uid);

      await _storage.write(key: _longLiveAccessToken, value: longAuthResponse.accessToken);

      yield InstagramConnectSucceed();
    } on FirebaseAuthException catch (err) {
      yield InstagramConnectFailure(
        err: ErrorResponse([
          ErrorItem(
            errorCode: mapFirebaseErrorToAppError(err.code),
            description: err.message,
          )
        ]),
      );
    }
  }
}
