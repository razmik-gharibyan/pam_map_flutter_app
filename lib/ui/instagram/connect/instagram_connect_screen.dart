// @dart=2.9

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:geolocator/geolocator.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../../common/common.dart';
import 'bloc/bloc.dart';

class InstagramConnectScreen extends StatelessWidget with ConfirmationDialogMixin, LocationPermissionMixin  {
  String _initialUrl = "https://api.instagram.com/oauth/authorize?app_id=460485674626498&redirect_uri" +
      "=https://narsad.github.io/Loading-map/?fbclid=IwAR0PWa2a1FdrIBm1y3UayI3OMOxBsxbxkBfXv_ibKksmmCzg3MHXgNqTuaM" +
      "&scope=user_profile,user_media&response_type=code";
  final _redirectFullBeforeCode = "https://narsad.github.io/Loading-map/?code=";
  final _redirectBeforeCode = "Loading-map/?code=";
  final _redirectAfterCode = "#_";
  final _instagramUrl = "https://www.instagram.com/";
  final _logoutUrl = "https://instagram.com/accounts/logout/";

  // FIXME: Translate
  final _backPressedDialogTitle = 'Are you sure?';
  final _topText = 'Connect Instagram account';

  final _dialogService = injector<DialogService>();
  final _navigationService = injector<NavigationService>();

  WebViewController _controller;

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final mediaQuery = MediaQuery.of(context);

    return BlocProvider<InstagramConnectBloc>(
      create: (_) => InstagramConnectBloc()..add(InstagramConnectEventStarted()),
      child: BlocCommonListener<InstagramConnectBloc, InstagramConnectState>(
        inProgress: (state) => state is InstagramConnectInProgress,
        error: (state) => state is InstagramConnectFailure ? state.err : null,
        child: BlocListener<InstagramConnectBloc, InstagramConnectState>(
          listener: (context, state) {
            if (state is InstagramConnectStarted) {
              if (state.instagramState != null && state.instagramState == 'logout' && _controller != null) {
                _controller.loadUrl(_logoutUrl);
              }
            } else if (state is InstagramConnectCanceled) {
              _navigationService.pop(true);
            } else if (state is InstagramConnectSucceed) {
              _requestLocationUpdates(context);
            }
          },
          child: BlocBuilder<InstagramConnectBloc, InstagramConnectState>(
            builder: (context, state) {
              return WillPopScope(
                child: Scaffold(
                  appBar: AppBar(
                    backgroundColor: AppColors.primary,
                    elevation: 0.5,
                    iconTheme: IconThemeData(
                      color: Colors.white, //change your color here
                    ),
                    title: Text(
                      _topText,
                      textAlign: TextAlign.center,
                      style: textTheme.bodyText1.copyWith(color: AppColors.white),
                    ),
                  ),
                  body: SingleChildScrollView(
                    child: InkWell(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Container(
                            height: mediaQuery.size.height * 0.9,
                            child: Stack(
                              children: [
                                Container(
                                  child: WebView(
                                    initialUrl: _initialUrl,
                                    javascriptMode: JavascriptMode.unrestricted,
                                    gestureNavigationEnabled: true,
                                    onWebViewCreated: (webViewController) => _controller = webViewController,
                                    navigationDelegate: (request) {
                                      if (request.url.startsWith("http://www.gstatic.com")) {
                                        return NavigationDecision.prevent;
                                      } else if (request.url.startsWith("https://play.google.com")) {
                                        return NavigationDecision.prevent;
                                      }
                                      return NavigationDecision.navigate;
                                    },
                                    onPageStarted: (_) => SystemChannels.textInput.invokeMethod("TextInput.hide"),
                                    onPageFinished: (url) {
                                      if (url.contains(_redirectFullBeforeCode)) {
                                        _onInstagramConnectSuccess(context, url);
                                      } else if (url == _instagramUrl) {
                                        _controller.loadUrl(_initialUrl);
                                      }
                                    },
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                onWillPop: () => _onBackPressed(context),
              );
            },
          ),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed(final BuildContext context) {
    return _dialogService.showPlatform(
      context: context,
      child: PlatformAlertDialog(
        content: Text(_backPressedDialogTitle ?? '', textAlign: TextAlign.center),
        actions: <Widget>[
          PlatformDialogAction(
            // FIXME: Translate
            child: Text('No'),
            onPressed: () => _dialogService.hideActiveDialog(),
          ),
          PlatformDialogAction(
            // FIXME: Translate
            child: Text('Yes'),
            onPressed: () {
              _dialogService.hideActiveDialog();
              context.read<InstagramConnectBloc>().add(InstagramConnectCancelPressed());
            },
          ),
        ],
      ),
    );
  }

  void _onInstagramConnectSuccess(final BuildContext context, final String url) {
    int index = url.lastIndexOf(_redirectBeforeCode) + _redirectBeforeCode.length;
    int hashTagIndex = url.lastIndexOf(_redirectAfterCode);
    String code = url.substring(index, hashTagIndex);

    context.read<InstagramConnectBloc>().add(InstagramConnected(code: code));
  }

  void _requestLocationUpdates(final BuildContext context) async {
    await promptLocation(
      context: context,
      onLocationServiceEnabled: () {
        _startLocationUpdates(context);
        _navigateToHomeScreen();
      },
      onLocationServiceDisabled: () {
        _requestLocationUpdates(context);
      },
      onLocationServiceCanceled: () {
        _navigateToHomeScreen();
      },
      onPermissionNotGranted: () {
        _navigateToHomeScreen();
      },
    );
  }

  void _startLocationUpdates(final BuildContext context) {
    BlocProvider.of<LocationBloc>(context).add(LocationStarted());
  }

  void _navigateToHomeScreen() {
    _navigationService.pushNamedAndRemoveUntil('home', (route) => (route.settings.name ?? '') == 'auth');
  }
}
