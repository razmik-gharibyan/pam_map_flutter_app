// @dart=2.9

import 'package:flutter/material.dart';
import 'package:pam_map_flutter_app/common/common.dart';
import 'package:pam_map_flutter_app/core/core.dart';

import '../../common/common.dart';

class InformInstagramConnectScreen extends StatelessWidget {
  final _navigationService = injector<NavigationService>();

  // FIXME: Translate
  final _topText = 'People Around Me';
  final _connectAccountTitle = 'Connect Instagram account';
  final _connectAccountDescription =
      'Press on Connect Account button and log in with your Instagram account to connect it.';
  final _connectAccountButtonText = 'Connect Account';
  final _connectAccountNote =
      '* application will use your Instagram profile picture, username, number of followers and account type to show you on map (after connecting account, you can choose to be invisible on map)';

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final mediaQuery = MediaQuery.of(context);

    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.primary,
        elevation: 0.5,
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        title: Text(
          _topText,
          textAlign: TextAlign.center,
          style: textTheme.bodyText1.copyWith(color: AppColors.white),
        ),
      ),
      body: LayoutBuilder(
        builder: (ctx, constraints) => Center(
          child: Container(
            width: constraints.maxWidth * 0.8,
            margin: EdgeInsets.only(
                top: (mediaQuery.size.height * 30) / screenHeightCoefficent,
                bottom: (mediaQuery.size.height * 100) / screenHeightCoefficent),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey,
                  offset: Offset(0.0, 1.0), //(x,y)
                  blurRadius: 6.0,
                ),
              ],
            ),
            child: Stack(
              children: [
                Column(
                  children: [
                    SizedBox(
                      height: constraints.maxHeight * 0.06,
                    ),
                    Container(
                      height: constraints.maxHeight * 0.05,
                      child: Text(
                        _connectAccountTitle,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: (mediaQuery.size.height * 13) / screenHeightCoefficent,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: constraints.maxHeight * 0.04,
                    ),
                    Container(
                      height: constraints.maxHeight * 0.1,
                      padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.07),
                      child: Text(
                        _connectAccountDescription,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: (mediaQuery.size.height * 11) / screenHeightCoefficent,
                          fontWeight: FontWeight.bold,
                          color: Colors.black54,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: constraints.maxHeight * 0.02,
                    ),
                    SingleChildScrollView(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          SizedBox(
                            height: constraints.maxHeight * 0.01,
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(
                              horizontal: constraints.maxWidth * 0.1,
                            ),
                            child: Container(
                              height: constraints.maxHeight * 0.13,
                              alignment: Alignment.center,
                              child: ButtonTheme(
                                minWidth: (mediaQuery.size.height * 250) / screenHeightCoefficent,
                                height: (mediaQuery.size.height * 35) / screenHeightCoefficent,
                                child: RaisedButton(
                                  child: Text(
                                    _connectAccountButtonText,
                                    style: TextStyle(
                                      fontSize: (mediaQuery.size.height * 12) / screenHeightCoefficent,
                                    ),
                                  ),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(30),
                                  ),
                                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                                  color: Theme.of(context).primaryColor,
                                  textColor: Colors.white,
                                  splashColor: Theme.of(context).accentColor,
                                  onPressed: () => _openConnectInstagramScreen(),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: constraints.maxHeight * 0.02,
                          ),
                          Container(
                            height: constraints.maxHeight * 0.23,
                            padding: EdgeInsets.symmetric(horizontal: constraints.maxWidth * 0.07),
                            child: Text(
                              _connectAccountNote,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: (mediaQuery.size.height * 8) / screenHeightCoefficent,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54,
                              ),
                            ),
                          ),
                          Container(
                            width: constraints.maxWidth * 0.5,
                            padding: EdgeInsets.only(left: (mediaQuery.size.width * 5) / screenWidthCoefficent),
                            child: FlatButton(
                              child: Container(
                                  height: constraints.maxHeight * 0.07,
                                  child: Row(crossAxisAlignment: CrossAxisAlignment.center, children: [
                                    Icon(
                                      Icons.arrow_back_ios,
                                      color: Theme.of(context).primaryColor,
                                      size: (mediaQuery.size.height * 13) / screenHeightCoefficent,
                                    ),
                                    Container(
                                      padding: EdgeInsets.only(
                                        top: mediaQuery.size.height * 2.5 / screenHeightCoefficent,
                                        left: mediaQuery.size.height * 2.5 / screenHeightCoefficent,
                                      ),
                                      child: Text(
                                        // FIXME: Translate
                                        'back to login',
                                        style: TextStyle(
                                          color: Theme.of(context).primaryColor,
                                          fontSize: (mediaQuery.size.height * 13) / screenHeightCoefficent,
                                        ),
                                      ),
                                    ),
                                  ])),
                              materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                              onPressed: _onBackPressed,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _openConnectInstagramScreen() {
    _navigationService.pushReplacementNamed('instagram/connect');
  }

  void _onBackPressed() {
    _navigationService.pop(true);
  }
}
