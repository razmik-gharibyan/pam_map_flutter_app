// @dart=2.9

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';
import 'package:pam_map_flutter_app/di_registery.dart';
import 'package:pam_map_flutter_app/ui/ui.dart';

import 'common/common.dart';
import 'core/core.dart';
import 'ui/routes.dart';

void main() async {
  // Flutter engine initialization
  WidgetsFlutterBinding.ensureInitialized();

  // Firebase sdk initialization
  try {
    await Firebase.initializeApp();
  } catch (err) {
    print('Unable to initialization firebase sdk $err');
  }

  configure();

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  final _navigatorKey = injector<NavigationService>().getNavigatorKey();

  @override
  Widget build(BuildContext context) {
    final locale = 'en';
    Intl.defaultLocale = locale;

    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(
          create: (context) => AuthBloc()..add(AppStarted()),
        ),
        BlocProvider<LocationBloc>(
          create: (context) => LocationBloc(),
        ),
        BlocProvider<LifecycleBloc>(
          create: (context) => LifecycleBloc()..add(LifecycleStartedEvent()),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: theme,
        navigatorKey: _navigatorKey,
        routes: routes,
        locale: Locale(locale),
        localizationsDelegates: AppLocalizations.localizationsDelegates,
        supportedLocales: AppLocalizations.supportedLocales,
        builder: (context, child) {
          return LifecycleListener(child: child);
        },
        home: InitialScreen(),
      ),
    );
  }
}
