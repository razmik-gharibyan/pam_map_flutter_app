// @dart=2.9

import 'package:bloc_test/bloc_test.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';
import 'package:pam_map_flutter_app/core/core.dart';
import 'package:pam_map_flutter_app/ui/settings/border/bloc/bloc.dart';
import 'package:quiver/core.dart';

class MockFirestoreService extends Mock implements FirestoreService {}

class MockUserRepository extends Mock implements UserRepository {}

class MockSettingsRepository extends Mock implements SettingsRepository {}

class MockUser extends Mock implements User {}

class MockUserProfileInfo extends Mock implements UserProfileInfo {}

class MockBorderResponse extends Mock implements BorderResponse {}

class SelectBorderEventFake extends Fake implements SelectBorderEvent {}

class SelectBorderStateFake extends Fake implements SelectBorderState {}

void main() {
  MockFirestoreService _firestoreService;
  MockUserRepository _userRepository;
  MockSettingsRepository _settingsRepository;

  group('SelectBorderBloc', () {
    setUpAll(() {
      _firestoreService = MockFirestoreService();
      _userRepository = MockUserRepository();
      _settingsRepository = MockSettingsRepository();
      when(() => _userRepository.getUserInfo()).thenAnswer((_) async => MockUser());
      when(() => _firestoreService.getUserDocument(any())).thenAnswer((_) async => MockUserProfileInfo());
      when(() => _settingsRepository.getBorders()).thenAnswer((_) async => [MockBorderResponse()]);
      registerFallbackValue(SelectBorderEventFake());
      registerFallbackValue(SelectBorderStateFake());
    });

    blocTest<SelectBorderBloc, SelectBorderState>(
      'initial state is correct and emits [loading, success] when initialized',
      build: () => SelectBorderBloc(
        firestoreService: _firestoreService,
        userRepository: _userRepository,
        settingsRepository: _settingsRepository,
      ),
      act: (bloc) => bloc.add(SelectBorderStarted()),
      expect: () => <SelectBorderState>[
        SelectBorderState(
          currentUser: null,
          borders: [],
          inProgress: true,
          err: Optional.absent(),
        ),
        SelectBorderState(
          currentUser: MockUserProfileInfo(),
          borders: [MockBorderResponse()],
          inProgress: false,
          err: Optional.absent(),
        ),
      ],
    );

    blocTest<SelectBorderBloc, SelectBorderState>(
      'emits [loading, failure] when select border update',
      setUp: () {
        when(() => _userRepository.getUserInfo()).thenThrow(Exception('oops'));
      },
      build: () => SelectBorderBloc(
        firestoreService: _firestoreService,
        userRepository: _userRepository,
        settingsRepository: _settingsRepository,
      ),
      act: (bloc) => bloc.add(SelectBorderUpdated()),
      expect: () => <SelectBorderState>[
        SelectBorderState(
          currentUser: null,
          borders: [],
          inProgress: true,
          err: Optional.absent(),
        ),
      ],
    );
  });
}
