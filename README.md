# People Around Me MAP

## CI/CD
### Setup

1. Create a second account with `App Manager` permission to App Store Connect without 2FA-enabled.
   [Apple docs](https://support.appmachine.com/hc/en-us/articles/203647716-Authorize-an-Additional-User-to-your-iOS-Developer-Account)

2. Create google service account.
   [Fastlane docs](https://docs.fastlane.tools/getting-started/android/setup/)

3. Create keystore for android
   ```
   keytool -genkey -v -keystore ~/key.jks -keyalg RSA -keysize 2048 -validity 10000 -alias key
    ```

4. Update signing config in `android/app/build.grade`
   ```
    ...
    signingConfigs {
        release {
            keyAlias System.getenv('ANDROID_KEY_ALIAS')
            keyPassword System.getenv('ANDROID_KEY_PASSWORD')
            storeFile System.getenv('ANDROID_STORE_FILE') ? file(System.getenv('ANDROID_STORE_FILE')) : null
            storePassword System.getenv('ANDROID_STORE_PASSWORD')
        }
    }
    ...
   ```

5. Install fastlane
   ```
   gem install fastlane
   ```

6. Create git repository for IOS certificates
7. Create build flavors

[Setup build flavors](https://medium.com/@animeshjain/build-flavors-in-flutter-android-and-ios-with-different-firebase-projects-per-flavor-27c5c5dac10b)

[Setup CI/CD](https://appditto.com/blog/automate-your-flutter-workflow)

### Build commands

#### Flutter
```

# Save app env variables
dart parse_env.dart

```

#### Android
```
# Build the release binaries:
bundle exec fastlane build_android

# Google play deploy:
bundle exec fastlane deploy_android

```

#### IOS
```
# Build the release binaries:
bundle exec fastlane build_ios

# TestFlight deploy:
bundle exec fastlane deploy_ios

# Update certificates:
bundle exec fastlane match --api_key_path ./path/to/fastlane_api_key.json
```

### ENV variables

#### App

```sh
export API_BASE_URL="https://example.com"
export OAUTH_BASE_URL="https://example.com"
export CLIENT_ID=""
export CLIENT_SECRET=""
export GOOGLE_API_KEY=""
export SURVEY_URL_EN=""
export SURVEY_URL_NL=""
export SUPPORT_URL=""
```


#### Android

```sh
export ANDROID_KEY_ALIAS="key"
export ANDROID_KEY_PASSWORD=""
export ANDROID_STORE_PASSWORD=""
export ANDROID_STORE_FILE="path/to/key.jks"
export GOOGLE_ACCESS_FILE="path/to/google_service_key.json"
```

#### IOS
```sh
export MATCH_PASSWORD="" # Match repository passphrase
export FASTLINE_IOS_CREDENTIAL_GIT="https://example.com/ios-certificates.git"
export APP_STORE_CONNECT_API_KEY_ID=""
export APP_STORE_CONNECT_ISSUER_ID=""
export APP_STORE_CONNECT_KEY_FILE_PATH="AuthXYZ.p8"

# Deprecated
# export SPACESHIP_SKIP_2FA_UPGRADE=1
# export FASTLANE_USER="" # Apple account email
# export FASTLANE_PASSWORD="" # Apple account password
# export MATCH_GIT_BEARER_AUTHORIZATION=""  # Azure bearer auth token
# export MATCH_GIT_BASIC_AUTHORIZATION=""  # Github basic auth token
```

#### Deploy Gate
```sh
export DEPLOY_GATE_TOKEN=""
```
<br/>
#### Helper scripts

```
# Run build_runner
flutter pub run build_runner build

# Generate locale translation files
flutter gen-l10n

# Save app env variables
dart parse_env.dart
```
